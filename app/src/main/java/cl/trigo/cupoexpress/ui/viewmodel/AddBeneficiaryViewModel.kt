package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.Communes
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.COMMUNE_PARAM
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.EMAIL_PARAM
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.FIRST_NAME_PARAM
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.LAST_NAME_PARAM
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.PHONE_PARAM
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.PHOTO_PARAM
import cl.trigo.cupoexpress.domain.usecase.AddBeneficiaryUseCase.Companion.RUT_PARAM
import cl.trigo.cupoexpress.domain.usecase.GetCommunesUseCase

class AddBeneficiaryViewModel(
    private val addBeneficiaryUseCase: AddBeneficiaryUseCase,
    private val getCommunesUseCase: GetCommunesUseCase
) : ViewModel() {

    val addBeneficiaryLiveResult = LiveCompletable()
    val communesLiveData = LiveResult<Communes>()

    fun getCommunes() = getCommunesUseCase.execute(communesLiveData)

    fun addBeneficiary(
        rut: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        photo: String,
        commune: String

    ) = addBeneficiaryUseCase.execute(
        addBeneficiaryLiveResult, mapOf(
            RUT_PARAM to rut,
            FIRST_NAME_PARAM to firstName,
            LAST_NAME_PARAM to lastName,
            EMAIL_PARAM to email,
            PHONE_PARAM to phone,
            COMMUNE_PARAM to commune,
            PHOTO_PARAM to photo
        )
    )
}
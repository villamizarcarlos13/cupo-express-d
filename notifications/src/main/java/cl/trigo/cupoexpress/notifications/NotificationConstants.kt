package cl.trigo.cupoexpress.notifications

object NotificationConstants {
    const val NEW_NOTIFICATION = 1
    const val READ_NOTIFICATION = 2
    const val HEADER_NEW_NOTIFICATION = -1
    const val HEADER_READ_NOTIFICATION = -2
}
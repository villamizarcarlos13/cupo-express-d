package cl.trigo.cupoexpress.notifications.ui.mapper

import cl.trigo.cupoexpress.notifications.NotificationConstants.HEADER_NEW_NOTIFICATION
import cl.trigo.cupoexpress.notifications.NotificationConstants.HEADER_READ_NOTIFICATION
import cl.trigo.cupoexpress.notifications.NotificationConstants.NEW_NOTIFICATION
import cl.trigo.cupoexpress.notifications.NotificationConstants.READ_NOTIFICATION
import cl.trigo.cupoexpress.notifications.domain.model.NotificationModel
import cl.trigo.cupoexpress.notifications.ui.model.Notification

class NotificationMapper {

    fun NotificationModel.toModel() = Notification(
        id = id,
        title = title,
        body = body,
        dateCreated = dateCreated * 1000,
        isRead = isRead,
        iconUrl = iconUrl,
        viewType = if (isRead) READ_NOTIFICATION else NEW_NOTIFICATION
    )

    fun sortNotificationByDate(list: List<Notification>): List<Notification> {
        val new = list.filter { !it.isRead }.sortedByDescending { it.dateCreated }
        val read = list.filter { it.isRead }.sortedByDescending { it.dateCreated }
        val sorted: MutableList<Notification> = ArrayList()
        if (new.isNotEmpty()) {
            sorted.add(addNewNotificationHeader())
            sorted.addAll(new)
        }
        if (read.isNotEmpty()) {
            sorted.add(addReadNotificationHeader())
            sorted.addAll(read)
        }
        return sorted
    }

    private fun addNewNotificationHeader() = Notification(
        id = "",
        title = "",
        body = "",
        dateCreated = 0,
        isRead = false,
        iconUrl = "",
        viewType = HEADER_NEW_NOTIFICATION
    )

    private fun addReadNotificationHeader() = Notification(
        id = "",
        title = "",
        body = "",
        dateCreated = 0,
        isRead = false,
        iconUrl = "",
        viewType = HEADER_READ_NOTIFICATION
    )
}
package cl.trigo.payment.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.core.extension.formatAmount
import cl.trigo.payment.R
import cl.trigo.payment.domain.model.Benefit
import cl.trigo.payment.ui.fragment.listener.OnClickBenefitListener
import kotlinx.android.synthetic.main.item_benefit.view.*


class BenefitsAdapter(
    private val list: List<Benefit>,
    private val onClickListener: OnClickBenefitListener
) : RecyclerView.Adapter<BenefitsAdapter.ViewHolder>() {

    private var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_benefit, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.amount.text = formatAmount(item.price)

        if (position == selectedPosition) {
            holder.amount.setTextColor(ContextCompat.getColor(holder.amount.context, R.color.colorPrimary))
            holder.view.isSelected = true
            holder.radioButton.isChecked = true
        } else {
            holder.amount.setTextColor(ContextCompat.getColor(holder.amount.context, R.color.grey))
            holder.view.isSelected = false
            holder.radioButton.isChecked = false
        }

        with(holder.view) {
            tag = item
            setOnClickListener {
                onClickListener.onClickItem(list[position])
                selectedPosition = position
                notifyDataSetChanged()
                return@setOnClickListener
            }
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val amount: TextView = view.tv_benefit
        val radioButton: RadioButton = view.rb_benefit
    }
}
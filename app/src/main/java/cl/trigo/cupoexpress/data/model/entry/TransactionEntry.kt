package cl.trigo.cupoexpress.data.model.entry


import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.Transaction

@Keep
data class TransactionEntry(
    val last4CardDigits: String?,
    val transactionId: String?,
    val authorizationCode: String?,
    val responseCode: Int,
    val creditCardType: String?
)

fun TransactionEntry.toTransaction(): Transaction {
    return Transaction(
        last4CardDigits = last4CardDigits.orEmpty(),
        transactionId = transactionId.orEmpty(),
        authorizationCode = authorizationCode.orEmpty(),
        responseCode = responseCode,
        creditCardType = creditCardType.orEmpty()
    )
}
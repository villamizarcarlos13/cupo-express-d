package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import cl.trigo.cupoexpress.R
import kotlinx.android.synthetic.main.item_commune.view.*


class CommunesAdapter(
    private val list: List<String>
) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val viewHolder: ViewHolder
        val rowView: View?
        if (convertView == null) {
            rowView = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_commune, parent, false)
            viewHolder = ViewHolder(rowView)
            rowView.tag = viewHolder

        } else {
            rowView = convertView
            viewHolder = rowView.tag as ViewHolder
        }
        viewHolder.communeName.text = list[position]
        return rowView!!
    }

    override fun getItem(position: Int): String = list.get(position)

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = list.size

    inner class ViewHolder(view: View) {
        val communeName: TextView = view.tv_commune_name
    }
}
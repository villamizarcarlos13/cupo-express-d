package cl.trigo.authentication.domain.model

import androidx.annotation.Keep

@Keep
class Status(
    val status: Boolean,
    val statusCode: Int,
    val description: String
)

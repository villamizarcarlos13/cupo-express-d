package cl.trigo.payment.domain.repository

import cl.trigo.core.base.ResponseCompletable
import cl.trigo.payment.domain.model.Benefit
import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import kotlinx.coroutines.flow.Flow

interface PaymentRepository {
    suspend fun getPaymentMethods(forceUpdate: Boolean): Flow<List<PaymentMethod>>
    suspend fun getBenefits(): List<Benefit>
    suspend fun deletePaymentMethod(id: String)
    suspend fun confirmPayment(
        benefitId: String,
        beneficiaryId: String,
        quantity: String,
        paymentMethodId: String
    ): ResponseCompletable

    suspend fun registerPaymentMethod(): RegisterPaymentMethod
}
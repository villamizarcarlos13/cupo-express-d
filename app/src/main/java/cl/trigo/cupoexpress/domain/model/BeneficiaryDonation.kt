package cl.trigo.cupoexpress.domain.model

data class BeneficiaryDonation (
    val type: String,
    val amount: String,
    val beneficiary: Beneficiary,
    val quantity: Int?
)

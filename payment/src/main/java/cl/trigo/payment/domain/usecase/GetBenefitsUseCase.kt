package cl.trigo.payment.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.payment.domain.model.Benefit
import cl.trigo.payment.domain.repository.PaymentRepository
import kotlinx.coroutines.Dispatchers

class GetBenefitsUseCase(
    private val paymentRepository: PaymentRepository
) : ResultUnitUseCase<List<Benefit>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): List<Benefit>? {
        return paymentRepository.getBenefits()
    }
}
package cl.trigo.authentication.data.source

import cl.trigo.authentication.domain.model.User

interface TokenDataSource {
    suspend fun saveAuthToken(token: String)
    suspend fun getAuthToken(): String?
    suspend fun closeSession(): Boolean
    suspend fun saveUserInfo(user: User)
}
package cl.trigo.authentication.data.source.local

import cl.trigo.authentication.data.source.TokenDataSource
import cl.trigo.authentication.domain.model.User
import cl.trigo.core.util.AppPreferences

class TokenLocalDataSource(
    private val appPreferences: AppPreferences
) : TokenDataSource {
    override suspend fun saveAuthToken(token: String) {
        appPreferences.authToken = token
    }

    override suspend fun getAuthToken(): String? = appPreferences.authToken
    override suspend fun closeSession(): Boolean = appPreferences.cleanSession()
    override suspend fun saveUserInfo(user: User) {
        appPreferences.userFirstName = user.firstname
        appPreferences.userImage = user.photo
    }
}
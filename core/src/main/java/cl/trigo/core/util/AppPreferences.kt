package cl.trigo.core.util

import android.content.SharedPreferences
import androidx.core.content.edit
import org.koin.core.KoinComponent
import org.koin.core.inject

object AppPreferences : KoinComponent {

    private val preferences: SharedPreferences by inject()

    const val AUTH_TOKEN_DEFAULT = "authTokenDefault"
    private const val AUTH_TOKEN = "authToken"
    private const val FIREBASE_MESSAGING_TOKEN = "firebaseToken"
    const val SHARED_PREFERENCES_NAME = "CupoExpress"
    private const val USER_FIRST_NAME = "user_first_name"
    private const val USER_IMAGE = "user_image"
    private const val SHOW_COMMUNES_PROMPT = "show_communes_prompt"
    private const val NEW_NOTIFICATIONS_COUNT = "new_notification_count"

    var authToken: String?
        get() = preferences.getString(
            AUTH_TOKEN,
            AUTH_TOKEN_DEFAULT
        )
        set(value) = preferences.edit {
            putString(AUTH_TOKEN, "Bearer $value").apply()
        }

    var firebaseToken: String?
        get() = preferences.getString(
            FIREBASE_MESSAGING_TOKEN, ""
        )
        set(value) = preferences.edit {
            putString(FIREBASE_MESSAGING_TOKEN, value).apply()
        }

    fun cleanSession(): Boolean {
        preferences.edit()
            .remove(AUTH_TOKEN)
            .remove(USER_FIRST_NAME)
            .remove(USER_IMAGE)
            .apply()
        return authToken == null
    }

    var userFirstName: String?
        get() = preferences.getString(USER_FIRST_NAME, "")
        set(value) = preferences.edit {
            putString(USER_FIRST_NAME, value).apply()
        }

    var userImage: String?
        get() = preferences.getString(USER_IMAGE, "")
        set(value) = preferences.edit {
            putString(USER_IMAGE, value).apply()
        }

    var showCommunesPrompt: Boolean
        get() = preferences.getBoolean(SHOW_COMMUNES_PROMPT, true)
        set(value) = preferences.edit {
            putBoolean(SHOW_COMMUNES_PROMPT, value).apply()
        }

    var newNotificationCount: Int
        get() = preferences.getInt(NEW_NOTIFICATIONS_COUNT, 0)
        set(value) = preferences.edit {
            putInt(NEW_NOTIFICATIONS_COUNT, value).apply()
        }

}
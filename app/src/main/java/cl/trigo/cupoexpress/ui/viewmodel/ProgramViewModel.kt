package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.authentication.domain.usecase.CloseSessionUseCase
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.Program
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.domain.usecase.GetProgramsUseCase
import cl.trigo.cupoexpress.domain.usecase.GetUserInfoUseCase

class ProgramViewModel(
    private val getProgramsUseCase: GetProgramsUseCase,
    private val getUserInfoUseCase: GetUserInfoUseCase,
    private val closeSessionUseCase: CloseSessionUseCase
) : ViewModel() {
    val getProgramLiveData = LiveResult<List<Program>>()
    val userInfoLiveData = LiveResult<UserInfo>()
    val closeSessionLiveData = LiveCompletable()

    fun getPrograms() =
        getProgramsUseCase.execute(getProgramLiveData)

    fun getUserInfo() = getUserInfoUseCase.execute(userInfoLiveData)

    fun closeSession() = closeSessionUseCase.execute(closeSessionLiveData)
}
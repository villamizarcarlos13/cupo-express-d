package cl.trigo.payment.ui.fragment.adapter.diffutil

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import cl.trigo.payment.domain.model.PaymentMethod

class PaymentMethodsDiffCallback(
    private val oldList: List<PaymentMethod>,
    private val newList: List<PaymentMethod>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val new = newList[newItemPosition]
        val old = oldList[oldItemPosition]
        val isIdTheSame = old.id == new.id
        val isTypeTheSame = old.creditCardType == new.creditCardType
        val isDigitsTheSame = old.lastCardDigits == new.lastCardDigits
        val isLastTheSame = old.isLast == new.isLast
        return isIdTheSame && isTypeTheSame && isDigitsTheSame && isLastTheSame
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val (_, value, name) = oldList[oldPosition]
        val (_, value1, name1) = newList[newPosition]

        return name == name1 && value == value1
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val new = newList[newItemPosition]
        val old = oldList[oldItemPosition]
        val set = mutableSetOf<String>()

        val isIdTheSame = old.id == new.id
        val isTypeTheSame = old.creditCardType == new.creditCardType
        val isDigitsTheSame = old.lastCardDigits == new.lastCardDigits
        val isLastTheSame = old.isLast == new.isLast

        if (isIdTheSame.not()) {
            set.add(ID)
        }
        if (isTypeTheSame.not()) {
            set.add(TYPE)
        }
        if (isDigitsTheSame.not()) {
            set.add(DIGITS)
        }
        if (isLastTheSame.not()) {
            set.add(IS_LAST)
        }
        return set
    }

    companion object {
        const val ID = "ID"
        const val TYPE = "TYPE"
        const val DIGITS = "DIGITS"
        const val IS_LAST = "IS_LAST"
    }
}
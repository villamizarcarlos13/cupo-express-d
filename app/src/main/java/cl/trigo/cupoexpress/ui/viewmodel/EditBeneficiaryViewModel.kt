package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.Communes
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.COMMUNE_PARAM
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.EMAIL_PARAM
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.FIRST_NAME_PARAM
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.ID_PARAM
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.LAST_NAME_PARAM
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.PHONE_PARAM
import cl.trigo.cupoexpress.domain.usecase.EditBeneficiaryUseCase.Companion.PHOTO_PARAM
import cl.trigo.cupoexpress.domain.usecase.GetCommunesUseCase

class EditBeneficiaryViewModel(
    private val editBeneficiaryUseCase: EditBeneficiaryUseCase,
    private val getCommunesUseCase: GetCommunesUseCase
) : ViewModel() {

    val editBeneficiaryLiveResult = LiveCompletable()
    val communesLiveData = LiveResult<Communes>()

    fun getCommunes() = getCommunesUseCase.execute(communesLiveData)

    fun editBeneficiary(
        rut: String,
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        commune: String?,
        photo: String?
    ) = editBeneficiaryUseCase.execute(
        editBeneficiaryLiveResult, mapOf(
            ID_PARAM to rut,
            COMMUNE_PARAM to commune,
            FIRST_NAME_PARAM to firstName,
            LAST_NAME_PARAM to lastName,
            EMAIL_PARAM to email,
            PHONE_PARAM to phone,
            PHOTO_PARAM to photo
        )
    )
}
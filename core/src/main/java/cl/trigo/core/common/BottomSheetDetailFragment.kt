package cl.trigo.core.common

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cl.trigo.core.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_detail_bottom_sheet.*


class BottomSheetDetailFragment : BottomSheetDialogFragment() {

    private var title: String = ""
    private var subTitle: String = ""
    private var description: String = ""
    private var image: String = ""
    private var buttonText: String = ""
    private var clickListener: (() -> Unit)? = null

    private val liveData: MutableLiveData<Boolean> = MutableLiveData()

    fun liveData(): LiveData<Boolean> = liveData

    override fun getTheme(): Int = R.style.RoundedBottomSheetDialogTheme

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dialog.let {
            it?.setOnShowListener { dialog ->
                val d = dialog as BottomSheetDialog
                val bottomSheetInternal =
                    d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                BottomSheetBehavior.from<View?>(bottomSheetInternal!!).state =
                    BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return inflater.inflate(R.layout.dialog_detail_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            title = it.getString(KEY_TITLE).toString()
            subTitle = it.getString(KEY_SUB_TITLE).toString()
            description = it.getString(KEY_DESCRIPTION).toString()
            image = it.getString(KEY_IMAGE).toString()
            buttonText = it.getString(KEY_ACTION_BUTTON).toString()
        }
    }

    private fun initView() {
        tv_title.text = title
        tv_subtitle.text = subTitle
        tv_description.text = description
        btn_action.text = buttonText
        btn_action.isVisible = buttonText.isNotEmpty()
        btn_action.setOnClickListener { clickListener?.invoke() }
        btn_close.setOnClickListener {
            this@BottomSheetDetailFragment.dismiss()
        }
    }

    fun setActionClickListener(listener: () -> Unit) {
        clickListener = listener
    }

    companion object {

        fun newInstance(
            title: String,
            subTitle: String?,
            description: String?,
            image: String?,
            buttonText: String?
        ): BottomSheetDetailFragment =
            BottomSheetDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_TITLE, title)
                    putString(KEY_SUB_TITLE, subTitle.orEmpty())
                    putString(KEY_DESCRIPTION, description.orEmpty())
                    putString(KEY_IMAGE, image.orEmpty())
                    putString(KEY_ACTION_BUTTON, buttonText.orEmpty())
                }
            }

        private const val KEY_TITLE = "title"
        private const val KEY_SUB_TITLE = "sub_title"
        private const val KEY_DESCRIPTION = "description"
        private const val KEY_IMAGE = "image"
        private const val KEY_ACTION_BUTTON = "action_button"
    }

}
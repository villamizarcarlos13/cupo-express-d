package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.Commune

@Keep
data class CommuneEntry(
    val name: String,
    val country: String,
    val region: String,
    val province: String,
    val isActive: Boolean,
    val id: String
)

fun CommuneEntry.toCommune(): Commune {
    return Commune(name, country, region, province, isActive, id)
}

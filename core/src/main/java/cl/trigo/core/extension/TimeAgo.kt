package cl.trigo.core.extension

import android.annotation.SuppressLint
import android.text.format.DateUtils
import android.text.format.DateUtils.DAY_IN_MILLIS
import android.text.format.DateUtils.WEEK_IN_MILLIS
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val DATE_FORMAT_SIMPLE = "dd/MM/yy"
const val TIME_FORMAT_SIMPLE = "kk:mm"

@SuppressLint("SimpleDateFormat")
fun getTimeAgo(timeStamp: String?, resources: Array<String>): String {
    val notAvailable = ""
    val date: Date?
    if (timeStamp.isNullOrEmpty()) {
        return notAvailable
    } else {
        val sdf = SimpleDateFormat("YYYY-MM-DD")
        try {
            date = sdf.parse(timeStamp)
        } catch (e: ParseException) {
            return notAvailable
        }
    }

    var time = date?.time
    if (time != null) {
        if (time < 1000000000000L) {
            time *= 1000
        }
    } else {
        return notAvailable
    }

    val now = System.currentTimeMillis()
    if (time > now || time <= 0) {
        return notAvailable
    }

    val diff = now - time
    return when {
        diff < DAY_IN_MILLIS -> resources[0]
        diff < DAY_IN_MILLIS * 2 -> resources[1]
        diff < DAY_IN_MILLIS * 3 -> resources[2]
        diff < DAY_IN_MILLIS * 4 -> resources[3]
        diff < DAY_IN_MILLIS * 5 -> resources[4]
        diff < DAY_IN_MILLIS * 6 -> resources[5]
        diff < DAY_IN_MILLIS * 7 -> resources[6]
        diff < WEEK_IN_MILLIS * 2 -> resources[7]
        diff < WEEK_IN_MILLIS * 3 -> resources[8]
        diff < WEEK_IN_MILLIS * 4 -> resources[9]
        diff < DAY_IN_MILLIS * 30 -> resources[10]
        else -> resources[11]
    }
}

fun millisToDate(millis: Long, yesterday: String): String {
    return when {
        DateUtils.isToday(millis) -> SimpleDateFormat(TIME_FORMAT_SIMPLE, Locale.getDefault()).format(millis)
        DateUtils.isToday(millis + DAY_IN_MILLIS) -> yesterday
        else -> SimpleDateFormat(DATE_FORMAT_SIMPLE, Locale.getDefault()).format(millis)
    }
}
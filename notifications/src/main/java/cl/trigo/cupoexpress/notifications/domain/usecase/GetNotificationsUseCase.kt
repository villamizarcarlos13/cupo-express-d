package cl.trigo.cupoexpress.notifications.domain.usecase


import cl.trigo.cupoexpress.notifications.domain.model.NotificationModel
import cl.trigo.cupoexpress.notifications.domain.repository.NotificationRepository
import kotlinx.coroutines.flow.Flow

class GetNotificationsUseCase(private val repository: NotificationRepository) {
    suspend fun getAllNotifications(forceUpdate: Boolean): Flow<List<NotificationModel>> = repository.getAllNotifications(forceUpdate)
}
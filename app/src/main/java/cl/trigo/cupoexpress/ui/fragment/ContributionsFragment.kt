package cl.trigo.cupoexpress.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.observe
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Contribution
import cl.trigo.cupoexpress.ui.fragment.adapter.ContributionsAdapter
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickContribution
import cl.trigo.cupoexpress.ui.viewmodel.ContributionsViewModel
import kotlinx.android.synthetic.main.fragment_contributions.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContributionsFragment : BaseFragment(), OnClickContribution {

    private val viewModel by viewModel<ContributionsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(getContributionsLiveData, ::getContributionsObserver)
            getContributions()
        }
        list_contributions.layoutManager = LinearLayoutManager(context)
    }

    private fun showLoading(isLoading: Boolean) {
        contributions_loading.isVisible = isLoading
    }

    private fun getContributionsObserver(result: Result<List<Contribution>>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
            }
            is Result.OnSuccess -> {
                showLoading(false)
                if (result.value.isEmpty()) {
                    ll_list.isVisible = false
                    tv_no_contributions.isVisible = true
                } else {
                    ll_list.isVisible = true
                    tv_no_contributions.isVisible = false
                    list_contributions.adapter = ContributionsAdapter(result.value, this)
                }
            }
            is Result.OnError -> {
                showLoading(false)
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
                ll_list.isVisible = false
                tv_no_contributions.isVisible = true
            }
        }
    }

    override fun onClickItem(contribution: Contribution) {

    }

    override fun getLayoutId(): Int = R.layout.fragment_contributions
}
package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult
import cl.trigo.core.extension.postComplete
import cl.trigo.core.extension.postThrowable
import cl.trigo.cupoexpress.domain.model.BeneficiaryDetail
import cl.trigo.cupoexpress.domain.model.Donation
import cl.trigo.cupoexpress.domain.model.SetFavoriteParams
import cl.trigo.cupoexpress.domain.usecase.DeleteBeneficiaryByIdUseCase
import cl.trigo.cupoexpress.domain.usecase.GetBeneficiaryDetailByRutUseCase
import cl.trigo.cupoexpress.domain.usecase.GetBeneficiaryDonationsUseCase
import cl.trigo.cupoexpress.domain.usecase.SetBeneficiaryFavoriteUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BeneficiaryDetailViewModel(
    private val getBeneficiaryDetailByRutUseCase: GetBeneficiaryDetailByRutUseCase,
    private val deleteBeneficiaryByIdUseCase: DeleteBeneficiaryByIdUseCase,
    private val setBeneficiaryFavoriteUseCase: SetBeneficiaryFavoriteUseCase,
    private val getDonationsUseCase: GetBeneficiaryDonationsUseCase
) : ViewModel() {
    val getBeneficiaryDetailLiveData = LiveResult<BeneficiaryDetail>()
    val getBeneficiaryDonationsLiveData = LiveResult<List<Donation>>()
    val deleteBeneficiaryLiveData = LiveCompletable()
    val setFavoriteLiveData = LiveCompletable()

    fun getBeneficiaryDetails(rut: String) =
        getBeneficiaryDetailByRutUseCase.execute(getBeneficiaryDetailLiveData, rut)

    fun getBeneficiaryDonations(rut: String) =
        getDonationsUseCase.execute(getBeneficiaryDonationsLiveData, rut)

    fun deleteBeneficiaryById(beneficiaryId: String) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            deleteBeneficiaryByIdUseCase.deleteBeneficiary(beneficiaryId)
        }.onFailure { deleteBeneficiaryLiveData.postThrowable(it) }
            .onSuccess { deleteBeneficiaryLiveData.postComplete() }
    }

    fun setFavorite(id: String, favorite: Boolean) {
        setBeneficiaryFavoriteUseCase.execute(setFavoriteLiveData, SetFavoriteParams(id, favorite))
    }
}
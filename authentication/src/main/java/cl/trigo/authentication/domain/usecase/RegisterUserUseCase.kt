package cl.trigo.authentication.domain.usecase

import cl.trigo.authentication.domain.model.Register
import cl.trigo.authentication.domain.repository.LoginRepository
import cl.trigo.core.coroutines.ResultUseCase
import kotlinx.coroutines.Dispatchers

class RegisterUserUseCase(
    private val loginRepository: LoginRepository
) : ResultUseCase<Map<String, String>, Register>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {

    companion object {
        const val FIRST_NAME_PARAM = "FIRST_NAME_PARAM"
        const val LAST_NAME_PARAM = "LAST_NAME_PARAM"
        const val RUT_PARAM = "RUT_PARAM"
        const val EMAIL_PARAM = "EMAIL_PARAM"
        const val PASSWORD_PARAM = "PASSWORD_PARAM"
    }

    override suspend fun executeOnBackground(params: Map<String, String>): Register? {

        return loginRepository.signUp(
            params.getValue(FIRST_NAME_PARAM),
            params.getValue(LAST_NAME_PARAM),
            params.getValue(RUT_PARAM),
            params.getValue(EMAIL_PARAM),
            params.getValue(PASSWORD_PARAM)
        )
    }
}
package cl.trigo.authentication.domain.usecase

import android.content.res.Resources
import cl.trigo.authentication.domain.model.User
import cl.trigo.authentication.domain.repository.LoginRepository
import cl.trigo.authentication.domain.repository.TokenRepository
import cl.trigo.core.coroutines.ResultUseCase
import kotlinx.coroutines.Dispatchers

class LoginUseCase(
    private val loginRepository: LoginRepository,
    private val tokenRepository: TokenRepository
) : ResultUseCase<Map<String, String>, User>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {

    companion object {
        const val EMAIL_PARAM = "EMAIL_PARAM"
        const val PASSWORD_PARAM = "PASSWORD_PARAM"
    }

    override suspend fun executeOnBackground(params: Map<String, String>): User? {

        if (params.containsKey(EMAIL_PARAM) && params.containsKey(
                PASSWORD_PARAM
            )
        ) {

            val email = params.getValue(EMAIL_PARAM)
            val password = params.getValue(PASSWORD_PARAM)

            val login = loginRepository.signIn(email, password)

            if (login.status.statusCode == 200) {
                tokenRepository.saveAuthToken(token = login.idToken)
                tokenRepository.saveUserInfo(login.user)
                return login.user
            }
        }
        throw Resources.NotFoundException()
    }
}
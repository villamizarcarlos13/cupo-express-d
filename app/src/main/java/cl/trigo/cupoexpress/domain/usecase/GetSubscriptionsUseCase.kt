package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.Subscription
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetSubscriptionsUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUnitUseCase<List<Subscription>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): List<Subscription>? {
        return beneficiaryRepository.getSubscriptions()
    }
}
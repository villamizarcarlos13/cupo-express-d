package cl.trigo.cupoexpress.ui.fragment.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.cupoexpress.R
import kotlinx.android.synthetic.main.item_brand.view.*


class BrandsAdapter : RecyclerView.Adapter<BrandsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_brand, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.image.setImageDrawable(holder.image.context?.let {
            ContextCompat.getDrawable(it, brandIds[position])
        })
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.iv_brand
    }

    override fun getItemCount(): Int = brandIds.size

    private var brandIds = arrayOf(
        R.drawable.logo_axe, R.drawable.logo_cif,
        R.drawable.logo_dove, R.drawable.logo_hellmanns,
        R.drawable.logo_lipton, R.drawable.logo_lux,
        R.drawable.logo_omo, R.drawable.logo_rexona,
        R.drawable.logo_sedal, R.drawable.logo_tresemme,
        R.drawable.logo_drive, R.drawable.logo_rinso,
        R.drawable.logo_vim, R.drawable.logo_maizena,
        R.drawable.logo_confort, R.drawable.logo_elite,
        R.drawable.logo_nova, R.drawable.logo_babysec,
        R.drawable.logo_ladysoft, R.drawable.logo_cotidian,
        R.drawable.logo_watts, R.drawable.logo_soprole,
        R.drawable.logo_supremo, R.drawable.logo_superior,
        R.drawable.logo_lucchetti
    )
}
package cl.trigo.authentication.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import cl.trigo.authentication.R
import cl.trigo.core.base.BaseFragment

class SplashFragment : BaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler().postDelayed({
            NavHostFragment.findNavController(this).navigate(R.id.action_splash_to_rut)
        }, 2000)
    }

    override fun getLayoutId() = R.layout.fragment_splash
}

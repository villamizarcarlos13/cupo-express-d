package cl.trigo.cupoexpress.notifications.data.local

import cl.trigo.core.storage.dao.NotificationsDao
import cl.trigo.core.storage.entity.NotificationEntity
import cl.trigo.core.util.AppPreferences

class NotificationLocalDataSource(
    private val notificationsDao: NotificationsDao,
    private val appPreferences: AppPreferences
) :
    NotificationLocal {

    override suspend fun getAllNotifications(): List<NotificationEntity> =
        notificationsDao.getAllNotifications()

    override suspend fun insertNotificationsList(notifications: List<NotificationEntity>) =
        notificationsDao.insertNotificationsList(notifications)

    override suspend fun markAsReadNotification(id: String) =
        notificationsDao.markNotificationAsReadById(true, id)

    override suspend fun deleteNotification(id: String) =
        notificationsDao.deleteNotificationById(id)

    override suspend fun deleteAllNotifications() =
        notificationsDao.deleteAllNotifications()

    override suspend fun updateNotificationCount(count: Int) {
        appPreferences.newNotificationCount = count
    }

}
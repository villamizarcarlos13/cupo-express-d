package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.Contribution
import cl.trigo.cupoexpress.domain.usecase.GetContributionsUseCase


class ContributionsViewModel(
    private val getContributionsUseCase: GetContributionsUseCase
) : ViewModel(

) {
    val getContributionsLiveData = LiveResult<List<Contribution>>()

    fun getContributions() = getContributionsUseCase.execute(getContributionsLiveData)

}
package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Subscription
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickSubscription
import kotlinx.android.synthetic.main.item_subscription.view.*

class SubscriptionsAdapter(
    private val list: List<Subscription>,
    private val onClickListener: OnClickSubscription
) : RecyclerView.Adapter<SubscriptionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_subscription, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.name.text = item.fullname
        holder.detail.text = item.amount

        with(holder.view) {
            tag = item
            setOnClickListener {
                onClickListener.onClickItem(list[position])
                return@setOnClickListener
            }
        }
        holder.delete.setOnClickListener {
            onClickListener.onClickDelete(list[position])
            return@setOnClickListener
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.tv_beneficiary_name
        val detail: TextView = view.tv_subscription_detail
        val image: ImageView = view.iv_beneficiary_photo
        val delete: ImageView = view.iv_delete
    }
}
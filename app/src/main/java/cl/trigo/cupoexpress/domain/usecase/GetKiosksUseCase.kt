package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.Kiosk
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetKiosksUseCase(
    private val repository: BeneficiaryRepository
) : ResultUnitUseCase<List<Kiosk>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): List<Kiosk> = repository.getKiosks()
}
package cl.trigo.cupoexpress.ui.fragment

import android.app.AlertDialog
import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.authentication.ui.activity.LoginActivity
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.exceptions.DeleteBeneficiaryException
import cl.trigo.core.extension.*
import cl.trigo.core.navigation.NotificationNavigation
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.AppConstants
import cl.trigo.cupoexpress.AppConstants.REQUEST_LOGIN_FROM_PEOPLE_LIST
import cl.trigo.cupoexpress.AppConstants.TOKEN_EXPIRED_403
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.databinding.FragmentPeopleBinding
import cl.trigo.cupoexpress.domain.model.Beneficiary
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.ui.activity.BeneficiaryDetailActivity
import cl.trigo.cupoexpress.ui.activity.NewBeneficiaryActivity
import cl.trigo.cupoexpress.ui.fragment.adapter.BenefitedAdapter
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickBeneficiary
import cl.trigo.cupoexpress.ui.fragment.listener.OnLongClickBeneficiary
import cl.trigo.cupoexpress.ui.viewmodel.BeneficiaryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.HttpException

class PeopleListFragment : BaseFragment(), OnClickBeneficiary, OnLongClickBeneficiary {

    private var _binding: FragmentPeopleBinding? = null
    private val binding get() = _binding!!
    private val beneficiaryViewModel by viewModel<BeneficiaryViewModel>()
    private var navigateToAddBeneficiary = false
    private var userInfo: UserInfo? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPeopleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(beneficiaryViewModel) {
            observe(getBeneficiariesLiveData, ::getBeneficiariesObserver)
            observe(deleteBeneficiaryLiveData, ::deleteBeneficiaryObserver)
            observe(closeSessionLiveData, ::closeSessionObserver)
            observe(userInfoLiveData, ::userInfoObserver)
            getUserInfo()
        }
        binding.apply {
            list.layoutManager = LinearLayoutManager(context)
            toolbarHomePeople.ivNotification.setSingleOnClickListener { navigateToNotifications() }
            btnAddPeople.setOnClickListener {
                // TODO: I have to improve this block
                activity.let {
                    if (AppPreferences.authToken != AppPreferences.AUTH_TOKEN_DEFAULT) {
                        startActivityForResult(
                            NewBeneficiaryActivity.getLaunchIntent(activity as Context),
                            REQUEST_ADD
                        )
                    } else startActivityForResult(
                        LoginActivity.getLaunchIntent(it as Context, false),
                        REQUEST_LOGIN_FROM_PEOPLE_LIST
                    )
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        messageReceiver?.let { LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(it, IntentFilter(AppConstants.NOTIFICATION_DATA)) }
    }

    override fun onResume() {
        super.onResume()
        beneficiaryViewModel.getUserInfo()
    }

    override fun onStop() {
        super.onStop()
        messageReceiver?.let { LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private val messageReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (userInfo?.isAuthenticated == true) {
                binding.toolbarHomePeople.ivNotification.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                    R.drawable.toolbar_icon_notification_on) })
            }
        }
    }

    override fun onClick(beneficiary: Beneficiary) {
        startActivityForResult(
            BeneficiaryDetailActivity.getLaunchIntent(
                activity as Context,
                beneficiary.id,
                userInfo?.userImage.orEmpty()
            ), REQUEST_DETAIL
        )
    }

    override fun onLongClick(beneficiary: Beneficiary) {
        val message =
            "¿Está seguro que desea eliminar a " + beneficiary.firstname + " " + beneficiary.lastname + " de su lista de beneficiados?"
        AlertDialog.Builder(activity)
            .setTitle("¡Importante!")
            .setMessage(message)
            .setPositiveButton("Aceptar") { _: DialogInterface, _: Int ->
                beneficiaryViewModel.deleteBeneficiaryById(beneficiary.id)
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    private fun setViewNotAuthenticated() {
        binding.apply {
            toolbarHomePeople.ivNotification.isEnabled = false
            toolbarHomePeople.tvUserName.text = getSpannedText(getString(R.string.home_toolbar_title_na))
            toolbarHomePeople.ivNotification.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.toolbar_icon_notification_off) })
            toolbarHomePeople.ivUserPhoto.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_default_photo) })
            list.isVisible = false
            layoutEmpty.root.isVisible = true
            tvListIsEmpty.isVisible = true
            tvListIsEmpty.text = resources.getString(R.string.people_list_need_to_start_session)
        }
        userInfo = null
    }

    private fun setViewAuthenticated(user: UserInfo?) {
        binding.apply {
            if (user?.newNotificationsCounter != userInfo?.newNotificationsCounter) {
                if (user?.newNotificationsCounter!! > 0) {
                    toolbarHomePeople.ivNotification.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.toolbar_icon_notification_on
                        )
                    })
                } else {
                    toolbarHomePeople.ivNotification.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.toolbar_icon_notification_off
                        )
                    })
                }
            }
            if (user?.name != userInfo?.name) {
                toolbarHomePeople.tvUserName.text =
                    getSpannedText(getString(R.string.home_toolbar_title, user?.name))
            }
            if (user?.userImage != userInfo?.userImage) {
                toolbarHomePeople.ivUserPhoto.loadWithCircleCrop(
                    user?.userImage,
                    R.drawable.ic_default_photo
                )
            }
            toolbarHomePeople.ivNotification.isEnabled = true
            list.isVisible = true
            layoutEmpty.root.isVisible = false
            tvListIsEmpty.isVisible = false
        }
        userInfo = user
    }

    private fun navigateToNotifications() {
        NotificationNavigation.dynamicStart?.let { startActivity(it) }
    }

    private fun setViewListIsEmpty(listIsEmpty: Boolean) {
        binding.apply {
            list.isVisible = !listIsEmpty
            layoutEmpty.root.isVisible = listIsEmpty
            tvListIsEmpty.isVisible = listIsEmpty
            tvListIsEmpty.text = resources.getString(R.string.people_list_is_empty)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        beneficiaryViewModel.getUserInfo()
        when (requestCode) {
            REQUEST_LOGIN_FROM_PEOPLE_LIST -> navigateToAddBeneficiary = true
        }
    }

    private fun getBeneficiariesObserver(result: Result<List<Beneficiary>>?) {
        when (result) {
            is Result.OnLoading -> {
            }
            is Result.OnSuccess -> {
                setViewListIsEmpty(result.value.isEmpty())
                binding.list.adapter = BenefitedAdapter(result.value, this, this)
            }
            is Result.OnError -> {
                val errorMessage = result.throwable.message.orEmpty()
                errorMessage.run { showErrorDialog(message = this) }

                if (result.throwable is HttpException) {
                    val exception: HttpException = result.throwable as HttpException
                    when (exception.code()) {
                        TOKEN_EXPIRED_403 -> {
                            beneficiaryViewModel.closeSession()
                        }
                    }
                }
            }
        }
    }

    private fun deleteBeneficiaryObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
            }
            is Completable.OnComplete -> beneficiaryViewModel.getBeneficiaries(false)
            is Completable.OnError -> {
                var errorMessage = result.throwable.message.orEmpty()
                when(result.throwable) {
                    is DeleteBeneficiaryException -> { errorMessage = resources.getString(R.string.delete_beneficiary_error) }
                }
                errorMessage.run { showErrorDialog(message = this) }
            }
            else -> {
            }
        }
    }

    private fun closeSessionObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
            }
            is Completable.OnComplete -> {
                beneficiaryViewModel.getUserInfo()
            }
            is Completable.OnError -> {
            }
        }
    }

    private fun userInfoObserver(result: Result<UserInfo>?) {
        when (result) {
            is Result.OnSuccess -> {
                if (result.value.isAuthenticated) {
                    if (navigateToAddBeneficiary) {
                        startActivityForResult(NewBeneficiaryActivity.getLaunchIntent(activity as Context), REQUEST_ADD)
                        navigateToAddBeneficiary = false
                    }
                    beneficiaryViewModel.getBeneficiaries(true)
                    setViewAuthenticated(result.value)
                } else setViewNotAuthenticated()
            }
            is Result.OnError -> setViewNotAuthenticated()
        }
    }

    override fun getLayoutId() = R.layout.fragment_people

    companion object {
        private const val REQUEST_DETAIL = 31
        private const val REQUEST_ADD = 41
    }
}

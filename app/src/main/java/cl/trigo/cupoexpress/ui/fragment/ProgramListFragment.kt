package cl.trigo.cupoexpress.ui.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.*
import cl.trigo.core.navigation.NotificationNavigation
import cl.trigo.cupoexpress.AppConstants
import cl.trigo.cupoexpress.AppConstants.TOKEN_EXPIRED_403
import cl.trigo.cupoexpress.AppConstants.URL_EXPLANATION_VIDEO
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.databinding.FragmentProgramBinding
import cl.trigo.cupoexpress.domain.model.Program
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.ui.activity.KioskMapActivity
import cl.trigo.cupoexpress.ui.activity.ProgramDetailActivity
import cl.trigo.cupoexpress.ui.viewmodel.ProgramViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.HttpException


class ProgramListFragment : BaseFragment() {

    private var _binding: FragmentProgramBinding? = null
    private val binding get() = _binding!!
    private val programViewModel by viewModel<ProgramViewModel>()
    private var userInfo: UserInfo? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProgramBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(programViewModel) {
            observe(getProgramLiveData, ::getProgramsObserver)
            observe(userInfoLiveData, ::userInfoObserver)
            observe(closeSessionLiveData, ::closeSessionObserver)
            getUserInfo()
        }
        setupUi()
    }

    override fun onStart() {
        super.onStart()
        messageReceiver?.let { LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(it, IntentFilter(AppConstants.NOTIFICATION_DATA)) }


    }

    override fun onResume() {
        super.onResume()
        programViewModel.getUserInfo()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStop() {
        super.onStop()
        messageReceiver?.let { LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(it) }
    }

    private val messageReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (userInfo?.isAuthenticated == true) {
                binding.toolbarHomeProgramm.ivNotification.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                    R.drawable.toolbar_icon_notification_on) })
            }
        }
    }

    private fun setupUi() {
        binding.apply {
            toolbarHomeProgramm.ivNotification.setSingleOnClickListener { navigateToNotifications() }
            btnContribute.setSingleOnClickListener {  }
            btnShowProgram.setSingleOnClickListener { showProgramDetails() }
            btnShowMap.setSingleOnClickListener { showMapOfKiosks() }
            ivPlay.setSingleOnClickListener { playVideo() }
        }
    }

    private fun playVideo() {
        val webView = WebView(context)
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(URL_EXPLANATION_VIDEO)
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(R.string.explication_video))
                .setView(webView)
                .setPositiveButton("OK", null)
                .show()
        }
    }

    private fun navigateToNotifications() {
        NotificationNavigation.dynamicStart?.let { startActivity(it) }
    }

    private fun showMapOfKiosks() {
        startActivity(context?.let { KioskMapActivity.getLaunchIntent(activity as Context) })
    }

    private fun showProgramDetails() {
        startActivity(context?.let { ProgramDetailActivity.getLaunchIntent(it, false) })
    }

    private fun setViewAuthenticated(user: UserInfo?) {
        binding.apply {
            if (user?.newNotificationsCounter != userInfo?.newNotificationsCounter) {
                if (user?.newNotificationsCounter!! > 0) {
                    toolbarHomeProgramm.ivNotification.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.toolbar_icon_notification_on
                        )
                    })
                } else {
                    toolbarHomeProgramm.ivNotification.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.toolbar_icon_notification_off
                        )
                    })
                }
            }
            if (user?.name != userInfo?.name) {
                toolbarHomeProgramm.tvUserName.text =
                    getSpannedText(getString(R.string.home_toolbar_title, user?.name))
            }
            if (user?.userImage != userInfo?.userImage) {
                toolbarHomeProgramm.ivUserPhoto.loadWithCircleCrop(
                    user?.userImage,
                    R.drawable.ic_default_photo
                )
            }
            toolbarHomeProgramm.ivNotification.isEnabled = true
            btnContribute.text = resources.getText(R.string.contribute)
            cvMap.isVisible = true
            cvProgram.isVisible = true
        }
        userInfo = user
    }

    private fun setViewNotAuthenticated() {
        binding.apply {
            toolbarHomeProgramm.tvUserName.text = getSpannedText(getString(R.string.home_toolbar_title_na))
            toolbarHomeProgramm.ivNotification.isEnabled = false
            toolbarHomeProgramm.ivNotification.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.toolbar_icon_notification_off) })
            toolbarHomeProgramm.ivUserPhoto.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_default_photo) })
            btnContribute.text = resources.getText(R.string.contribute2)
            cvMap.isVisible = false
            cvProgram.isVisible = false
        }
        userInfo = null
    }


    private fun userInfoObserver(result: Result<UserInfo>?) {
        when (result) {
            is Result.OnSuccess -> {
                if (result.value.isAuthenticated) {
                    setViewAuthenticated(result.value)
                } else setViewNotAuthenticated()
            }
            is Result.OnError -> setViewNotAuthenticated()
        }
    }

    private fun closeSessionObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
            }
            is Completable.OnComplete -> {
                programViewModel.getUserInfo()
            }
            is Completable.OnError -> {
            }
            else -> {
            }
        }
    }

    private fun getProgramsObserver(result: Result<List<Program>>?) {
        when (result) {
            is Result.OnLoading -> {
            }
            is Result.OnSuccess -> {
                binding.listPrograms.adapter =
                    cl.trigo.cupoexpress.ui.fragment.adapter.ProgramAdapter(result.value)
            }
            is Result.OnError -> {
                val errorMessage = result.throwable.message.orEmpty()
                errorMessage.run { showErrorDialog(message = this) }
                if (result.throwable is HttpException) {
                    val exception: HttpException = result.throwable as HttpException
                    when (exception.code()) {
                        TOKEN_EXPIRED_403 -> {
                            programViewModel.closeSession()
                        }
                    }
                }
            }
        }
    }

    override fun getLayoutId() = R.layout.fragment_program
}

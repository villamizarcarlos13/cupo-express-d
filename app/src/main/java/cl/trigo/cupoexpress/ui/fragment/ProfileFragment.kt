package cl.trigo.cupoexpress.ui.fragment

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import cl.trigo.authentication.ui.activity.LoginActivity
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.*
import cl.trigo.core.navigation.NotificationNavigation
import cl.trigo.cupoexpress.AppConstants
import cl.trigo.cupoexpress.AppConstants.REQUEST_LOGIN_FROM_PROFILE
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.databinding.FragmentProfileBinding
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.ui.activity.ContributionsActivity
import cl.trigo.payment.ui.activity.PaymentMethodsActivity
import cl.trigo.cupoexpress.ui.activity.SubscriptionsActivity
import cl.trigo.cupoexpress.ui.activity.UserPreferencesActivity
import cl.trigo.cupoexpress.ui.viewmodel.ProfileViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private val profileViewModel by viewModel<ProfileViewModel>()
    private var userInfo: UserInfo? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewNotAuthenticated()
        with(profileViewModel) {
            observe(closeSessionLiveCompletable, ::closeSessionObserver)
            observe(userInfoLiveData, ::userInfoObserver)
            getUserInfo()
        }
        setupClickListeners()
    }

    override fun onStart() {
        super.onStart()
        messageReceiver?.let { LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(it, IntentFilter(AppConstants.NOTIFICATION_DATA)) }
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.getUserInfo()
    }

    override fun onStop() {
        super.onStop()
        messageReceiver?.let { LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private val messageReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (userInfo?.isAuthenticated == true) {
                binding.toolbarHomeProfile.ivNotification.setImageDrawable(context?.let { ContextCompat.getDrawable(it,
                    R.drawable.toolbar_icon_notification_on) })
            }
        }
    }

    private fun setupClickListeners() {
        binding.apply {
            btnEnter.setOnClickListener {
                startActivityForResult(
                    LoginActivity.getLaunchIntent(activity as Context, false),
                    REQUEST_LOGIN_FROM_PROFILE
                )
            }
            toolbarHomeProfile.ivNotification.setSingleOnClickListener { navigateToNotifications() }
            btnPreferences.setOnClickListener { openProfilePreferences() }
            btnPaymentMethods.setOnClickListener { openPaymentMethods() }
//            btnSubscriptions.setOnClickListener { openUserSubscriptions() }
            btnContributions.setOnClickListener { openUserContributions() }
            btnCloseSession.setOnClickListener { showCloseSessionDialog() }
        }
    }

    private fun navigateToNotifications() {
        NotificationNavigation.dynamicStart?.let { startActivity(it) }
    }

    private fun openPaymentMethods() {
        startActivity(PaymentMethodsActivity.getLaunchIntent(activity as Context))
    }

    private fun openUserContributions() {
        startActivity(ContributionsActivity.getLaunchIntent(activity as Context))
    }

    private fun openUserSubscriptions() {
        startActivity(SubscriptionsActivity.getLaunchIntent(activity as Context))
    }

    private fun openProfilePreferences() {
        startActivityForResult(
            UserPreferencesActivity.getLaunchIntent(activity as Context),
            REQUEST_ADJUST_PROFILE
        )
    }

    private fun showCloseSessionDialog() {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(R.string.close_session))
                .setPositiveButton(getString(R.string.accept)) { _, _ ->
                    profileViewModel.apply {
                        deleteFcmToken(userInfo?.firebaseMessagingToken.orEmpty())
                        closeSession()
                    }
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun closeSessionObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {

            }
            is Completable.OnComplete -> {
                profileViewModel.getUserInfo()
            }
            is Completable.OnError -> {

            }
        }
    }

    private fun userInfoObserver(result: Result<UserInfo>?) {
        when (result) {
            is Result.OnSuccess -> {
                if (result.value.isAuthenticated) {
                    setViewAuthenticated(result.value)
                } else setViewNotAuthenticated()
            }
            is Result.OnError -> setViewNotAuthenticated()
        }
    }

    private fun setViewNotAuthenticated() {
        binding.apply {
            toolbarHomeProfile.tvUserName.text = getSpannedText(getString(R.string.home_toolbar_title_na))
            toolbarHomeProfile.ivNotification.isEnabled = false
            toolbarHomeProfile.ivNotification.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.toolbar_icon_notification_off) })
            toolbarHomeProfile.ivUserPhoto.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_default_photo) })
            btnEnter.isVisible = true
            tvNotAuthorized.isVisible = true
            llUserData.isVisible = false
        }
        userInfo = null
    }

    private fun setViewAuthenticated(user: UserInfo?) {
        binding.apply {
            if (user?.newNotificationsCounter != userInfo?.newNotificationsCounter) {
                if (user?.newNotificationsCounter!! > 0) {
                    toolbarHomeProfile.ivNotification.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.toolbar_icon_notification_on
                        )
                    })
                } else {
                    toolbarHomeProfile.ivNotification.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.toolbar_icon_notification_off
                        )
                    })
                }
            }
            if (user?.name != userInfo?.name) {
                toolbarHomeProfile.tvUserName.text =
                    getSpannedText(getString(R.string.home_toolbar_title, user?.name))
            }
            if (user?.userImage != userInfo?.userImage) {
                toolbarHomeProfile.ivUserPhoto.loadWithCircleCrop(
                    user?.userImage,
                    R.drawable.ic_default_photo
                )
            }
            toolbarHomeProfile.ivNotification.isEnabled = true
            btnEnter.isVisible = false
            tvNotAuthorized.isVisible = false
            llUserData.isVisible = true
        }
        userInfo = user
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_ADJUST_PROFILE -> profileViewModel.getUserInfo()
                REQUEST_LOGIN_FROM_PROFILE -> profileViewModel.getUserInfo()
            }
        }
    }

    override fun getLayoutId() = R.layout.fragment_profile

    companion object {
        private const val REQUEST_ADJUST_PROFILE = 61
    }
}

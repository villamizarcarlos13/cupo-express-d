package cl.trigo.core.storage.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import cl.trigo.core.storage.db.DbConstants.TABLE_BENEFICIARIES

@Entity(tableName = TABLE_BENEFICIARIES)
data class BeneficiaryEntity(
    @PrimaryKey val id: String,
    val firstname: String,
    val lastname: String,
    val photo: String,
    val rut: String,
    val email: String,
    val phone: String,
    val country: String,
    val city: String,
    val region: String,
    val comuna: String,
    val address: String,
    val isFavorite: Boolean,
    val active: Boolean?,
    val signedup: Boolean?,
    val lastDonationDate: String
)
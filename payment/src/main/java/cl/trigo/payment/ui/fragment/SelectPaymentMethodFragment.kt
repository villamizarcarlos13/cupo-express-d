package cl.trigo.payment.ui.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.*
import cl.trigo.payment.R
import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import cl.trigo.payment.ui.activity.AddPaymentMethodActivity
import cl.trigo.payment.ui.fragment.adapter.SelectPaymentMethodAdapter
import cl.trigo.payment.ui.fragment.listener.OnClickPaymentMethod
import cl.trigo.payment.ui.viewmodel.SelectPaymentMethodViewModel
import kotlinx.android.synthetic.main.fragment_select_payment_method.*
import kotlinx.android.synthetic.main.fragment_select_payment_method.btn_continue
import kotlinx.android.synthetic.main.fragment_select_payment_method.iv_beneficiary_image
import kotlinx.android.synthetic.main.fragment_select_payment_method.tv_beneficiary_name
import org.koin.androidx.viewmodel.ext.android.viewModel

class SelectPaymentMethodFragment : BaseFragment(), OnClickPaymentMethod {

    private val args: SelectPaymentMethodFragmentArgs by navArgs()
    private val viewModel by viewModel<SelectPaymentMethodViewModel>()
    private var paymentMethod: PaymentMethod? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(getPaymentMethodsLiveData, ::getPaymentMethodsObserver)
            observe(registerPaymentMethodLiveData, ::registerPaymentMethodObserver)
            getPaymentMethods(true)
        }
        initView()
    }

    private fun initView() {
        select_payment_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        payment_methods_list.layoutManager = LinearLayoutManager(context)
        tv_beneficiary_name.text =
            getSpannedText(getString(R.string.direct_contribution_title, args.beneficiaryName))
        iv_beneficiary_image?.loadWithCircleCrop(args.beneficiaryImage, R.drawable.ic_default_photo)
        tv_contribution_amount.text = formatAmount(args.benefit.price)
        btn_continue.setOnClickListener {
            if (paymentMethod != null) {
                val direction =
                    SelectPaymentMethodFragmentDirections.actionPaymentMethodToConfirmation(
                        args.beneficiaryName,
                        args.beneficiaryImage,
                        args.beneficiaryId,
                        args.benefit,
                        paymentMethod!!
                    )
                NavHostFragment.findNavController(this).navigate(direction)
            }
        }
        btn_add_payment_method.setSingleOnClickListener { viewModel.registerPaymentMethod() }
    }

    private fun navigateToAddPaymentWebView(data: RegisterPaymentMethod) {
        startActivityForResult(
            AddPaymentMethodActivity.getLaunchIntent(
                activity as Context,
                data.formAction,
                data.token
            ), REQUEST_ADD_PAYMENT_METHOD
        )
    }

    override fun onClickItem(paymentMethod: PaymentMethod) {
        this.paymentMethod = paymentMethod
    }

    override fun onClickDelete(paymentMethod: PaymentMethod) {}

    private fun registerPaymentMethodObserver(result: Result<RegisterPaymentMethod>?) {
        when (result) {
            is Result.OnLoading -> {

            }
            is Result.OnSuccess -> {
                navigateToAddPaymentWebView(result.value)
            }
            is Result.OnError -> {
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    private fun getPaymentMethodsObserver(result: Result<List<PaymentMethod>>?) {
        when (result) {
            is Result.OnLoading -> {

            }
            is Result.OnSuccess -> {
                payment_methods_list.adapter = SelectPaymentMethodAdapter(result.value.sortedBy { it.isLast }, this)
            }
            is Result.OnError -> {
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_ADD_PAYMENT_METHOD -> viewModel.getPaymentMethods(true)
            }
        }
    }


    override fun getLayoutId(): Int = R.layout.fragment_select_payment_method

    companion object {
        private const val REQUEST_ADD_PAYMENT_METHOD = 91
    }

}
package cl.trigo.cupoexpress.notifications.ui.activity

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import cl.trigo.core.base.BaseDynamicFeatureActivity
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.core.extension.setSingleOnClickListener
import cl.trigo.cupoexpress.notifications.R
import cl.trigo.cupoexpress.notifications.databinding.ActivityNotificationsBinding
import cl.trigo.cupoexpress.notifications.di.NotificationModule
import com.google.android.material.bottomsheet.BottomSheetBehavior


class NotificationsActivity : BaseDynamicFeatureActivity() {

    private lateinit var binding: ActivityNotificationsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        setupNavigation()
        setupUi()
        makeStatusBarTransparent()
        initBottomSheetBehavior()
    }

    private fun setupUi() {
        binding.btnClose.setSingleOnClickListener { this@NotificationsActivity.finish() }
    }

    override fun loadModules() {
        NotificationModule.load()
    }

    private fun setupNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
    }

    private fun initBottomSheetBehavior() {
        val bottomSheetBehavior = BottomSheetBehavior.from(binding.detailContainer)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.skipCollapsed = true
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    finish()
                    overridePendingTransition(0, 0)
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }

}
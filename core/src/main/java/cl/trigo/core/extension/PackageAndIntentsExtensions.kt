package cl.trigo.core.extension

import android.content.pm.PackageManager

private fun isMapsAvailable(packageManager: PackageManager): Boolean {
    val packageName = "com.google.android.apps.maps"
    val flag = 0
    return try {
        val appInfo = packageManager.getApplicationInfo(packageName, flag)
        appInfo.enabled
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }
}
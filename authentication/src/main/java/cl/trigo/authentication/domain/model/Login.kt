package cl.trigo.authentication.domain.model

data class Login(val idToken: String, val user: User, var status: Status)
package cl.trigo.cupoexpress.notifications.data.remote

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.notifications.data.model.entry.NotificationEntry
import cl.trigo.cupoexpress.notifications.data.net.NotificationService
import retrofit2.await

class NotificationRemoteDataSource(
    private val notificationService: NotificationService,
    private val appPreferences: AppPreferences
) : NotificationRemote {

    override suspend fun getAllNotifications(): Response<List<NotificationEntry>> =
        notificationService.getAllNotifications(appPreferences.authToken!!).await()

    override suspend fun markAsReadNotification(id: String): ResponseCompletable =
        notificationService.markAsRead(appPreferences.authToken!!, id).await()

    override suspend fun deleteNotification(id: String): ResponseCompletable =
        notificationService.deleteNotificationById(appPreferences.authToken!!, id).await()

}
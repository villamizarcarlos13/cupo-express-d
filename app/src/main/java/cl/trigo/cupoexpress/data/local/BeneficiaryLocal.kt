package cl.trigo.cupoexpress.data.local

import cl.trigo.core.storage.entity.BeneficiaryEntity
import cl.trigo.cupoexpress.domain.model.UserInfo

interface BeneficiaryLocal {
    suspend fun getUserInfo(): UserInfo
    suspend fun saveUserInfoToPreferences(name: String, userImage: String)
    suspend fun updateNotification()
    suspend fun getShowCommunesPrompt(): Boolean
    suspend fun getBeneficiaries(): List<BeneficiaryEntity>
    suspend fun insertBeneficiariesList(beneficiaries: List<BeneficiaryEntity>)
    suspend fun getBeneficiaryById(id: String): BeneficiaryEntity
    suspend fun insertBeneficiary(beneficiary: BeneficiaryEntity)
    suspend fun deleteAllBeneficiary()
    suspend fun deleteBeneficiaryById(id: String)
}
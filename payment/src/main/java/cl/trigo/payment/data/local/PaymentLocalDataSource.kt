package cl.trigo.payment.data.local

import cl.trigo.core.storage.dao.PaymentMethodsDao
import cl.trigo.core.storage.entity.PaymentMethodEntity
import cl.trigo.core.util.AppPreferences

class PaymentLocalDataSource(
    private val appPreferences: AppPreferences,
    private val paymentMethodsDao: PaymentMethodsDao
) : PaymentLocal {
    override suspend fun getAllPaymentMethods(): List<PaymentMethodEntity> =
        paymentMethodsDao.getPaymentMethods()

    override suspend fun insertPaymentMethodsList(methods: List<PaymentMethodEntity>) =
        paymentMethodsDao.savePaymentMethods(methods)

    override suspend fun deletePaymentMethod(id: String) {
        paymentMethodsDao.deleteMethodById(id)
    }

    override suspend fun deleteAllPaymentsMethods() {
        paymentMethodsDao.deleteAllPaymentMethods()
    }

}
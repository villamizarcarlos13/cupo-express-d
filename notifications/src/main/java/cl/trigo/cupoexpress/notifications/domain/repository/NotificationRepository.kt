package cl.trigo.cupoexpress.notifications.domain.repository


import cl.trigo.cupoexpress.notifications.domain.model.NotificationModel
import kotlinx.coroutines.flow.Flow

interface NotificationRepository {

    suspend fun getAllNotifications(forceUpdate: Boolean): Flow<List<NotificationModel>>

    suspend fun markAsReadNotification(id: String)

    suspend fun deleteNotification(id: String)

}
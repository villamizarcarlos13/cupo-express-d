package cl.trigo.payment.di

import cl.trigo.core.storage.db.CupoExpressDatabase
import cl.trigo.core.util.AppPreferences
import cl.trigo.payment.data.PaymentDataRepository
import cl.trigo.payment.data.local.PaymentLocal
import cl.trigo.payment.data.local.PaymentLocalDataSource
import cl.trigo.payment.data.mapper.PaymentDataMapper
import cl.trigo.payment.data.net.PaymentService
import cl.trigo.payment.data.remote.PaymentRemote
import cl.trigo.payment.data.remote.PaymentRemoteDataSource
import cl.trigo.payment.domain.repository.PaymentRepository
import cl.trigo.payment.domain.usecase.ConfirmPaymentUseCase
import cl.trigo.payment.domain.usecase.GetBenefitsUseCase
import cl.trigo.payment.domain.usecase.GetPaymentMethodsUseCase
import cl.trigo.payment.domain.usecase.RegisterPaymentMethodUseCase
import cl.trigo.payment.ui.viewmodel.ConfirmPaymentViewModel
import cl.trigo.payment.ui.viewmodel.GetBenefitsViewModel
import cl.trigo.payment.ui.viewmodel.PaymentMethodsViewModel
import cl.trigo.payment.ui.viewmodel.SelectPaymentMethodViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val paymentModule = module {

    /* API REST Remote */
    single { get<Retrofit>().create(PaymentService::class.java) as PaymentService }

    /* DataSources*/
    factory<PaymentRemote> { PaymentRemoteDataSource(AppPreferences, get()) }
    factory<PaymentLocal> { PaymentLocalDataSource(AppPreferences, get()) }

    /* Repositories */
    factory<PaymentRepository> { PaymentDataRepository(get(), get(), get()) }

    /* Dao Interfaces */
    factory { get<CupoExpressDatabase>().paymentMethodsDao()}

    /* Mappers */
    factory { PaymentDataMapper() }

    /* UseCases */
    factory { GetPaymentMethodsUseCase(get()) }
    factory { RegisterPaymentMethodUseCase(get()) }
    factory { ConfirmPaymentUseCase(get()) }
    factory { GetBenefitsUseCase(get()) }

    /* View Models */
    viewModel { SelectPaymentMethodViewModel(get(), get()) }
    viewModel { PaymentMethodsViewModel(get(), get(), get()) }
    viewModel { ConfirmPaymentViewModel(get()) }
    viewModel { GetBenefitsViewModel(get()) }

}
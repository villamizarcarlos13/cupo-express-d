package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.Communes
import cl.trigo.cupoexpress.domain.model.Kiosk
import cl.trigo.cupoexpress.domain.usecase.GetCommunesUseCase
import cl.trigo.cupoexpress.domain.usecase.GetKiosksUseCase

class KiosksViewModel(
    private val getKiosksUseCase: GetKiosksUseCase,
    private val getCommunesUseCase: GetCommunesUseCase
) : ViewModel() {
    val getKiosksLiveData = LiveResult<List<Kiosk>>()
    val getCommuneLiveData = LiveResult<Communes>()

    fun getKiosks() =
        getKiosksUseCase.execute(getKiosksLiveData)

    fun getCommunes() =
        getCommunesUseCase.execute(getCommuneLiveData)

}
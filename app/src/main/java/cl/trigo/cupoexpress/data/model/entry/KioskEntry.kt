package cl.trigo.cupoexpress.data.model.entry

data class KioskEntry(
    val id: String?,
    val name: String?,
    val country: String?,
    val region: String?,
    val commune: String?,
    val province: String?,
    val address: String?,
    val image: String?,
    val description: String?,
    val isActive: Boolean?,
    val lat: Double?,
    val lng: Double?
)

fun KioskEntry.toKiosk() = cl.trigo.cupoexpress.domain.model.Kiosk(
    id = id.orEmpty(),
    name = name.orEmpty(),
    country = country.orEmpty(),
    region = region.orEmpty(),
    commune = commune.orEmpty(),
    province = province.orEmpty(),
    address = address.orEmpty(),
    image = image.orEmpty(),
    description = description.orEmpty(),
    isActive = isActive ?: false,
    lat = lat ?: 0.0,
    lng = lng ?: 0.0
)
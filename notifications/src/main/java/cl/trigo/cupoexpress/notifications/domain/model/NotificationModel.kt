package cl.trigo.cupoexpress.notifications.domain.model

data class NotificationModel (
    val id: String,
    val title: String,
    val body: String,
    val iconUrl: String,
    val dateCreated: Long,
    val isRead: Boolean
)
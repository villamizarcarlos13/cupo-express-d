package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUseCase
import cl.trigo.cupoexpress.domain.model.UserEditResponse
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class EditUserDataUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUseCase<Map<String, String?>, UserEditResponse> (
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    companion object {
        const val FIRST_NAME_PARAM = "FIRST_NAME_PARAM"
        const val LAST_NAME_PARAM = "LAST_NAME_PARAM"
        const val EMAIL_PARAM = "EMAIL_PARAM"
        const val PHONE_PARAM = "PHONE_PARAM"
        const val PHOTO_PARAM = "PHOTO_PARAM"
    }

    override suspend fun executeOnBackground(params: Map<String, String?>) : UserEditResponse? {
        val firstName = params.getValue(FIRST_NAME_PARAM)
        val lastName = params.getValue(LAST_NAME_PARAM)
        val email = params.getValue(EMAIL_PARAM)
        val phone = params.getValue(PHONE_PARAM)
        val photo = params.getValue(EditBeneficiaryUseCase.PHOTO_PARAM)

        val saveUser = beneficiaryRepository.editUserData(firstName, lastName, email, phone, photo)

        beneficiaryRepository.saveUserInfoToPreferences(firstName.orEmpty(), saveUser.url)
        return saveUser
    }
}
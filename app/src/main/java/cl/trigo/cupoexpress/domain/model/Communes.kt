package cl.trigo.cupoexpress.domain.model

data class Communes (
    val communes : List<Commune>,
    val showPrompt: Boolean = false
)
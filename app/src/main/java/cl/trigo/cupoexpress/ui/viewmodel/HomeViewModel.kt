package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.CheckVersion
import cl.trigo.cupoexpress.domain.usecase.CheckVersionUseCase
import cl.trigo.cupoexpress.domain.usecase.SaveFcmTokenUseCase
import cl.trigo.cupoexpress.domain.usecase.UpdateNotificationUseCase

class HomeViewModel (
    private val checkVersionUseCase: CheckVersionUseCase,
    private val saveFcmTokenUseCase: SaveFcmTokenUseCase,
    private val updateNotificationUseCase: UpdateNotificationUseCase
) : ViewModel() {

    val checkVersionLiveData = LiveResult<CheckVersion>()
    val saveToken = LiveCompletable()
    val updateNotification = LiveCompletable()

    fun checkVersion() = checkVersionUseCase.execute(checkVersionLiveData)

    fun saveFcmToken(token: String) = saveFcmTokenUseCase.execute(saveToken, token)

    fun updateNotification() = updateNotificationUseCase.execute(updateNotification)

}

package cl.trigo.cupoexpress.ui.fragment.listener

import cl.trigo.cupoexpress.domain.model.Beneficiary

interface OnLongClickBeneficiary {
    fun onLongClick(beneficiary: Beneficiary): Unit
}
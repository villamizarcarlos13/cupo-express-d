package cl.trigo.cupoexpress.notifications.data.local

import cl.trigo.core.storage.entity.NotificationEntity

interface NotificationLocal {

    suspend fun getAllNotifications(): List<NotificationEntity>

    suspend fun insertNotificationsList(notifications: List<NotificationEntity>)

    suspend fun markAsReadNotification(id: String)

    suspend fun deleteNotification(id: String)

    suspend fun deleteAllNotifications()

    suspend fun updateNotificationCount(count: Int)
}
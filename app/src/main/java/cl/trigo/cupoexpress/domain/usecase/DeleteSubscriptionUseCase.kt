package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.CompletableUseCase
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class DeleteSubscriptionUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : CompletableUseCase<String>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(params: String) {
        beneficiaryRepository.deleteSubscription(id = params)
    }
}
package cl.trigo.cupoexpress.notifications.data.net

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.cupoexpress.notifications.data.model.entry.NotificationEntry
import retrofit2.Call
import retrofit2.http.*

interface NotificationService {

    @GET("notifications")
    fun getAllNotifications(
        @Header("Authorization") authorization: String
    ): Call<Response<List<NotificationEntry>>>

    @Headers("Accept: application/json")
    @DELETE("notifications/{id}")
    fun deleteNotificationById(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @PATCH("notifications/{id}?isRead=true")
    fun markAsRead(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
    ): Call<ResponseCompletable>

}
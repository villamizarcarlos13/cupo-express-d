package cl.trigo.core.extension

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream


fun encodeBitmapToString(bitmap: Bitmap?): String {
    return try {
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 30, baos)
        val imageBytes = baos.toByteArray()
        Base64.encodeToString(imageBytes, Base64.DEFAULT)
    } catch (e: Exception) { "" }
}

fun decode(imageString: String): Bitmap {
    val imageBytes = Base64.decode(imageString, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
}




package cl.trigo.core.base

import androidx.annotation.Keep

@Keep
data class Response<T>(
    val status: Boolean,
    val statusCode: Int,
    val description: String,
    var data: T
)
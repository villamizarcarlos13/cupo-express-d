package cl.trigo.payment.data.remote

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.payment.data.model.entry.BenefitEntry
import cl.trigo.payment.data.model.entry.PaymentMethodEntry
import cl.trigo.payment.data.model.entry.RegisterPaymentMethodEntry

interface PaymentRemote {
    suspend fun getPaymentMethods(): Response<List<PaymentMethodEntry>>
    suspend fun registerPaymentMethod(): Response<RegisterPaymentMethodEntry>
    suspend fun deletePaymentMethodById(id: String): ResponseCompletable
    suspend fun confirmPayment(
        benefitId: String,
        beneficiaryId: String,
        quantity: Int,
        paymentMethodId: String
    ): ResponseCompletable

    suspend fun getBenefits(): Response<List<BenefitEntry>>
}
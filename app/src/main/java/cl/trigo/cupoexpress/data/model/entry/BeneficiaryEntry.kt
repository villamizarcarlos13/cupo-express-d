package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.Beneficiary
import cl.trigo.cupoexpress.domain.model.LastBenefactor

@Keep
data class BeneficiaryEntry(
    val id: String?,
    val firstname: String?,
    val lastname: String?,
    val photo: String?,
    val rut: String?,
    val email: String?,
    val phone: String?,
    val country: String?,
    val city: String?,
    val region: String?,
    val comuna: String?,
    val address: String?,
    val isFavorite: Boolean,
    val active: Boolean?,
    val signedup: Boolean?,
    val lastBenefactors: List<LastBenefactorEntry?>?,
    val lastDonationDate: String?
)

fun BeneficiaryEntry.toBeneficiary() = Beneficiary(
    id = id.orEmpty(),
    firstname = firstname,
    lastname = lastname,
    fullname = "$firstname $lastname",
    photo = photo.orEmpty(),
    rut = rut,
    email = email.orEmpty(),
    phone = phone.orEmpty(),
    country = country,
    city = city,
    region = region,
    commune = comuna,
    address = address,
    isFavorite = isFavorite,
    active = active,
    signedup = signedup,
    lastBenefactors = lastBenefactors?.map {
        it?.toLastBenefactor() ?: LastBenefactor(
            "",
            0,
            "",
            "",
            ""
        )
    }.orEmpty(),
    lastDonationDate = lastDonationDate
)
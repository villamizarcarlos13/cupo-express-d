package cl.trigo.cupoexpress.ui.fragment.adapter.diffutil

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import cl.trigo.cupoexpress.domain.model.Beneficiary

class BenefitedDiffCallback (private val oldList: List<Beneficiary>, private val newList: List<Beneficiary>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val new = newList[newItemPosition]
        val old = oldList[oldItemPosition]
        val isIdTheSame = old.id == new.id
        val isNameTheSame = old.firstname == new.firstname
        val isLastNameTheSame = old.lastname == new.lastname
        val isPhotoTheSame = old.photo == new.photo
        val isFavoriteTheSame = old.isFavorite == new.isFavorite
        val isLastDonationTheSame = old.lastDonationDate == new.lastDonationDate
        return isIdTheSame && isNameTheSame && isLastNameTheSame && isPhotoTheSame && isFavoriteTheSame && isLastDonationTheSame
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val (_, value, name) = oldList[oldPosition]
        val (_, value1, name1) = newList[newPosition]

        return name == name1 && value == value1
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val new = newList[newItemPosition]
        val old = oldList[oldItemPosition]
        val set = mutableSetOf<String>()

        val isIdTheSame = old.id == new.id
        val isNameTheSame = old.firstname == new.firstname
        val isLastNameTheSame = old.lastname == new.lastname
        val isPhotoTheSame = old.photo == new.photo
        val isFavoriteTheSame = old.isFavorite == new.isFavorite
        val isLastDonationTheSame = old.lastDonationDate == new.lastDonationDate

        if(isIdTheSame.not()) {
            set.add(ID)
        }
        if(isNameTheSame.not()) {
            set.add(NAME)
        }
        if(isLastNameTheSame.not()) {
            set.add(LAST_NAME)
        }
        if(isPhotoTheSame.not()) {
            set.add(PHOTO)
        }
        if(isFavoriteTheSame.not()) {
            set.add(IS_FAVORITE)
        }
        if(isLastDonationTheSame.not()) {
            set.add(LAST_DONATION)
        }
        return set
    }

    companion object {
        const val ID = "ID"
        const val NAME = "NAME"
        const val LAST_NAME = "LAST_NAME"
        const val PHOTO = "PHOTO"
        const val IS_FAVORITE = "IS_FAVORITE"
        const val LAST_DONATION = "LAST_DONATION"
    }
}
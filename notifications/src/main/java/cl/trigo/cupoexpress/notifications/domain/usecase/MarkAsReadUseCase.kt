package cl.trigo.cupoexpress.notifications.domain.usecase

import cl.trigo.cupoexpress.notifications.domain.repository.NotificationRepository

class MarkAsReadUseCase(private val repository: NotificationRepository) {
    suspend fun markAsRead(id: String) = repository.markAsReadNotification(id)
}
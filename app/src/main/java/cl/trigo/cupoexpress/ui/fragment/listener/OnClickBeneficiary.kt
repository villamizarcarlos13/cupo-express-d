package cl.trigo.cupoexpress.ui.fragment.listener

import cl.trigo.cupoexpress.domain.model.Beneficiary

interface OnClickBeneficiary {
    fun onClick(beneficiary: Beneficiary): Unit
}
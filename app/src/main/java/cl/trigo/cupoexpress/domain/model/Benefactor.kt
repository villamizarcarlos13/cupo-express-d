package cl.trigo.cupoexpress.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Benefactor(
    val id: String,
    val firstName: String,
    val lastName: String,
    val profileUrl: String,
    val fullName: String,
    val region: String,
    val country: String,
    val email: String,
    val rut: String,
    val phone: String
) : Parcelable
package cl.trigo.authentication.domain.usecase

import cl.trigo.core.coroutines.ResultUseCase
import cl.trigo.authentication.domain.model.LoginRut
import cl.trigo.authentication.domain.repository.LoginRepository
import kotlinx.coroutines.Dispatchers

class GetUserByRutUseCase(
    private val loginRepository: LoginRepository
) : ResultUseCase<String, LoginRut>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(params: String): LoginRut? {
        return loginRepository.verifyUserByRut(rut = params)
    }
}
package cl.trigo.cupoexpress.domain.model

data class Subscription(
    val id: String?,
    val firstname: String?,
    val lastname: String?,
    val fullname: String?,
    val amount: String?
)
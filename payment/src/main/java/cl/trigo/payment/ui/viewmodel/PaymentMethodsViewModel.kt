package cl.trigo.payment.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.trigo.core.extension.*
import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import cl.trigo.payment.domain.usecase.DeletePaymentMethodUseCase
import cl.trigo.payment.domain.usecase.GetPaymentMethodsUseCase
import cl.trigo.payment.domain.usecase.RegisterPaymentMethodUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class PaymentMethodsViewModel(
    private val deletePaymentMethodUseCase: DeletePaymentMethodUseCase,
    private val getPaymentMethodsUseCase: GetPaymentMethodsUseCase,
    private val registerPaymentMethodUseCase: RegisterPaymentMethodUseCase
) : ViewModel(

) {
    val deletePaymentMethodLiveData = LiveCompletable()
    val getPaymentMethodsLiveData = LiveResult<List<PaymentMethod>>()
    val registerPaymentMethodLiveData = LiveResult<RegisterPaymentMethod>()

    fun deletePaymentMethod(id: String) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            deletePaymentMethodUseCase.deletePaymentMethod(id)
        }.onFailure {
            deletePaymentMethodLiveData.postThrowable(it) }
            .onSuccess {
                deletePaymentMethodLiveData.postComplete() }
    }

    fun registerPaymentMethod() =
        registerPaymentMethodUseCase.execute(registerPaymentMethodLiveData)

    fun getPaymentMethods(forceUpdate: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            getPaymentMethodsUseCase.getPaymentMethods(forceUpdate)
                .onStart { getPaymentMethodsLiveData.postLoading() }
                .collect { getPaymentMethodsLiveData.postSuccess(it) }
        }
            .onFailure { getPaymentMethodsLiveData.postThrowable(it) }
    }
}
package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.trigo.cupoexpress.domain.model.Beneficiary
import cl.trigo.authentication.domain.usecase.CloseSessionUseCase
import cl.trigo.core.extension.*
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.domain.usecase.DeleteBeneficiaryByIdUseCase
import cl.trigo.cupoexpress.domain.usecase.GetBeneficiariesUseCase
import cl.trigo.cupoexpress.domain.usecase.GetUserInfoUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class BeneficiaryViewModel(
    private val getBeneficiariesUseCase: GetBeneficiariesUseCase,
    private val deleteBeneficiaryByIdUseCase: DeleteBeneficiaryByIdUseCase,
    private val closeSessionUseCase: CloseSessionUseCase,
    private val getUserInfoUseCase: GetUserInfoUseCase
) : ViewModel() {

    val getBeneficiariesLiveData = LiveResult<List<Beneficiary>>()
    val deleteBeneficiaryLiveData = LiveCompletable()
    val closeSessionLiveData = LiveCompletable()
    val userInfoLiveData = LiveResult<UserInfo>()

    fun getBeneficiaries(forceUpdate: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            getBeneficiariesUseCase.getBeneficiaries(forceUpdate)
                .onStart { getBeneficiariesLiveData.postLoading() }
                .collect { getBeneficiariesLiveData.postSuccess(it) }
        }
            .onFailure { getBeneficiariesLiveData.postThrowable(it) }
    }

    fun deleteBeneficiaryById(beneficiaryId: String) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            deleteBeneficiaryByIdUseCase.deleteBeneficiary(beneficiaryId)
        }.onFailure { deleteBeneficiaryLiveData.postThrowable(it) }
            .onSuccess { deleteBeneficiaryLiveData.postComplete() }
    }

    fun closeSession() = closeSessionUseCase.execute(closeSessionLiveData)
    fun getUserInfo() = getUserInfoUseCase.execute(userInfoLiveData)
}
package cl.trigo.core.navigation

import android.content.Intent
import cl.trigo.core.navigation.loaders.loadIntentOrNull

object NotificationNavigation : DynamicFeature<Intent> {

    private const val NOTIFICATIONS =
        "cl.trigo.cupoexpress.notifications.ui.activity.NotificationsActivity"

    override val dynamicStart: Intent?
        get() = NOTIFICATIONS.loadIntentOrNull()

}
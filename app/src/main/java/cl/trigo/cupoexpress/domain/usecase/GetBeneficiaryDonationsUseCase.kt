package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUseCase
import cl.trigo.cupoexpress.domain.model.Donation
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetBeneficiaryDonationsUseCase (
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUseCase<String, List<Donation>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(params: String): List<Donation> {
        return beneficiaryRepository.getBeneficiaryDonationsById(rut = params)
    }
}
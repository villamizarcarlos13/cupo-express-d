package cl.trigo.cupoexpress.ui.fragment.listener

import cl.trigo.cupoexpress.domain.model.Contribution

interface OnClickContribution {
    fun onClickItem(contribution: Contribution): Unit
}
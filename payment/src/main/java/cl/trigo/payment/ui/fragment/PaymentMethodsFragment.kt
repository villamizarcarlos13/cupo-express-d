package cl.trigo.payment.ui.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.exceptions.DeletePaymentMethodException
import cl.trigo.core.extension.observe
import cl.trigo.core.extension.setSingleOnClickListener
import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.R
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import cl.trigo.payment.ui.activity.AddPaymentMethodActivity
import cl.trigo.payment.ui.fragment.adapter.PaymentMethodsAdapter
import cl.trigo.payment.ui.fragment.listener.OnClickPaymentMethod
import cl.trigo.payment.ui.viewmodel.PaymentMethodsViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_payment_methods.*
import kotlinx.android.synthetic.main.fragment_payment_methods.btn_add_payment_method
import kotlinx.android.synthetic.main.fragment_payment_methods.payment_methods_list
import org.koin.androidx.viewmodel.ext.android.viewModel

class PaymentMethodsFragment : BaseFragment(), OnClickPaymentMethod {

    private val viewModel by viewModel<PaymentMethodsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(getPaymentMethodsLiveData, ::getPaymentMethodsObserver)
            observe(deletePaymentMethodLiveData, ::deletePaymentMethodObserver)
            observe(registerPaymentMethodLiveData, ::registerPaymentMethodObserver)
            getPaymentMethods(true)
        }
        payment_methods_list.layoutManager = LinearLayoutManager(context)
        btn_add_payment_method.setSingleOnClickListener { viewModel.registerPaymentMethod() }
    }

    private fun showDeleteDialog(paymentMethod: PaymentMethod) {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.delete_payment_method))
                .setMessage(
                    getString(
                        R.string.delete_payment_method_message,
                        paymentMethod.lastCardDigits
                    )
                )
                .setPositiveButton(getString(R.string.delete)) { _, _ ->
                    viewModel.deletePaymentMethod(paymentMethod.id)
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun navigateToAddPaymentWebView(data: RegisterPaymentMethod) {
        startActivityForResult(
            AddPaymentMethodActivity.getLaunchIntent(
                activity as Context,
                data.formAction,
                data.token
            ), REQUEST_ADD_PAYMENT_METHOD
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_ADD_PAYMENT_METHOD -> viewModel.getPaymentMethods(true)
            }
        }
    }

    private fun registerPaymentMethodObserver(result: Result<RegisterPaymentMethod>?) {
        when (result) {
            is Result.OnLoading -> {

            }
            is Result.OnSuccess -> {
                navigateToAddPaymentWebView(result.value)
            }
            is Result.OnError -> {
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    private fun deletePaymentMethodObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {

            }
            is Completable.OnComplete -> {
                viewModel.getPaymentMethods(false)
            }
            is Completable.OnError -> {
                var errorMessage = result.throwable.message.orEmpty()
                when (result.throwable) {
                    is DeletePaymentMethodException -> {
                        errorMessage = resources.getString(R.string.delete_payment_method_error)
                    }
                }
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    private fun getPaymentMethodsObserver(result: Result<List<PaymentMethod>>?) {
        when (result) {
            is Result.OnLoading -> {

            }
            is Result.OnSuccess -> {
                if (result.value.isEmpty()) {
                    payment_methods_list.isVisible = false
                    tv_no_methods.isVisible = true
                } else {
                    payment_methods_list.isVisible = true
                    tv_no_methods.isVisible = false
                    payment_methods_list.adapter = PaymentMethodsAdapter(result.value.sortedBy { it.isLast }, this)
                }
            }
            is Result.OnError -> {
                payment_methods_list.isVisible = false
                tv_no_methods.isVisible = true
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    override fun onClickItem(paymentMethod: PaymentMethod) {

    }

    override fun onClickDelete(paymentMethod: PaymentMethod) {
        showDeleteDialog(paymentMethod)
    }

    override fun getLayoutId(): Int = R.layout.fragment_payment_methods

    companion object {
        private const val REQUEST_ADD_PAYMENT_METHOD = 92
    }
}



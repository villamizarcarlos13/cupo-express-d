package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.cupoexpress.domain.model.Beneficiary
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.flow.Flow

class GetBeneficiariesUseCase(private val beneficiaryRepository: BeneficiaryRepository) {
    suspend fun getBeneficiaries(forceUpdate: Boolean): Flow<List<Beneficiary>> {
        return beneficiaryRepository.getBeneficiaries(forceUpdate)
    }
}
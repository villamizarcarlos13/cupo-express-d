package cl.trigo.cupoexpress.domain.model

data class UserInfo (
    val name: String?,
    val userImage: String?,
    val firebaseMessagingToken: String,
    val newNotificationsCounter: Int = 0,
    val isAuthenticated: Boolean = false
)
package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.viewpager.widget.ViewPager
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.fragment.WelcomeFragment1
import cl.trigo.cupoexpress.ui.fragment.WelcomeFragment2
import cl.trigo.cupoexpress.ui.fragment.WelcomeFragment3
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity: AppCompatActivity() {

    private val STEP_1 = 0
    private val STEP_2 = 1
    private val STEP_3 = 2

    private lateinit var welcomeViewPager: ViewPager
    private lateinit var indicatorTabLayout: TabLayout
    private lateinit var adapter : IndicationsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        makeStatusBarTransparent()
        setupNavigation()
    }

    private fun setupNavigation() {
        welcomeViewPager = findViewById<ViewPager>(R.id.welcome_step_viewpager)
        indicatorTabLayout = findViewById<TabLayout>(R.id.indicator_btns)
        adapter = IndicationsAdapter(supportFragmentManager)
        adapter.addFragments(arrayListOf<Fragment>(WelcomeFragment1(), WelcomeFragment2(), WelcomeFragment3()))
        welcomeViewPager.adapter = adapter
        indicatorTabLayout.setupWithViewPager(welcomeViewPager)

        btn_show_program.isSelected = false
        btn_show_program.setOnClickListener{
            when(welcomeViewPager.currentItem){
                STEP_1 -> {
                    welcomeViewPager.setCurrentItem(STEP_2, true)
                    btn_show_program.isSelected = false
                }
                STEP_2 -> {
                    welcomeViewPager.setCurrentItem(STEP_3, true)
                    btn_show_program.isSelected = true
                }
                else -> {
                    btn_show_program.isSelected = false
                    finish()
                }
            }
        }

    }

    companion object {
        fun getLaunchIntent(context: Context): Intent =
            Intent(context, WelcomeActivity::class.java)
    }

    class IndicationsAdapter(fm: FragmentManager):
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val fragmentList : MutableList<Fragment> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragments(fragments: List<Fragment>){
            fragmentList.addAll(fragments)
        }


    }
}
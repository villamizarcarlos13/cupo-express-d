package cl.trigo.authentication.domain.usecase

import cl.trigo.authentication.domain.repository.LoginRepository
import cl.trigo.core.coroutines.CompletableUseCase
import kotlinx.coroutines.Dispatchers

class RestorePasswordUseCase (
     private val loginRepository: LoginRepository
) : CompletableUseCase<String>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(params: String) {
        loginRepository.restorePassword(params)
    }
}
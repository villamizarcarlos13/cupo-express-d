package cl.trigo.cupoexpress.notifications.data.remote

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.cupoexpress.notifications.data.model.entry.NotificationEntry

interface NotificationRemote {

    suspend fun getAllNotifications(): Response<List<NotificationEntry>>

    suspend fun markAsReadNotification(id: String): ResponseCompletable

    suspend fun deleteNotification(id: String): ResponseCompletable

}
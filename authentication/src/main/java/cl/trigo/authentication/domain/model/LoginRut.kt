package cl.trigo.authentication.domain.model

data class LoginRut(val user: UserRut, var status: Status)
package cl.trigo.cupoexpress.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import cl.trigo.core.storage.entity.BeneficiaryEntity
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Beneficiary(
    val id: String,
    val firstname: String?,
    val lastname: String?,
    val fullname: String?,
    val photo: String,
    val rut: String?,
    val email: String,
    val phone: String,
    val country: String?,
    val city: String?,
    val region: String?,
    val commune: String?,
    val address: String?,
    val isFavorite: Boolean,
    val active: Boolean?,
    val signedup: Boolean?,
    val lastBenefactors: List<LastBenefactor>,
    val lastDonationDate: String?
) : Parcelable
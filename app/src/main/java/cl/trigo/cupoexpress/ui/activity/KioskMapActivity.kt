package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.common.BottomSheetDetailFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.core.extension.observe
import cl.trigo.cupoexpress.AppConstants.COMMUNE_ZOOM_LVL
import cl.trigo.cupoexpress.AppConstants.GOOGLE_MAPS_PACKAGE
import cl.trigo.cupoexpress.AppConstants.GOOGLE_NAVIGATION_REQUEST
import cl.trigo.cupoexpress.AppConstants.RENCA
import cl.trigo.cupoexpress.AppConstants.RENCA_LAT
import cl.trigo.cupoexpress.AppConstants.RENCA_LNG
import cl.trigo.cupoexpress.AppConstants.SANTIAGO_LAT
import cl.trigo.cupoexpress.AppConstants.SANTIAGO_LNG
import cl.trigo.cupoexpress.AppConstants.SANTIAGO_MARKER_ZOOM_LVL
import cl.trigo.cupoexpress.AppConstants.SAN_JOAQUIN
import cl.trigo.cupoexpress.AppConstants.SAN_JOAQUIN_LAT
import cl.trigo.cupoexpress.AppConstants.SAN_JOAQUIN_LNG
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Commune
import cl.trigo.cupoexpress.domain.model.Communes

import cl.trigo.cupoexpress.domain.model.Kiosk
import cl.trigo.cupoexpress.ui.fragment.adapter.CommuneMapAdapter
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickCommune
import cl.trigo.cupoexpress.ui.viewmodel.KiosksViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_kiosk_map.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class KioskMapActivity : AppCompatActivity(), OnMapReadyCallback, OnMarkerClickListener,
    OnClickCommune {

    private lateinit var map: GoogleMap
    private val viewModel by viewModel<KiosksViewModel>()
    private val kiosks: MutableList<Kiosk> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kiosk_map)
        makeStatusBarTransparent()
        setupUi()
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        with(viewModel) {
            observe(getKiosksLiveData, ::kiosksObserver)
            observe(getCommuneLiveData, ::communesObserver)
            getKiosks()
            getCommunes()
        }
    }

    private fun setupUi() {
        rv_kiosks.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        kiosks_map_top_bar.setOnClickListener { this.onBackPressed() }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        moveCameraToDefaultPosition()
    }

    private fun moveCameraToDefaultPosition() {
        val santiago = LatLng(SANTIAGO_LAT, SANTIAGO_LNG)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(santiago, SANTIAGO_MARKER_ZOOM_LVL))
        if (kiosks.isNotEmpty()) {
            showMarkers()
        }
    }

    private fun showMarkers() {
        for (kiosk in kiosks) {
            val marker = map.addMarker(
                MarkerOptions()
                    .position(LatLng(kiosk.lat, kiosk.lng))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin))
            )
            marker.tag = kiosk.id
        }
        map.setOnMarkerClickListener(this)
    }

    private fun showFilteredMarkers(communeName: String) {
       val kiosksInCommune =  kiosks.filter { it.commune.contains(communeName) }
        for (kiosk in kiosksInCommune) {
            val marker = map.addMarker(
                MarkerOptions()
                    .position(LatLng(kiosk.lat, kiosk.lng))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin))
            )
            marker.tag = kiosk.id
        }
        map.setOnMarkerClickListener(this)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        val filteredKiosks = kiosks.filter { it.id == marker?.tag }
        if (filteredKiosks.isNotEmpty()) {
            showKioskDetail(filteredKiosks.first())
        }
        return false
    }

    override fun onClickItem(commune: Commune, isSelected: Boolean) {
        rv_kiosks?.adapter?.notifyDataSetChanged()
        if (isSelected) {
            moveCameraToCommune(commune)
        } else {
            moveCameraToDefaultPosition()
        }
    }

    private fun showKioskDetail(kiosk: Kiosk) {
        val bottomSheet =
            BottomSheetDetailFragment.newInstance(
                kiosk.name,
                kiosk.address,
                kiosk.description,
                kiosk.image,
                resources.getString(R.string.get_rout)
            )
        bottomSheet.setActionClickListener { showNavigationToKiosk(kiosk) }
        Handler().postDelayed({ bottomSheet.show(supportFragmentManager, bottomSheet.tag) }, 800)
    }

    private fun moveCameraToCommune(commune: Commune) {
        when (commune.name) {
            RENCA -> {
                map.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(RENCA_LAT, RENCA_LNG),
                        COMMUNE_ZOOM_LVL
                    )
                )
                showFilteredMarkers(commune.name)
            }
            SAN_JOAQUIN -> {
                map.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            SAN_JOAQUIN_LAT,
                            SAN_JOAQUIN_LNG
                        ), COMMUNE_ZOOM_LVL
                    )
                )
                showFilteredMarkers(commune.name)
            }
            else -> {
                moveCameraToDefaultPosition()
            }
        }
    }

    private fun showNavigationToKiosk(kiosk: Kiosk){
        val gmmIntentUri: Uri = Uri.parse(GOOGLE_NAVIGATION_REQUEST + kiosk.lat + "," + kiosk.lng)
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.let {
            it.setPackage(GOOGLE_MAPS_PACKAGE)
            startActivity(it)
        }
    }

    private fun showLoading(isLoading: Boolean) {
        kiosks_loading.isVisible = isLoading
    }

    private fun communesObserver(result: Result<Communes>?) {
        when (result) {
            is Result.OnLoading -> { showLoading(true) }
            is Result.OnSuccess -> {
                showLoading(false)
                ll_communes.isVisible = true
                rv_kiosks.adapter = CommuneMapAdapter(result.value.communes, this)
            }
            is Result.OnError -> {
                showLoading(false)
            }
            else -> {
            }
        }
    }

    private fun kiosksObserver(result: Result<List<Kiosk>>?) {
        when (result) {
            is Result.OnSuccess -> {
                kiosks.clear()
                kiosks.addAll(result.value)
                if (this::map.isInitialized) {
                    showMarkers()
                }
            }
            is Result.OnError -> {
            }
            else -> {
            }
        }
    }

    companion object {
        fun getLaunchIntent(context: Context): Intent {
            return Intent(context, KioskMapActivity::class.java)
        }
    }
}
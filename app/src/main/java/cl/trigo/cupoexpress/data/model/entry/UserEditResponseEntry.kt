package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.UserEditResponse

@Keep
data class UserEditResponseEntry(
    val uid: String?,
    val url: String?
)

fun UserEditResponseEntry.toUserEditResponse(): UserEditResponse {
    return UserEditResponse(
        uid = uid.orEmpty(),
        url = url.orEmpty()
    )
}
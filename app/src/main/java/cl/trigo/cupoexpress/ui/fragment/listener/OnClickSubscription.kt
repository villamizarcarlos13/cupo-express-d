package cl.trigo.cupoexpress.ui.fragment.listener

import cl.trigo.cupoexpress.domain.model.Subscription

interface OnClickSubscription {
    fun onClickItem(subscription: Subscription): Unit
    fun onClickDelete(subscription: Subscription): Unit
}
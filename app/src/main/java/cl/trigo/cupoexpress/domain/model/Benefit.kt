package cl.trigo.cupoexpress.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Benefit (
    val name: String,
    val description: String,
    val price: String,
    val shortName: String
) : Parcelable
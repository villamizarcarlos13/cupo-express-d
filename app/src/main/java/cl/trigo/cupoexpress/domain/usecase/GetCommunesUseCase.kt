package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.Communes
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetCommunesUseCase (
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUnitUseCase<Communes>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): Communes? {
        return beneficiaryRepository.getCommunes()
    }
}
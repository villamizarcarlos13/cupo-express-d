package cl.trigo.authentication.data.repository.remote

import cl.trigo.authentication.data.model.entry.toLogin
import cl.trigo.authentication.data.model.entry.toUser
import cl.trigo.authentication.data.source.AuthenticationDataSource
import cl.trigo.authentication.domain.model.Login
import cl.trigo.authentication.domain.model.LoginRut
import cl.trigo.authentication.domain.model.Register
import cl.trigo.authentication.domain.model.Status
import cl.trigo.authentication.domain.repository.LoginRepository
import cl.trigo.core.base.ResponseCompletable

class LoginRemoteRepository(
    private val authenticationDataSource: AuthenticationDataSource
) : LoginRepository {
    override suspend fun signUp(
        firstName: String,
        lastName: String,
        rut: String,
        email: String,
        password: String
    ): Register {
        val registerEntry =
            authenticationDataSource.signUp(firstName, lastName, rut, email, password)
        return Register(uid = registerEntry.data.uid)
    }

    override suspend fun signIn(email: String, password: String): Login {
        val loginEntry = authenticationDataSource.signIn(email, password)
        val loginStatus = Status(
            status = loginEntry.status,
            statusCode = loginEntry.statusCode,
            description = loginEntry.description
        )
        return Login(
            idToken = loginEntry.data.idToken,
            user = loginEntry.data.user.toUser(),
            status = loginStatus
        )
    }

    override suspend fun verifyUserByRut(rut: String): LoginRut {
        val loginEntry = authenticationDataSource.verifyUserByRut(rut)
        val loginStatus = Status(
            status = loginEntry.status,
            statusCode = loginEntry.statusCode,
            description = loginEntry.description
        )
        return authenticationDataSource.verifyUserByRut(rut).data.toLogin(loginStatus)
    }

    override suspend fun restorePassword(email: String): ResponseCompletable =
        authenticationDataSource.restorePassword(email)
}
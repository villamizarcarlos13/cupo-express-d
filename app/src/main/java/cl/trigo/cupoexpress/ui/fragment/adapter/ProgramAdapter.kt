package cl.trigo.cupoexpress.ui.fragment.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import cl.trigo.core.extension.load
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Program
import kotlinx.android.synthetic.main.item_program.view.*

class ProgramAdapter(
    private val programs: List<Program>
) : RecyclerView.Adapter<ProgramAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_program, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val program = programs[position]
        holder.logoView.load(program.foundation.logo)
        holder.nameView.text = program.name
        holder.regionView.text = program.region
        holder.headerView.load(program.header)
        holder.descriptionView.text = program.description

        with(holder.mView) { tag = program }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val logoView: ImageView = mView.logo
        val nameView: TextView = mView.name
        val regionView: TextView = mView.region
        val headerView: ImageView = mView.header
        val descriptionView: TextView = mView.description
    }

    override fun getItemCount(): Int = programs.size
}

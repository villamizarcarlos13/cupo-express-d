package cl.trigo.authentication.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.findNavController
import cl.trigo.authentication.R
import cl.trigo.core.extension.makeStatusBarTransparent


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupNavigation()
        makeStatusBarTransparent()
    }

    private fun setupNavigation() {
        val shouldShowSplash = intent.getBooleanExtra(SHOULD_SHOW_SPLASH, true)

        val navController: NavController = findNavController(R.id.nav_host_auth)

        val navGraph: NavGraph = navController.navInflater.inflate(R.navigation.nav_graph_auth)
        if (shouldShowSplash) {
            navGraph.startDestination = R.id.splashFragment
        } else {
            navGraph.startDestination = R.id.loginRutFragment
        }
        navController.graph = navGraph
    }

    companion object {
        private const val SHOULD_SHOW_SPLASH = "SHOULD_SHOW_SPLASH"

        fun getLaunchIntent(context: Context, shouldShowSplash: Boolean): Intent {
            return Intent(context, LoginActivity::class.java).apply {
                putExtra(SHOULD_SHOW_SPLASH, shouldShowSplash)
            }
        }
    }
}

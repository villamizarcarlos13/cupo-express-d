package cl.trigo.payment.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.payment.R
import cl.trigo.payment.ui.fragment.PaymentMethodsFragment
import kotlinx.android.synthetic.main.activity_payment_methods.*

class PaymentMethodsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_methods)
        makeStatusBarTransparent()
        payment_methods_top_bar.setNavigationOnClickListener { this.onBackPressed() }
        supportFragmentManager.beginTransaction()
            .add(R.id.payment_methods_fragment_container,
                PaymentMethodsFragment()
            )
            .commit()
    }

    companion object {
        fun getLaunchIntent(context: Context): Intent =
            Intent(context, PaymentMethodsActivity::class.java)
    }
}
package cl.trigo.cupoexpress

import kotlin.test.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

object ExampleUnitTest : Spek({

    describe("A String") {
        val string by memoized { "Hola " }

        it("should return 4") {
            assertEquals("Hola Amigo", string.plus("Amigo"))
        }
    }
})
package cl.trigo.cupoexpress.data.model.entry


import androidx.annotation.Keep
import cl.trigo.core.extension.formatAmount
import cl.trigo.cupoexpress.domain.model.Contribution

@Keep
data class ContributionEntry(
    val id: String?,
    val createdAt: String?,
    val beneficiaries: List<BeneficiaryDonationEntry>?,
    val totalDonation: Int,
    val paymentOrderId: String?,
    val benefactor: BenefactorEntry,
    val transaction: TransactionEntry
)

fun ContributionEntry.toContribution(): Contribution {
    return Contribution(
        id = id.orEmpty(),
        createdAt = createdAt.orEmpty(),
        beneficiaries = if (beneficiaries?.isNullOrEmpty() != true) beneficiaries.map { it.toBeneficiaryDonation() } else listOf(),
        totalDonation = formatAmount(totalDonation.or(0)),
        paymentOrderId = paymentOrderId.orEmpty(),
        benefactor = benefactor.toBenefactor(),
        transaction = transaction.toTransaction()
    )
}
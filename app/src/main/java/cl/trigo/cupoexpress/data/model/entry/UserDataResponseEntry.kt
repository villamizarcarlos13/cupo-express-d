package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.User

@Keep
data class UserDataResponseEntry(
    val user: UserEntry
)

fun UserDataResponseEntry.toUser(): User {
    return User(
        id = user.id.orEmpty(),
        firstname = user.firstname.orEmpty(),
        lastname = user.lastname.orEmpty(),
        photo = user.photo.orEmpty(),
        rut = user.rut.orEmpty(),
        email = user.email.orEmpty(),
        role = user.role.orEmpty(),
        phone = user.phone.orEmpty(),
        country = user.country.orEmpty(),
        city = user.city.orEmpty(),
        region = user.region.orEmpty(),
        address = user.address.orEmpty(),
        active = user.active,
        termsAcceptance = user.termsAcceptance
    )
}
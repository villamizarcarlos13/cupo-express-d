package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.CompletableEmptyUseCase
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class UpdateNotificationUseCase (
    private val beneficiaryRepository: BeneficiaryRepository
) : CompletableEmptyUseCase(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground() {
        beneficiaryRepository.updateNotification()
    }
}
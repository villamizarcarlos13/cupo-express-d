package cl.trigo.cupoexpress.domain.model

data class Program(
    val id: String,
    val name: String,
    val description: String,
    val region: String,
    val city: String,
    val country: String,
    val contact: String,
    val email: String,
    val phone: String,
    val logo: String,
    val header: String,
    val active: Boolean,
    val foundation: Foundation
)
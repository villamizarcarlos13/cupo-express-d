package cl.trigo.payment.data.model.request

import androidx.annotation.Keep

@Keep
data class PaymentItemRequest(
    val id: String,
    val quantity: Int
)
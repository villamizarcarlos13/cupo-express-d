package cl.trigo.authentication.ui.fragment

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.core.util.PatternsCompat.EMAIL_ADDRESS
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import cl.trigo.authentication.R
import cl.trigo.authentication.domain.model.Register
import cl.trigo.authentication.domain.model.User
import cl.trigo.authentication.ui.viewmodel.RegisterViewModel
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.common.RegisterBottomSheetFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.observe
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.regex.Matcher
import java.util.regex.Pattern

class RegisterFragment : BaseFragment() {

    private val registerViewModel by viewModel<RegisterViewModel>()
    private val args: RegisterFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(registerViewModel) {
            observe(registerLiveData, ::registerObserver)
            observe(loginLiveData, ::loginObserver)
        }
        setupClickListeners()
    }

    private fun setupClickListeners() {
        register_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        btn_register.setOnClickListener {
            val name = input_name.editText!!.text.toString()
            val email = input_email.editText!!.text.toString()
            val password = input_password.editText!!.text.toString()

            if (!validateName(name)) return@setOnClickListener
            if (!validateEmail(email)) return@setOnClickListener
            if (!validatePassword(password)) return@setOnClickListener

            registerViewModel.register(name, "", args.userRut, email, password)
        }
    }

    private fun validateName(firstName: String): Boolean {
        firstName.let {
            if (it.length < 3) {
                input_name.editText!!.error = "El nombre tener al menos 8 carácteres"
                input_name.editText!!.requestFocus()
                return false
            }
        }
        return true
    }

    private fun validateEmail(email: String): Boolean {
        email.let {
            if (!EMAIL_ADDRESS.matcher(email).matches()) {
                input_email.editText!!.error = "El email no es válido"
                input_email.requestFocus()
                return false
            }
        }
        return true
    }

    private fun validatePassword(password: String): Boolean {
        var messageError = ""
        val checkUppercase = "(?=.*[A-Z]).{1,}"
        val checkLowercase = "(?=.*[a-z]).{1,}"
        val checkSpecialCharacter = "(?=.*[!@#\$&.*]).{1,}"
        val checkDigit = "(?=.*[0-9]).{1,}"

        password.let {
            if (it.length < 8) {
                messageError = messageError + "\n" + resources.getString(R.string.password_length)
            }
            if (!isValidPassword(it, checkUppercase)){
                messageError = messageError + "\n" + resources.getString(R.string.upper_case_missing)
            }
            if (!isValidPassword(it, checkLowercase)){
                messageError = messageError + "\n" + resources.getString(R.string.lower_case_missing)
            }
            if (!isValidPassword(it, checkSpecialCharacter)){
                messageError = messageError + "\n" + resources.getString(R.string.special_character_missing)
            }
            if (!isValidPassword(it, checkDigit)){
                messageError = messageError + "\n" + resources.getString(R.string.digit_missing)
            }

            if (messageError.isNotEmpty()){
                input_password.editText!!.error = resources.getString(R.string.wrong_password_format) + "\n" +
                        messageError
                input_password.requestFocus()
                return false
            }
        }
        return true
    }

    private fun isValidPassword(password: String?, patternIn: String?): Boolean {
        val pattern: Pattern = Pattern.compile(patternIn)
        val matcher: Matcher = pattern.matcher(password)
        return matcher.matches()
    }

    private fun showLoading(isLoading: Boolean) {
        register_loading.isVisible = isLoading
    }

    private fun registerObserver(result: Result<Register>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
                btn_register.isEnabled = false
            }
            is Result.OnSuccess -> {
                showLoading(false)
                btn_register.isEnabled = true
                val bottomSheet =
                    RegisterBottomSheetFragment.newInstance(
                        resources.getString(R.string.thanks, input_name.editText!!.text.toString()),
                        resources.getString(R.string.account_created),
                        resources.getString(R.string.proceed_to_finish)
                    )
                bottomSheet.liveData().observe(viewLifecycleOwner, Observer {
                    handleBottomSheetResult(it)
                })
                bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
            }
            is Result.OnError -> {
                showLoading(false)
                btn_register.isEnabled = true
                val errorMessage = result.throwable.message!!
                errorMessage.run {
                    showErrorDialog(message = this)
                }
            }
            else -> {

            }
        }
    }

    private fun loginObserver(result: Result<User>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
                btn_register.isEnabled = false
            }
            is Result.OnSuccess -> {
                showLoading(false)
                btn_register.isEnabled = true
                activity?.setResult(Activity.RESULT_OK)
                activity?.finish()
            }
            is Result.OnError -> {
                showLoading(false)
                btn_register.isEnabled = true
                val errorMessage = result.throwable.message!!
                errorMessage.run {
                    showErrorDialog(message = this)
                }
            }
            else -> {
            }
        }
    }

    private fun handleBottomSheetResult(isFinished: Boolean) {
        if (isFinished) {
            registerViewModel.login(
                input_email.editText!!.text.toString(),
                input_password.editText!!.text.toString()
            )
        }
    }

    override fun getLayoutId() = R.layout.fragment_register

}

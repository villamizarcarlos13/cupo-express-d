package cl.trigo.cupoexpress.ui.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ListView
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat.EMAIL_ADDRESS
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.common.RegisterBottomSheetFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.encodeBitmapToString
import cl.trigo.core.extension.observe
import cl.trigo.core.extension.validateRut
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Communes
import cl.trigo.cupoexpress.ui.fragment.adapter.CommunesAdapter
import cl.trigo.cupoexpress.ui.viewmodel.AddBeneficiaryViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.*
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.btn_show_communes
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.btn_upload_photo
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.ib_remove_photo
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.input_contact
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.input_first_name
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.input_last_name
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.input_rut
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.iv_beneficiary_photo
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.rb_email
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.rb_phone
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.spinner_communes
import kotlinx.android.synthetic.main.fragment_add_beneficiary_information.tv_communes_value
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddBeneficiaryInformationFragment(
    private val interactionListener: OnChangeFragmentInteractionListener
) : BaseFragment(), AdapterView.OnItemSelectedListener {

    private val addBeneficiaryViewModel by viewModel<AddBeneficiaryViewModel>()
    private val communesList = arrayListOf<String>()
    private var photo: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(addBeneficiaryViewModel) {
            observe(addBeneficiaryLiveResult, ::addBeneficiaryObserve)
            observe(communesLiveData, ::observeCommunesList)
        }
        setupClickListeners()
        addBeneficiaryViewModel.getCommunes()
    }

    private fun setupClickListeners() {
        add_beneficiary_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        input_contact.hint = resources.getString(R.string.input_hint)
        input_contact.isHintEnabled = false
        tv_communes_value.setOnClickListener { spinner_communes.performClick() }
        btn_show_communes.setOnClickListener { showRequisitesDialog(false) }
        btn_upload_photo.setOnClickListener { openImageGallery() }
        ib_remove_photo.setOnClickListener { removePhoto() }
        btn_add_beneficiary.setOnClickListener {
            val firstName = input_first_name.editText!!.text.toString()
            val lastName = input_last_name.editText!!.text.toString()
            val beneficiaryRut = input_rut.editText!!.text.toString()
            val beneficiaryContact = input_contact.editText!!.text.toString()
            val commune = tv_communes_value.text.toString()

            if (!validateNames(firstName, lastName)) return@setOnClickListener
            if (!validateRutInput(beneficiaryRut)) return@setOnClickListener
            if (!validateEmailOrPhone(beneficiaryContact)) return@setOnClickListener
            if (commune.isEmpty()) return@setOnClickListener

            val email: String = if (rb_email.isChecked) beneficiaryContact else ""
            val phone: String = if (rb_phone.isChecked) beneficiaryContact else ""

            addBeneficiaryViewModel.addBeneficiary(
                beneficiaryRut,
                firstName,
                lastName,
                email,
                phone,
                photo,
                commune
            )
        }
    }

    private fun removePhoto() {
        iv_beneficiary_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_avatar_default_xl))
        ib_remove_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_off))
        ib_remove_photo.isEnabled = false
        photo = ""
    }

    private fun openImageGallery() {
        val pickPhoto = Intent( Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, REQUEST_PHOTO_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) { REQUEST_PHOTO_CODE -> saveAndSetPhoto(intent?.data) }
        }
    }

    private fun saveAndSetPhoto(selectedPhoto: Uri?) {
        ib_remove_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_on))
        ib_remove_photo.isEnabled = true
        
        Glide.with(this)
            .asBitmap()
            .load(selectedPhoto)
            .error(R.drawable.ic_avatar_default_xl)
            .transform(CenterCrop(), RoundedCorners(24))
            .into(object : CustomTarget<Bitmap>(300,300){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    iv_beneficiary_photo.setImageBitmap(resource)
                    photo = encodeBitmapToString(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) { }
            })
    }

    private fun setupSpinner() {
        val adapter: ArrayAdapter<String>? =
            context?.let {
                ArrayAdapter<String>(
                    it,
                    R.layout.view_spinner_item,
                    communesList
                )
            }
        adapter?.setDropDownViewResource(R.layout.view_spinner_item)
        spinner_communes.adapter = adapter
        spinner_communes.onItemSelectedListener = this
    }

    private fun showRequisitesDialog(showCheckbox: Boolean) {
        context?.let {
            val view: View =
                LayoutInflater.from(it).inflate(R.layout.dialog_communes_available, null)
            val listView = view.findViewById<View>(R.id.lv_communes_list) as ListView
            val checkbox = view.findViewById<View>(R.id.cb_do_not_show_communes) as CheckBox
            checkbox.isVisible = showCheckbox
            listView.adapter = CommunesAdapter(communesList)

            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.available_communes))
                .setView(view)
                .setPositiveButton(getString(R.string.understood)) { _, _ ->
                    if (checkbox.isChecked) {
                        AppPreferences.showCommunesPrompt = false
                    }
                }
                .show()
        }
    }

    private fun validateNames(firstName: String, lastName: String): Boolean {
        firstName.let {
            if (it.length < 3) {
                input_first_name.editText!!.error = "El nombre debe tener al menos 3 carácteres"
                input_first_name.editText!!.requestFocus()
                return false
            }
        }
        lastName.let {
            if (it.length < 3) {
                input_last_name.editText!!.error = "El apellido debe tener al menos 3 carácteres"
                input_last_name.editText!!.requestFocus()
                return false
            }
        }
        return true
    }

    private fun validateRutInput(rut: String): Boolean {
        rut.let {
            if (!validateRut(it)) {
                input_rut.editText!!.error = "El Rut no es válido"
                input_rut.editText!!.requestFocus()
                return false
            }
        }
        return true
    }

    private fun validateEmailOrPhone(contact: String): Boolean {
        if (rb_email.isChecked) {
            contact.let {
                if (!EMAIL_ADDRESS.matcher(contact).matches()) {
                    input_contact.editText!!.error = "El email no es válido"
                    input_contact.requestFocus()
                    return false
                }
            }
        } else {
            contact.let {
                if (contact.length < 8) {
                    input_contact.editText!!.error = "El teléfono no es válido"
                    input_contact.requestFocus()
                    return false
                }
            }
        }
        return true
    }

    private fun setupViewOnAddBeneficiarySuccess() {
        val name =
            input_first_name.editText!!.text.toString() + " " + input_last_name.editText!!.text.toString()
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.done),
                resources.getString(R.string.beneficiary_added_successfully, name),
                resources.getString(R.string.proceed)
            )
        bottomSheet.liveData().observe(viewLifecycleOwner, Observer {
            activity?.setResult(Activity.RESULT_OK)
            activity?.finish()
        })
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun showLoading(isLoading: Boolean) {
        beneficiary_add_loading.isVisible = isLoading
    }

    private fun addBeneficiaryObserve(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
                showLoading(true)
            }
            is Completable.OnComplete -> {
                showLoading(false)
                setupViewOnAddBeneficiarySuccess()
            }
            is Completable.OnError -> {
                showLoading(false)
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    private fun observeCommunesList(result: Result<Communes>?) {
        when (result) {
            is Result.OnSuccess -> {
                communesList.clear()
                for (commune in result.value.communes) {
                    communesList.add(commune.name)
                }
                setupSpinner()
                if (result.value.showPrompt) showRequisitesDialog(true)
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        tv_communes_value.text = resources.getString(R.string.select)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        tv_communes_value.text = communesList[position]
    }

    override fun getLayoutId() = R.layout.fragment_add_beneficiary_information

    companion object {
        private const val REQUEST_PHOTO_CODE = 101
        fun newInstance(
            interactionListener: OnChangeFragmentInteractionListener
        ): AddBeneficiaryInformationFragment {
            return AddBeneficiaryInformationFragment(interactionListener)
        }
    }
}

package cl.trigo.cupoexpress.domain.model

data class Transaction (
    val last4CardDigits: String,
    val transactionId: String,
    val authorizationCode: String,
    val responseCode: Int,
    val creditCardType: String
)
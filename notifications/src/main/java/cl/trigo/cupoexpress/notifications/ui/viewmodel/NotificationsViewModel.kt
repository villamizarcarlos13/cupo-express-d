package cl.trigo.cupoexpress.notifications.ui.viewmodel


import androidx.lifecycle.viewModelScope
import cl.trigo.core.base.BaseViewModel
import cl.trigo.cupoexpress.notifications.domain.usecase.DeleteNotificationUseCase
import cl.trigo.cupoexpress.notifications.domain.usecase.GetNotificationsUseCase
import cl.trigo.cupoexpress.notifications.domain.usecase.MarkAsReadUseCase
import cl.trigo.cupoexpress.notifications.ui.mapper.NotificationMapper
import cl.trigo.cupoexpress.notifications.ui.uistate.NotificationUiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class NotificationsViewModel(
    private val getNotificationsUseCase: GetNotificationsUseCase,
    private val markAsReadUseCase: MarkAsReadUseCase,
    private val deleteNotificationUseCase: DeleteNotificationUseCase,
    private val mapper: NotificationMapper
) : BaseViewModel<NotificationUiState>() {

    @ExperimentalCoroutinesApi
    fun getAllNotifications(forceUpdate: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            getNotificationsUseCase.getAllNotifications(forceUpdate)
                .onStart { uiState.postValue(NotificationUiState.Loading) }
                .collect { notifications ->
                    uiState.postValue(
                        NotificationUiState.SuccessLoadNotifications(
                            with(mapper) {
                                sortNotificationByDate(notifications.map { it.toModel() })
                            }
                        )
                    )
                }
        }
            .onFailure { uiState.postValue(NotificationUiState.Error(it)) }
    }

    fun markAsRead(id: String) = viewModelScope.launch(Dispatchers.IO) {
        uiState.postValue(NotificationUiState.Loading)
        runCatching {
            markAsReadUseCase.markAsRead(id)
        }.onFailure { uiState.postValue(NotificationUiState.Error(it)) }
            .onSuccess { uiState.postValue(NotificationUiState.SuccessMarkAsRead) }
    }

    fun deleteNotification(id: String) = viewModelScope.launch(Dispatchers.IO) {
        uiState.postValue(NotificationUiState.Loading)
        runCatching {
            deleteNotificationUseCase.deleteNotification(id)
        }.onFailure { uiState.postValue(NotificationUiState.Error(it)) }
            .onSuccess { uiState.postValue(NotificationUiState.SuccessDelete) }
    }

}

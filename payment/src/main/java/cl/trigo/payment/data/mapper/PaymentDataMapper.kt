package cl.trigo.payment.data.mapper

import cl.trigo.core.storage.entity.NotificationEntity
import cl.trigo.core.storage.entity.PaymentMethodEntity
import cl.trigo.payment.data.model.entry.PaymentMethodEntry
import cl.trigo.payment.domain.model.PaymentMethod

class PaymentDataMapper {

    fun PaymentMethodEntity.toPaymentMethod() = PaymentMethod(
        id = id,
        creditCardType = creditCardType,
        lastCardDigits = lastCardDigits,
        isLast = isLast
    )

    fun PaymentMethodEntry.toPaymentMethod(): PaymentMethod {
        return PaymentMethod(
            id = id.orEmpty(),
            creditCardType = creditCardType.orEmpty(),
            lastCardDigits = "**** **** **** " + lastCardDigits.orEmpty(),
            isLast = isLast ?: false
        )
    }

    fun PaymentMethodEntry.toPaymentMethodEntity() = PaymentMethodEntity(
        id = id.orEmpty(),
        creditCardType = creditCardType.orEmpty(),
        lastCardDigits = "**** **** **** " + lastCardDigits.orEmpty(),
        isLast = isLast ?: false
    )
}
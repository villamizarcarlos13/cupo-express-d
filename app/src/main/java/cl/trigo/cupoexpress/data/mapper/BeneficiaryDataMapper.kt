package cl.trigo.cupoexpress.data.mapper

import cl.trigo.core.storage.entity.BeneficiaryEntity
import cl.trigo.cupoexpress.data.model.entry.BeneficiaryEntry
import cl.trigo.cupoexpress.data.model.entry.toLastBenefactor
import cl.trigo.cupoexpress.domain.model.Beneficiary
import cl.trigo.cupoexpress.domain.model.LastBenefactor

class BeneficiaryDataMapper {

    fun BeneficiaryEntity.toBeneficiary() = Beneficiary(
        id = id,
        firstname = firstname,
        lastname = lastname,
        fullname = "$firstname $lastname",
        photo = photo,
        rut = rut,
        email = email,
        phone = phone,
        country = country,
        city = city,
        region = region,
        commune = comuna,
        address = address,
        isFavorite = isFavorite,
        active = active,
        signedup = signedup,
        lastBenefactors = listOf(),
        lastDonationDate = lastDonationDate
    )

    fun BeneficiaryEntry.toBeneficiary() = Beneficiary(
        id = id.orEmpty(),
        firstname = firstname,
        lastname = lastname,
        fullname = "$firstname $lastname",
        photo = photo.orEmpty(),
        rut = rut,
        email = email.orEmpty(),
        phone = phone.orEmpty(),
        country = country,
        city = city,
        region = region,
        commune = comuna,
        address = address,
        isFavorite = isFavorite,
        active = active,
        signedup = signedup,
        lastBenefactors = lastBenefactors?.map {
            it?.toLastBenefactor() ?: LastBenefactor(
                "",
                0,
                "",
                "",
                ""
            )
        }.orEmpty(),
        lastDonationDate = lastDonationDate
    )

    fun BeneficiaryEntry.toBeneficiaryEntity() = BeneficiaryEntity(
        id = id.orEmpty(),
        firstname = firstname.orEmpty(),
        lastname = lastname.orEmpty(),
        photo = photo.orEmpty(),
        rut = rut.orEmpty(),
        email = email.orEmpty(),
        phone = phone.orEmpty(),
        country = country.orEmpty(),
        city = city.orEmpty(),
        region = region.orEmpty(),
        comuna = comuna.orEmpty(),
        address = address.orEmpty(),
        isFavorite = isFavorite,
        active = active,
        signedup = signedup,
        lastDonationDate = lastDonationDate.orEmpty()
    )


}
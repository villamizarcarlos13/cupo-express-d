package cl.trigo.core.base

data class ResponseCompletable(
    val status: Boolean,
    val statusCode: Int,
    val description: String
)
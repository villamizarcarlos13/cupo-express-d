package cl.trigo.core.exceptions

import java.lang.Exception

class DeletePaymentMethodException: Exception()
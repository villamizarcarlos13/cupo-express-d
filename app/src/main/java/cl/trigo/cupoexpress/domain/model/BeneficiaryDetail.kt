package cl.trigo.cupoexpress.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class BeneficiaryDetail(
    val beneficiary: Beneficiary
) : Parcelable
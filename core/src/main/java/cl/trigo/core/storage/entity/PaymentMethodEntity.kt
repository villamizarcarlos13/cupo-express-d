package cl.trigo.core.storage.entity
import androidx.room.Entity
import androidx.room.PrimaryKey
import cl.trigo.core.storage.db.DbConstants.TABLE_PAYMENT_METHODS

@Entity(tableName = TABLE_PAYMENT_METHODS)
data class PaymentMethodEntity (
    @PrimaryKey val id: String,
    val creditCardType: String,
    val lastCardDigits: String,
    val isLast: Boolean
)
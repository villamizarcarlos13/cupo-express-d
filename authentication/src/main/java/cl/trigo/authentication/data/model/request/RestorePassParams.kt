package cl.trigo.authentication.data.model.request

data class RestorePassParams (
    val email: String
)
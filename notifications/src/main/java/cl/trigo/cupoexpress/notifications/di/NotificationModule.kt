package cl.trigo.cupoexpress.notifications.di

import cl.trigo.core.util.AppPreferences
import cl.trigo.core.storage.db.CupoExpressDatabase
import cl.trigo.cupoexpress.notifications.data.NotificationDataRepository
import cl.trigo.cupoexpress.notifications.data.local.NotificationLocal
import cl.trigo.cupoexpress.notifications.data.local.NotificationLocalDataSource
import cl.trigo.cupoexpress.notifications.data.mapper.NotificationDataMapper
import cl.trigo.cupoexpress.notifications.data.net.NotificationService
import cl.trigo.cupoexpress.notifications.data.remote.NotificationRemote
import cl.trigo.cupoexpress.notifications.data.remote.NotificationRemoteDataSource
import cl.trigo.cupoexpress.notifications.domain.repository.NotificationRepository
import cl.trigo.cupoexpress.notifications.domain.usecase.DeleteNotificationUseCase
import cl.trigo.cupoexpress.notifications.domain.usecase.GetNotificationsUseCase
import cl.trigo.cupoexpress.notifications.domain.usecase.MarkAsReadUseCase
import cl.trigo.cupoexpress.notifications.ui.mapper.NotificationMapper
import cl.trigo.cupoexpress.notifications.ui.viewmodel.NotificationsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

object NotificationModule {

    fun load() = load

    private val load by lazy {
        loadKoinModules(notificationModule)
    }

    private val notificationModule = module {

        /* API REST Remote */
        single { get<Retrofit>().create(NotificationService::class.java) as NotificationService }

        /* DataSources*/
        factory<NotificationRemote> { NotificationRemoteDataSource(get(), AppPreferences) }
        factory<NotificationLocal> { NotificationLocalDataSource(get(), AppPreferences) }

        /* Repositories */
        factory<NotificationRepository> { NotificationDataRepository(get(), get(), get()) }

        /* Dao Interfaces */
        factory { get<CupoExpressDatabase>().notificationsDao()}

        /* Mappers */
        factory { NotificationDataMapper() }
        factory { NotificationMapper() }

        /* UseCases */
        factory { GetNotificationsUseCase(get()) }
        factory { MarkAsReadUseCase(get()) }
        factory { DeleteNotificationUseCase(get()) }

        /* View Models */
        viewModel { NotificationsViewModel(get(), get(), get(), get()) }
    }
}


package cl.trigo.cupoexpress.ui.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.common.RegisterBottomSheetFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.encodeBitmapToString
import cl.trigo.core.extension.isValidUrl
import cl.trigo.core.extension.loadWithRoundTransform
import cl.trigo.core.extension.observe
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Communes
import cl.trigo.cupoexpress.ui.fragment.adapter.CommunesAdapter
import cl.trigo.cupoexpress.ui.viewmodel.EditBeneficiaryViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_edit_beneficiary.*
import kotlinx.android.synthetic.main.fragment_edit_beneficiary.btn_save
import kotlinx.android.synthetic.main.fragment_edit_beneficiary.btn_upload_photo
import kotlinx.android.synthetic.main.fragment_edit_beneficiary.ib_remove_photo
import kotlinx.android.synthetic.main.fragment_edit_beneficiary.input_first_name
import kotlinx.android.synthetic.main.fragment_edit_beneficiary.input_last_name
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditBeneficiaryFragment : BaseFragment(), AdapterView.OnItemSelectedListener,
    CompoundButton.OnCheckedChangeListener {
    private val viewModel by viewModel<EditBeneficiaryViewModel>()
    private val args: EditBeneficiaryFragmentArgs by navArgs()
    private val communesList = arrayListOf<String>()
    private var photo: String = DEFAULT_PHOTO

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(editBeneficiaryLiveResult, ::editBeneficiaryObserve)
            observe(communesLiveData, ::observeCommunesList)
            getCommunes()
        }
        setupViews()
        setupClickListeners()
    }

    private fun setupViews() {
        args.beneficiary.let {
            input_first_name.editText?.text?.append(it.firstname)
            input_last_name.editText?.text?.append(it.lastname)
            input_rut.editText?.text?.append(it.rut)
            if (it.email.isNotEmpty()) {
                rb_email.isChecked = true
                input_contact.editText?.text?.append(it.email)
            } else {
                rb_phone.isChecked = true
                input_contact.editText?.text?.append(it.phone)
            }
            if (it.photo.isNotEmpty() && isValidUrl(it.photo)) {
                iv_beneficiary_photo.loadWithRoundTransform(it.photo, R.drawable.ic_avatar_default_xl)
                ib_remove_photo.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_on))
                ib_remove_photo.isEnabled = true
            }
        }
    }

    private fun setupSpinner() {
        val adapter: ArrayAdapter<String>? =
            context?.let {
                ArrayAdapter<String>(
                    it,
                    R.layout.view_spinner_item,
                    communesList
                )
            }
        adapter?.setDropDownViewResource(R.layout.view_spinner_item)
        spinner_communes.adapter = adapter
        spinner_communes.onItemSelectedListener = this
        for (commune: String in communesList) {
            if (args.beneficiary.commune == commune) {
                spinner_communes.setSelection(communesList.indexOf(commune))
            }
        }
    }

    private fun setupClickListeners() {
        edit_beneficiary_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        btn_show_communes.setOnClickListener { showAvailableCommunes() }
        rb_email.setOnCheckedChangeListener(this)
        rb_phone.setOnCheckedChangeListener(this)
        tv_communes_value.setOnClickListener { spinner_communes.performClick() }
        btn_upload_photo.setOnClickListener { openImageGallery() }
        ib_remove_photo.setOnClickListener { removePhoto() }
        btn_save.setOnClickListener {
            val id = args.beneficiary.id
            val firstName = input_first_name.editText!!.text.toString()
            val lastName = input_last_name.editText!!.text.toString()
            val beneficiaryContact = input_contact.editText!!.text.toString()
            val commune = tv_communes_value.text.toString()
            val beneficiaryPhoto: String? = if (photo == DEFAULT_PHOTO) null else photo

            if (!validateNames(firstName, lastName)) return@setOnClickListener
            if (!validateEmailOrPhone(beneficiaryContact)) return@setOnClickListener

            val email: String = if (rb_email.isChecked) beneficiaryContact else ""
            val phone: String = if (rb_phone.isChecked) beneficiaryContact else ""

            viewModel.editBeneficiary(
                id,
                firstName,
                lastName,
                email,
                phone,
                commune,
                beneficiaryPhoto
            )
        }
    }

    private fun removePhoto() {
        iv_beneficiary_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_avatar_default_xl))
        ib_remove_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_off))
        ib_remove_photo.isEnabled = false
        photo = ""
    }

    private fun openImageGallery() {
        val pickPhoto = Intent( Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, REQUEST_PHOTO_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) { REQUEST_PHOTO_CODE -> saveAndSetPhoto(intent?.data) }
        }
    }

    private fun saveAndSetPhoto(selectedPhoto: Uri?) {
        ib_remove_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_on))
        ib_remove_photo.isEnabled = true

        Glide.with(this)
            .asBitmap()
            .load(selectedPhoto)
            .error(R.drawable.ic_avatar_default_xl)
            .transform(CenterCrop(), RoundedCorners(24))
            .into(object : CustomTarget<Bitmap>(300,300){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    iv_beneficiary_photo.setImageBitmap(resource)
                    photo = encodeBitmapToString(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) { }
            })
    }

    private fun showAvailableCommunes() {
        context?.let {
            val view: View =
                LayoutInflater.from(it).inflate(R.layout.dialog_communes_available, null)
            val listView = view.findViewById<View>(R.id.lv_communes_list) as ListView
            val checkbox = view.findViewById<View>(R.id.cb_do_not_show_communes) as CheckBox
            checkbox.isVisible = false
            listView.adapter = CommunesAdapter(communesList)

            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.available_communes))
                .setView(view)
                .setPositiveButton(getString(R.string.understood), null)
                .show()
        }
    }

    private fun validateNames(firstName: String, lastName: String): Boolean {
        firstName.let {
            if (it.length < 3) {
                input_first_name.editText!!.error = "El nombre debe tener al menos 3 carácteres"
                input_first_name.editText!!.requestFocus()
                return false
            }
        }
        lastName.let {
            if (it.length < 3) {
                input_last_name.editText!!.error = "El apellido debe tener al menos 3 carácteres"
                input_last_name.editText!!.requestFocus()
                return false
            }
        }
        return true
    }

    private fun validateEmailOrPhone(contact: String): Boolean {
        if (rb_email.isChecked) {
            contact.let {
                if (!PatternsCompat.EMAIL_ADDRESS.matcher(contact).matches()) {
                    input_contact.editText!!.error = "El email no es válido"
                    input_contact.requestFocus()
                    return false
                }
            }
        } else {
            contact.let {
                if (contact.length < 8) {
                    input_contact.editText!!.error = "El teléfono no es válido"
                    input_contact.requestFocus()
                    return false
                }
            }
        }
        return true
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        input_contact.editText?.text?.clear()
        if (rb_email.isChecked) {
            input_contact.editText?.text?.append(args.beneficiary.email)
        } else {
            input_contact.editText?.text?.append(args.beneficiary.phone)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        tv_communes_value.text = resources.getString(R.string.select)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        tv_communes_value.text = communesList[position]
    }

    private fun setupViewOnEditBeneficiarySuccess() {
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.saved_successfully),
                resources.getString(R.string.saved_successfully_message),
                resources.getString(R.string.proceed)
            )
        bottomSheet.liveData().observe(viewLifecycleOwner, Observer {
            requireActivity().onBackPressed()
        })
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun showLoading(isLoading: Boolean) {
        beneficiary_edit_loading.isVisible = isLoading
    }

    private fun editBeneficiaryObserve(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
                showLoading(true)
            }
            is Completable.OnComplete -> {
                showLoading(false)
                setupViewOnEditBeneficiarySuccess()
            }
            is Completable.OnError -> {
                showLoading(false)
            }
            else -> {
            }
        }
    }

    private fun observeCommunesList(result: Result<Communes>?) {
        when (result) {
            is Result.OnSuccess -> {
                communesList.clear()
                for (commune in result.value.communes) {
                    communesList.add(commune.name)
                }
                setupSpinner()
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_edit_beneficiary

    companion object {
        private const val REQUEST_PHOTO_CODE = 101
        private const val DEFAULT_PHOTO = "none"
    }

}
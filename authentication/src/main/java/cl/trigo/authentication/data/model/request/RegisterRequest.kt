package cl.trigo.authentication.data.model.request

import androidx.annotation.Keep

@Keep
data class RegisterRequest(
    val firstname: String,
    val lastname: String,
    val rut: String,
    val email: String,
    val password: String
)
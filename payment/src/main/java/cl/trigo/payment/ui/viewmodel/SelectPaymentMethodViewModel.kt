package cl.trigo.payment.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.trigo.core.extension.LiveResult
import cl.trigo.core.extension.postLoading
import cl.trigo.core.extension.postSuccess
import cl.trigo.core.extension.postThrowable
import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import cl.trigo.payment.domain.usecase.GetPaymentMethodsUseCase
import cl.trigo.payment.domain.usecase.RegisterPaymentMethodUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class SelectPaymentMethodViewModel(
    private val getPaymentMethodsUseCase: GetPaymentMethodsUseCase,
    private val registerPaymentMethodUseCase: RegisterPaymentMethodUseCase
) : ViewModel(

) {
    val getPaymentMethodsLiveData = LiveResult<List<PaymentMethod>>()
    val registerPaymentMethodLiveData = LiveResult<RegisterPaymentMethod>()

    fun registerPaymentMethod() =
        registerPaymentMethodUseCase.execute(registerPaymentMethodLiveData)

    fun getPaymentMethods(forceUpdate: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            getPaymentMethodsUseCase.getPaymentMethods(forceUpdate)
                .onStart { getPaymentMethodsLiveData.postLoading() }
                .collect { getPaymentMethodsLiveData.postSuccess(it) }
        }
            .onFailure { getPaymentMethodsLiveData.postThrowable(it) }
    }

}
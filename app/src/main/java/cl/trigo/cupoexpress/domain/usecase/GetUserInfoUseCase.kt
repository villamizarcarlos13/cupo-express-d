package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetUserInfoUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUnitUseCase<UserInfo>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): UserInfo? =
        beneficiaryRepository.getUserInfo()

}
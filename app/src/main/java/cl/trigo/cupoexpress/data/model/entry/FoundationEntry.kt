package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep

@Keep
data class FoundationEntry(
    val id: String,
    val logo: String,
    val description: String,
    val region: String,
    val city: String,
    val rut: String
)

fun FoundationEntry.toFoundation(): cl.trigo.cupoexpress.domain.model.Foundation {
    return cl.trigo.cupoexpress.domain.model.Foundation(
        id,
        logo,
        description,
        region,
        city,
        rut
    )
}
package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Commune
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickCommune
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.item_kiosk.view.*

class CommuneMapAdapter (
    private val list: List<Commune>,
    private val onClickListener: OnClickCommune
) : RecyclerView.Adapter<CommuneMapAdapter.ViewHolder>() {

    var selectedName: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_kiosk, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.commune.text = item.name
        holder.commune.isActivated = item.name == selectedName

        holder.commune.setOnClickListener {
            val isSelected = holder.commune.isActivated
            selectedName = if (isSelected) "" else item.name
            onClickListener.onClickItem(list[position], !isSelected)
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val commune: MaterialButton = view.btn_commune
    }
}
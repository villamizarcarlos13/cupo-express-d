package cl.trigo.cupoexpress.notifications.ui.fragment.listener

import cl.trigo.cupoexpress.notifications.ui.model.Notification

interface NotificationMenuClickListener {

    fun onClickMarkAsRead(notification: Notification)

    fun onClickDelete(notification: Notification)

}
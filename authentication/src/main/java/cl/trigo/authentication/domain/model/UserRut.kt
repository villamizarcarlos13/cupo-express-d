package cl.trigo.authentication.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class UserRut(
    val firstname: String?,
    val lastname: String?,
    val email: String?,
    val active: Boolean?
) : Parcelable
package cl.trigo.payment.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class PaymentMethod(
    val id: String,
    val creditCardType: String,
    val lastCardDigits: String,
    val isLast: Boolean
): Parcelable

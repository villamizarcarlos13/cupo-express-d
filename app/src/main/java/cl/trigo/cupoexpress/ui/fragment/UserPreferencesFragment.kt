package cl.trigo.cupoexpress.ui.fragment


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.common.RegisterBottomSheetFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.*
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.User
import cl.trigo.cupoexpress.domain.model.UserEditResponse
import cl.trigo.cupoexpress.ui.viewmodel.UserPreferencesViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.fragment_user_preferences.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class UserPreferencesFragment : BaseFragment() {

    private val viewModel by viewModel<UserPreferencesViewModel>()
    private var photo: String = DEFAULT_PHOTO

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(getUserDataLiveData, ::observeUserData)
            observe(editUserDataLiveData, ::editUserObserver)
            getUserData()
        }
        setupClickListeners()
    }

    private fun setupClickListeners() {
        btn_save.setOnClickListener {
            val firstName = input_first_name.editText!!.text.toString()
            val lastName = input_last_name.editText!!.text.toString()
            val email = input_email.editText!!.text.toString()
            val phone = input_phone.editText!!.text.toString()
            val userPhoto: String? = if (photo == DEFAULT_PHOTO) null else photo
            if (!validateNames(firstName, lastName)) return@setOnClickListener
            if (!validateEmailAndPhone(email, phone)) return@setOnClickListener

            viewModel.editUser(firstName, lastName, email, phone, userPhoto)
        }
        input_last_name.hint = resources.getString(R.string.input_hint)
        input_last_name.isHintEnabled = false

        input_phone.hint = resources.getString(R.string.input_hint)
        input_phone.isHintEnabled = false

        btn_upload_photo.setOnClickListener { openImageGallery() }
        ib_remove_photo.setOnClickListener { removePhoto() }
    }

    private fun removePhoto() {
        iv_user_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_avatar_default_xl))
        ib_remove_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_off))
        ib_remove_photo.isEnabled = false
        photo = ""
    }

    private fun openImageGallery() {
        val pickPhoto = Intent( Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, REQUEST_PHOTO_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) { REQUEST_PHOTO_CODE -> saveAndSetPhoto(intent?.data) }
        }
    }

    private fun saveAndSetPhoto(selectedPhoto: Uri?) {
        ib_remove_photo.setImageDrawable(
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_on))
        ib_remove_photo.isEnabled = true

        Glide.with(this)
            .asBitmap()
            .load(selectedPhoto)
            .error(R.drawable.ic_avatar_default_xl)
            //.centerCrop()
            .transform(CenterCrop(), RoundedCorners(24))
            .into(object : CustomTarget<Bitmap>(300,300){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    iv_user_photo.setImageBitmap(resource)
                    photo = encodeBitmapToString(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) { }
            })
    }

    private fun validateNames(firstName: String, lastName: String): Boolean {
        firstName.let {
            if (it.length < 3) {
                input_first_name.editText!!.error = "El nombre debe tener al menos 3 carácteres"
                input_first_name.editText!!.requestFocus()
                return false
            }
        }
        lastName.let {
            if (it.length < 3 && it.isNotEmpty()) {
                input_last_name.editText!!.error = "El apellido debe tener al menos 3 carácteres"
                input_last_name.editText!!.requestFocus()
                return false
            }
        }
        return true
    }

    private fun validateEmailAndPhone(email: String, phone: String): Boolean {

        email.let {
            if (!PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()) {
                input_email.editText!!.error = "El email no es válido"
                input_email.requestFocus()
                return false
            }
        }

        phone.let {
            if (phone.length < 8 && it.isNotEmpty()) {
                input_phone.editText!!.error = "El teléfono no es válido"
                input_phone.requestFocus()
                return false
            }
        }
        return true
    }

    private fun setupViewOnSuccess(user: User) {
        input_first_name.editText?.text?.append(user.firstname)
        input_last_name.editText?.text?.append(user.lastname)
        input_email.editText?.text?.append(user.email)
        input_phone.editText?.text?.append(user.phone)
        input_user_rut.editText?.text?.append(rutFormatter(user.rut))

        if (user.photo.isNotEmpty() && isValidUrl(user.photo)) {
            iv_user_photo.loadWithRoundTransform(user.photo, R.drawable.ic_avatar_default_xl)
            ib_remove_photo.setImageDrawable(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_remove_on))
            ib_remove_photo.isEnabled = true
        }
    }

    private fun setupViewOnSaveSuccess() {
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.saved_successfully),
                resources.getString(R.string.saved_successfully_message),
                resources.getString(R.string.proceed)
            )
        bottomSheet.liveData().observe(viewLifecycleOwner, Observer {
            handleBottomSheetResult(it)
        })
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun handleBottomSheetResult(isFinished: Boolean) {
        if (isFinished) {
            activity.let {
                it?.setResult(Activity.RESULT_OK)
                it?.finish()
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        user_preferences_loading.isVisible = isLoading
    }

    private fun editUserObserver(result: Result<UserEditResponse>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
                btn_save.isEnabled = false
            }
            is Result.OnSuccess -> {
                showLoading(false)
                btn_save.isEnabled = true
                setupViewOnSaveSuccess()
            }
            is Result.OnError -> {
                showLoading(false)
                btn_save.isEnabled = false
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    private fun observeUserData(result: Result<User>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
                btn_save.isEnabled = false
            }
            is Result.OnSuccess -> {
                showLoading(false)
                btn_save.isEnabled = true
                setupViewOnSuccess(result.value)
            }
            is Result.OnError -> {
                showLoading(false)
                btn_save.isEnabled = false
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_user_preferences

    companion object {
        private const val REQUEST_PHOTO_CODE = 101
        private const val DEFAULT_PHOTO = "none"
    }

}
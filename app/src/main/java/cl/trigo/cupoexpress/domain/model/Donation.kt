package cl.trigo.cupoexpress.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Donation (
    val benefactor: Benefactor,
    val createAt: String,
    val benefit: Benefit,
    val amount: String,
    val likesQuantity: Int
) : Parcelable
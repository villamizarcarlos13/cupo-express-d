package cl.trigo.payment.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.payment.PaymentConstants.CONFIRMED
import cl.trigo.payment.PaymentConstants.TBK_TOKEN
import cl.trigo.payment.R
import kotlinx.android.synthetic.main.activity_add_payment_method.*
import java.net.URLEncoder


class AddPaymentMethodActivity : AppCompatActivity() {

    private lateinit var url: String
    private lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_payment_method)
        makeStatusBarTransparent()
        url = intent.getStringExtra(PARAM_URL).orEmpty()
        token = intent.getStringExtra(PARAM_TOKEN).orEmpty()
        add_payment_method_top_bar.setNavigationOnClickListener { this.onBackPressed() }
        initView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initView() {
        wv_add_payment_method.settings.javaScriptEnabled = true
        val postData = TBK_TOKEN + URLEncoder.encode(token, "UTF-8").toString()
        wv_add_payment_method.postUrl(url, postData.toByteArray())
        wv_add_payment_method.webViewClient = object : WebViewClient() {
            override fun shouldInterceptRequest(
                view: WebView?,
                request: WebResourceRequest?
            ): WebResourceResponse? {
                if (request?.url.toString().contains(CONFIRMED)) {
                    this@AddPaymentMethodActivity.setResult(Activity.RESULT_OK)
                    finish()
                }
                return super.shouldInterceptRequest(view, request)
            }
        }
    }

    companion object {

        const val PARAM_URL = "PARAM_URL"
        const val PARAM_TOKEN = "PARAM_TOKEN"

        fun getLaunchIntent(context: Context, name: String, image: String): Intent {
            return Intent(context, AddPaymentMethodActivity::class.java).apply {
                putExtra(PARAM_URL, name)
                putExtra(PARAM_TOKEN, image)
            }
        }
    }
}

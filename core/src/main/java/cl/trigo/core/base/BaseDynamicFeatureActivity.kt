package cl.trigo.core.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseDynamicFeatureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadModules()
    }

    protected abstract fun loadModules()

}
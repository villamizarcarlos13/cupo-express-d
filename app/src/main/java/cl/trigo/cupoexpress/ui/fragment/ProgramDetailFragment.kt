package cl.trigo.cupoexpress.ui.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.extension.setSingleOnClickListener
import cl.trigo.cupoexpress.AppConstants
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.activity.KioskMapActivity
import kotlinx.android.synthetic.main.fragment_program_detail.*


class ProgramDetailFragment : BaseFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        listBrands.layoutManager = GridLayoutManager(activity, 4)
        listBrands.adapter = cl.trigo.cupoexpress.ui.fragment.adapter.BrandsAdapter()
        listBanks.layoutManager = GridLayoutManager(activity, 4)
        listBanks.adapter = cl.trigo.cupoexpress.ui.fragment.adapter.BanksAdapter()
        btn_show_map.setSingleOnClickListener { showMapOfKiosks() }

        setupToolbar()
    }

    private fun setupToolbar() {
        program_top_bar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        program_top_bar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_set_share -> {
                    intentShare()
                    true
                }
                else -> false
            }
        }
    }

    private fun intentShare(){
        val shareIntent = Intent(Intent.ACTION_SEND)
        val shareLink = AppConstants.CUPO_EXPRESS_SHARE

        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareLink)
        startActivity(Intent.createChooser(shareIntent, getString(R.string.chooseOne)))
    }

    private fun showMapOfKiosks() {
        startActivity(context?.let { KioskMapActivity.getLaunchIntent(activity as Context) })
    }

    override fun getLayoutId(): Int = R.layout.fragment_program_detail

}
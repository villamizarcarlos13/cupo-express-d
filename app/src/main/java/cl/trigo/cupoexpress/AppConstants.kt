package cl.trigo.cupoexpress

object AppConstants {
    const val PLATFORM_NAME = "ANDROID"
    const val MARKET_URL = "market://details?id="
    const val GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id="

    /* API Error codes */
    const val TOKEN_EXPIRED_403 = 403

    /* Activity result codes*/
    const val REQUEST_LOGIN_FROM_PROFILE = 51
    const val REQUEST_LOGIN_FROM_PEOPLE_LIST = 21

    /* Location Constants */
    const val SANTIAGO_LAT = -33.437492
    const val SANTIAGO_LNG = -70.651062
    const val SANTIAGO_MARKER_ZOOM_LVL = 11.4f
    const val COMMUNE_ZOOM_LVL = 13.4f
    const val URL_EXPLANATION_VIDEO = "https://vimeo.com/465959847/8f0d7127be"
    const val GOOGLE_MAPS_PACKAGE = "com.google.android.apps.maps"
    const val GOOGLE_NAVIGATION_REQUEST = "google.navigation:q="
    const val RENCA = "Renca"
    const val RENCA_LAT = -33.406398
    const val RENCA_LNG = -70.727985
    const val SAN_JOAQUIN = "San Joaquín"
    const val SAN_JOAQUIN_LAT = -33.495994
    const val SAN_JOAQUIN_LNG = -70.628512
    const val CUPO_EXPRESS_SHARE = "cupoexpress.cl"

    /* Notification Constants*/
    const val NOTIFICATION_PUSH = "notification_push"
    const val NOTIFICATION_DATA = "notification_data"
}
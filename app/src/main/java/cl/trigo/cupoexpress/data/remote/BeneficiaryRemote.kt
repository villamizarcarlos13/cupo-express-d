package cl.trigo.cupoexpress.data.remote

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.cupoexpress.data.model.entry.*
import cl.trigo.cupoexpress.data.model.entry.ProgramEntry
import cl.trigo.cupoexpress.domain.model.Kiosk

interface BeneficiaryRemote {
    suspend fun getBeneficiaries(): Response<List<BeneficiaryEntry>>
    suspend fun checkVersion(): Response<CheckVersionEntry>
    suspend fun getCommunes(): Response<List<CommuneEntry>>
    suspend fun deleteBeneficiaryById(id: String): ResponseCompletable
    suspend fun addBeneficiary(
        rut: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        photo: String,
        commune: String
    ): ResponseCompletable

    suspend fun editBeneficiary(
        id: String,
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        commune: String?,
        photo: String?
    ): ResponseCompletable

    suspend fun editUser(
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        photo: String?
    ): Response<UserEditResponseEntry>

    suspend fun getUserData(): Response<UserDataResponseEntry>
    suspend fun getBeneficiaryDetail(rut: String): Response<BeneficiaryDetailEntry>
    suspend fun getBeneficiaryDonationsById(rut: String): Response<List<DonationEntry>>
    suspend fun setBeneficiaryFavorite(id: String, isFavorite: Boolean): ResponseCompletable
    suspend fun saveFirebaseMessagingServiceToken(token: String): ResponseCompletable
    suspend fun deleteFirebaseMessagingServiceToken(token: String): ResponseCompletable
    suspend fun getContributions(): Response<List<ContributionEntry>>
    suspend fun getPrograms(): Response<List<ProgramEntry>>
    suspend fun getKiosks(): Response<List<Kiosk>>
}
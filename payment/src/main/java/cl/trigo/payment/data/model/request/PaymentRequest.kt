package cl.trigo.payment.data.model.request

import androidx.annotation.Keep

@Keep
data class PaymentRequest(
    val benefitId: String,
    val beneficiaries: List<PaymentItemRequest>,
    val paymentMethodId: String
)

package cl.trigo.core.storage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import cl.trigo.core.storage.dao.BeneficiaryDao
import cl.trigo.core.storage.dao.NotificationsDao
import cl.trigo.core.storage.dao.PaymentMethodsDao
import cl.trigo.core.storage.db.DbConstants.DATABASE_VERSION
import cl.trigo.core.storage.entity.BeneficiaryEntity
import cl.trigo.core.storage.entity.NotificationEntity
import cl.trigo.core.storage.entity.PaymentMethodEntity


@Database(
    entities = [PaymentMethodEntity::class, BeneficiaryEntity::class, NotificationEntity::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class CupoExpressDatabase : RoomDatabase() {

    abstract fun paymentMethodsDao(): PaymentMethodsDao

    abstract fun beneficiaryDao(): BeneficiaryDao

    abstract fun notificationsDao(): NotificationsDao
}
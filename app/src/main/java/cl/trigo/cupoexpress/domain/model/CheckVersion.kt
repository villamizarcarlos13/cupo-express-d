package cl.trigo.cupoexpress.domain.model

data class CheckVersion(
    val version: String,
    val minimal: String,
    val mustUpdate: Boolean?
)

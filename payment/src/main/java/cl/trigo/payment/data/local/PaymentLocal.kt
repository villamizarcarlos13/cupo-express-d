package cl.trigo.payment.data.local

import cl.trigo.core.storage.entity.PaymentMethodEntity

interface PaymentLocal {

    suspend fun getAllPaymentMethods(): List<PaymentMethodEntity>

    suspend fun insertPaymentMethodsList(methods: List<PaymentMethodEntity>)

    suspend fun deletePaymentMethod(id: String)

    suspend fun deleteAllPaymentsMethods()
}
package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.core.extension.normalizeDate
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Donation
import kotlinx.android.synthetic.main.item_donation.view.*

class DonationsAdapter (
    private val list: List<Donation>
) : RecyclerView.Adapter<DonationsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_donation, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.amount.text = item.amount
        holder.date.text = normalizeDate(item.createAt).orEmpty()
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val date: TextView = view.tv_donation_date
        val amount: TextView = view.tv_donation_amount
    }
}
package cl.trigo.cupoexpress.notifications.ui.model

import androidx.annotation.Keep

@Keep
data class Notification(
    val id: String,
    val title: String,
    val body: String,
    val iconUrl: String,
    val dateCreated: Long,
    val isRead: Boolean,
    val viewType: Int = 1
)
package cl.trigo.cupoexpress.notifications.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.cupoexpress.notifications.R
import cl.trigo.cupoexpress.notifications.databinding.FragmentNotificationsBinding
import cl.trigo.cupoexpress.notifications.ui.fragment.adapter.NotificationAdapter
import cl.trigo.cupoexpress.notifications.ui.fragment.listener.NotificationMenuClickListener
import cl.trigo.cupoexpress.notifications.ui.model.Notification
import cl.trigo.cupoexpress.notifications.ui.uistate.NotificationUiState
import cl.trigo.cupoexpress.notifications.ui.uistate.NotificationUiState.*
import cl.trigo.cupoexpress.notifications.ui.viewmodel.NotificationsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class NotificationsFragment : BaseFragment(), NotificationMenuClickListener {
    private val viewModel by viewModel<NotificationsViewModel>()
    private var _binding: FragmentNotificationsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.uiState().observe(viewLifecycleOwner, { notificationsObserver(it) })
        setupUi()
    }

    private fun setupUi() {
        binding.apply {
            rvNotificationList.layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onResume() {
        super.onResume()
        getNotifications(true)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClickMarkAsRead(notification: Notification) {
        viewModel.markAsRead(notification.id)
    }

    override fun onClickDelete(notification: Notification) {
        viewModel.deleteNotification(notification.id)
    }

    private fun getNotifications(forceUpdate: Boolean) {
        viewModel.getAllNotifications(forceUpdate)
    }

    private fun notificationsObserver(viewState: NotificationUiState) {
        showLoading(viewState.isLoading)
        when (viewState) {
            is SuccessLoadNotifications -> renderNotifications(viewState.notifications)
            is SuccessMarkAsRead -> getNotifications(false)
            is SuccessDelete -> getNotifications(false)
            is Error -> showError(viewState.error.message.orEmpty())
        }
    }

    private fun showError(message: String) {
        message.run { showErrorDialog(message = this) }
    }

    private fun showLoading(loading: Boolean) {
        binding.apply {
            notificationListLoading.root.isVisible = loading
            rvNotificationList.isInvisible = loading
        }
    }

    private fun renderNotifications(notifications: List<Notification>) {
        binding.apply {
            tvEmptyList.isVisible = notifications.isEmpty()
            rvNotificationList.isVisible = notifications.isNotEmpty()
            rvNotificationList.adapter =
                NotificationAdapter(notifications, this@NotificationsFragment)
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_notifications
}
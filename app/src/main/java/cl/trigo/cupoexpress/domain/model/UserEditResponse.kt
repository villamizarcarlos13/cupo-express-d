package cl.trigo.cupoexpress.domain.model

data class UserEditResponse (
    val uid: String,
    val url: String
)
package cl.trigo.cupoexpress.domain.model

data class Contribution (
    val id: String,
    val createdAt: String,
    val beneficiaries: List<BeneficiaryDonation>,
    val totalDonation: String,
    val paymentOrderId: String,
    val benefactor: Benefactor,
    val transaction: Transaction
)
package cl.trigo.cupoexpress.data

import cl.trigo.core.base.ResponseCompletable
import cl.trigo.core.exceptions.DeleteBeneficiaryException
import cl.trigo.core.storage.entity.BeneficiaryEntity
import cl.trigo.cupoexpress.data.model.entry.*
import cl.trigo.cupoexpress.data.local.BeneficiaryLocal
import cl.trigo.cupoexpress.data.mapper.BeneficiaryDataMapper
import cl.trigo.cupoexpress.data.remote.BeneficiaryRemote
import cl.trigo.cupoexpress.domain.model.*
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import cl.trigo.cupoexpress.data.model.entry.KioskEntry
import cl.trigo.cupoexpress.data.model.entry.toKiosk
import cl.trigo.cupoexpress.data.model.entry.toProgram
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class BeneficiaryDataRepository(
    private val beneficiaryRemoteDataSource: BeneficiaryRemote,
    private val beneficiaryLocalDataSource: BeneficiaryLocal,
    private val mapper: BeneficiaryDataMapper
) : BeneficiaryRepository {
    override suspend fun getBeneficiaries(forceUpdate: Boolean): Flow<List<Beneficiary>> = flow {
        val localBeneficiaries = beneficiaryLocalDataSource.getBeneficiaries()
        emit(with(mapper) { localBeneficiaries.map { it.toBeneficiary() } })
        if (forceUpdate) {
            val remoteBeneficiaries = beneficiaryRemoteDataSource.getBeneficiaries()
            emit(with(mapper) { remoteBeneficiaries.data.map { it.toBeneficiary() } })
            replaceAllBeneficiariesInDb(remoteBeneficiaries.data)
        }
    }

    override suspend fun getUserData(): User =
        beneficiaryRemoteDataSource.getUserData().data.toUser()

    override suspend fun checkVersion(): CheckVersion =
        beneficiaryRemoteDataSource.checkVersion().data.toCheckVersion()

    override suspend fun editUserData(
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        photo: String?
    ): UserEditResponse {
        return beneficiaryRemoteDataSource.editUser(firstName, lastName, email, phone, photo).data.toUserEditResponse()
    }


    override suspend fun getCommunes(): Communes {
        val showPrompt = beneficiaryLocalDataSource.getShowCommunesPrompt()
        val list = beneficiaryRemoteDataSource.getCommunes().data.map { it.toCommune() }
        return Communes(list, showPrompt)
    }

    override suspend fun deleteBeneficiaryById(id: String) {
        runCatching { beneficiaryRemoteDataSource.deleteBeneficiaryById(id) }.onSuccess {
            beneficiaryLocalDataSource.deleteBeneficiaryById(id)
        }.onFailure { throw DeleteBeneficiaryException() }
    }

    override suspend fun addBeneficiary(
        id: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        photo: String,
        commune: String
    ): ResponseCompletable =
        beneficiaryRemoteDataSource.addBeneficiary(id, firstName, lastName, email, phone, photo, commune)

    override suspend fun editBeneficiary(
        id: String,
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        commune: String?,
        photo: String?
    ): ResponseCompletable =
        beneficiaryRemoteDataSource.editBeneficiary(id, firstName, lastName, email, phone, photo, commune)

    override suspend fun getBeneficiaryDetail(rut: String): BeneficiaryDetail =
        beneficiaryRemoteDataSource.getBeneficiaryDetail(rut).data.toBeneficiaryDetail()

    override suspend fun getBeneficiaryDonationsById(rut: String): List<Donation> =
        beneficiaryRemoteDataSource.getBeneficiaryDonationsById(rut).data.map { it.toDonation() }

    override suspend fun setBeneficiaryFavorite(
        id: String,
        isFavorite: Boolean
    ): ResponseCompletable =
        beneficiaryRemoteDataSource.setBeneficiaryFavorite(id, isFavorite)


    override suspend fun getUserInfo(): UserInfo =
        beneficiaryLocalDataSource.getUserInfo()

    override suspend fun saveFirebaseMessagingServiceToken(token: String): ResponseCompletable =
        beneficiaryRemoteDataSource.saveFirebaseMessagingServiceToken(token)

    override suspend fun deleteFirebaseMessagingServiceToken(token: String): ResponseCompletable =
        beneficiaryRemoteDataSource.deleteFirebaseMessagingServiceToken(token)

    override suspend fun updateNotification() {
        beneficiaryLocalDataSource.updateNotification()
    }

    override suspend fun saveUserInfoToPreferences(name: String, userImage: String) {
        beneficiaryLocalDataSource.saveUserInfoToPreferences(name, userImage)
    }

    override suspend fun getShowCommunesPrompt(): Boolean =
        beneficiaryLocalDataSource.getShowCommunesPrompt()

    override suspend fun getSubscriptions(): List<Subscription> {
        TODO("Not yet implemented")
    }

    override suspend fun deleteSubscription(id: String): ResponseCompletable {
        TODO("Not yet implemented")
    }

    override suspend fun getContributions(): List<Contribution> =
        beneficiaryRemoteDataSource.getContributions().data.map { it.toContribution() }

    override suspend fun getPrograms(): List<Program> =
        beneficiaryRemoteDataSource.getPrograms().data.map { it.toProgram() }

    override suspend fun getKiosks(): List<Kiosk> = mockKiosksMap().map { it.toKiosk() }

    private suspend fun replaceAllBeneficiariesInDb(methods: List<BeneficiaryEntry>) {
        beneficiaryLocalDataSource.deleteAllBeneficiary()
        insertBeneficiariesToDb(with(mapper) { methods.map { it.toBeneficiaryEntity() } })
    }

    private suspend fun insertBeneficiariesToDb(methods: List<BeneficiaryEntity>) {
        beneficiaryLocalDataSource.insertBeneficiariesList(methods)
    }

    private fun mockKiosksMap(): List<KioskEntry> {
        val recoleta = KioskEntry(
            id = "1",
            name = "Villanueva Llancaleo Jaime",
            country = "Chile",
            region = "Región Metropolitana",
            province = "",
            commune = "Renca",
            address = "Angol 1643",
            image = "",
            description = "Teléfono: 226425026",
            lat = -33.399939,
            lng = -70.726047,
            isActive = true
        )

        val quilicura = KioskEntry(
            id = "2",
            name = "Sociedad Comercial Luna SPA",
            country = "Chile",
            region = "Región Metropolitana",
            province = "",
            commune = "San Joaquín",
            address = "Juan Aravena 434 A",
            image = "",
            description = "Teléfono: 949632023",
            lat = -33.512381,
            lng = -70.633906,
            isActive = true
        )
        return listOf(recoleta, quilicura)
    }

}
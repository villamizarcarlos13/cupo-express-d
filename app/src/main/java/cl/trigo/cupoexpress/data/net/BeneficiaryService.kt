package cl.trigo.cupoexpress.data.net

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.cupoexpress.data.model.entry.*
import cl.trigo.cupoexpress.data.model.request.*
import cl.trigo.cupoexpress.data.model.entry.ProgramEntry
import retrofit2.Call
import retrofit2.http.*

interface BeneficiaryService {

    @Headers("Accept: application/json")
    @GET("beneficiaries")
    fun getBeneficiaries(
        @Header("Authorization") authorization: String
    ): Call<Response<List<BeneficiaryEntry>>>


    @GET("communes")
    fun getCommunesList(
    ): Call<Response<List<CommuneEntry>>>

    @Headers("Accept: application/json")
    @POST("versions")
    fun checkVersion(
        @Body request: CheckVersionRequest
    ): Call<Response<CheckVersionEntry>>

    @Headers("Accept: application/json")
    @POST("notifications/token")
    fun saveFirebaseMessagingServiceToken(
        @Header("Authorization") authorization: String,
        @Query("token") token: String
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @DELETE("notifications/token")
    fun deleteFirebaseMessagingServiceToken(
        @Header("Authorization") authorization: String,
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @DELETE("beneficiaries/{id}")
    fun deleteBeneficiaryById(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @POST("beneficiaries")
    fun addBeneficiary(
        @Header("Authorization") authorization: String,
        @Body beneficiaryRequest: BeneficiaryRequest
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @GET("beneficiaries/{rut}")
    fun getBeneficiaryDetail(
        @Header("Authorization") authorization: String,
        @Path("rut") rut: String
    ): Call<Response<BeneficiaryDetailEntry>>

    @Headers("Accept: application/json")
    @GET("beneficiaries/{rut}/donations")
    fun getBeneficiaryDonationsById(
        @Header("Authorization") authorization: String,
        @Path("rut") rut: String
    ): Call<Response<List<DonationEntry>>>

    @Headers("Accept: application/json")
    @PATCH("beneficiaries/{id}")
    fun setBeneficiaryFavorite(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Body setFavoriteRequest: SetFavoriteRequest
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @PATCH("beneficiaries/{id}")
    fun editBeneficiary(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Body beneficiaryRequest: EditBeneficiaryRequest
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @GET("users")
    fun getUserData(
        @Header("Authorization") authorization: String
    ): Call<Response<UserDataResponseEntry>>

    @Headers("Accept: application/json")
    @PATCH("users")
    fun editUser(
        @Header("Authorization") authorization: String,
        @Body userRequest: EditUserRequest
    ): Call<Response<UserEditResponseEntry>>

    @Headers("Accept: application/json")
    @GET("donations")
    fun getContributions(
        @Header("Authorization") authorization: String
    ): Call<Response<List<ContributionEntry>>>

    @Headers("Accept: application/json")
    @GET("programs")
    fun getPrograms(
        @Header("Authorization") authorization: String
    ): Call<Response<List<ProgramEntry>>>

}
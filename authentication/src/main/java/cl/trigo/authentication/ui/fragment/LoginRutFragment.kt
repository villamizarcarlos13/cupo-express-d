package cl.trigo.authentication.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import cl.trigo.authentication.R
import cl.trigo.authentication.domain.model.LoginRut
import cl.trigo.authentication.domain.model.UserRut
import cl.trigo.authentication.ui.viewmodel.LoginRutViewModel
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.validateRut
import kotlinx.android.synthetic.main.fragment_login_rut.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.HttpException

class LoginRutFragment : BaseFragment(), View.OnFocusChangeListener {

    private val loginRutViewModel by viewModel<LoginRutViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginRutViewModel.userByRutLiveData.observe(this, Observer { validateRutObserver(it) })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupClickListeners()
    }

    private fun setupClickListeners() {
        login_rut_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        input_rut.editText?.onFocusChangeListener = this
        input_rut.hint = resources.getString(R.string.rut_input_hint)
        input_rut.isHintEnabled = false

        btn_next.setOnClickListener {
            val rut = input_rut.editText!!.text.toString()
            if (!validateRutInput(rut)) return@setOnClickListener
            loginRutViewModel.validateRut(rut)
        }
    }

    private fun validateRutInput(rut: String): Boolean {
        rut.let {
            if (!validateRut(it)) {
                input_rut.editText!!.error = "El Rut no es válido"
                input_rut.editText!!.requestFocus()
                return false
            }
        }
        return true
    }

    private fun navigateToPasswordFragment(user: UserRut) {
        val direction =
            LoginRutFragmentDirections.actionRutToPassword(user)
        NavHostFragment.findNavController(this).navigate(direction)
    }

    private fun navigateToRegisterFragment() {
        val direction =
            LoginRutFragmentDirections.actionRutToRegister(input_rut.editText!!.text.toString())
        NavHostFragment.findNavController(this).navigate(direction)
    }

    private fun showLoading(isLoading: Boolean) {
        login_rut_loading.isVisible = isLoading
    }

    private fun validateRutObserver(result: Result<LoginRut>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
                btn_next.isEnabled = false
            }
            is Result.OnSuccess -> {
                showLoading(false)
                btn_next.isEnabled = true
                if (result.value.user.firstname.isNullOrEmpty()) {
                    navigateToRegisterFragment()
                } else {
                    navigateToPasswordFragment(result.value.user)
                }
            }
            is Result.OnError -> {
                showLoading(false)
                btn_next.isEnabled = true
                if (result.throwable is HttpException) {
                    val exception: HttpException = result.throwable as HttpException
                    when (exception.code()) {
                        404 -> {
                            navigateToRegisterFragment()
                        }
                        else -> {
                            val errorMessage = result.throwable.message!!
                            errorMessage.run {
                                showErrorDialog(message = this)
                            }
                        }
                    }
                } else {
                    val errorMessage = result.throwable.message!!
                    errorMessage.run {
                        showErrorDialog(message = this)
                    }
                }
            }
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (hasFocus) {
            input_rut.boxBackgroundColor = resources.getColor(R.color.white)
        } else {
            input_rut.boxBackgroundColor = resources.getColor(R.color.background_white)
        }
    }

    override fun getLayoutId() = R.layout.fragment_login_rut

}

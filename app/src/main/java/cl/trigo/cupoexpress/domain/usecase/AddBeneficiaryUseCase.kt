package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.CompletableUseCase
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class AddBeneficiaryUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : CompletableUseCase<Map<String, String>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    companion object {
        const val RUT_PARAM = "RUT_PARAM"
        const val FIRST_NAME_PARAM = "FIRST_NAME_PARAM"
        const val LAST_NAME_PARAM = "LAST_NAME_PARAM"
        const val COMMUNE_PARAM = "COMMUNE_PARAM"
        const val EMAIL_PARAM = "EMAIL_PARAM"
        const val PHONE_PARAM = "PHONE_PARAM"
        const val PHOTO_PARAM = "PHOTO_PARAM"
    }

    override suspend fun executeOnBackground(params: Map<String, String>) {
        if (params.containsKey(RUT_PARAM) &&
            params.containsKey(FIRST_NAME_PARAM) &&
            params.containsKey(LAST_NAME_PARAM) &&
            params.containsKey(COMMUNE_PARAM) &&
            (params.containsKey(EMAIL_PARAM) || params.containsKey(PHONE_PARAM))
        ) {

            val rut = params.getValue(RUT_PARAM)
            val firstName = params.getValue(FIRST_NAME_PARAM)
            val lastName = params.getValue(LAST_NAME_PARAM)
            val email = params.getValue(EMAIL_PARAM)
            val phone = params.getValue(PHONE_PARAM)
            val commune = params.getValue(COMMUNE_PARAM)
            val photo = params.getValue(PHOTO_PARAM)

            beneficiaryRepository.addBeneficiary(rut, firstName, lastName, email, phone, photo, commune )
        }
    }
}
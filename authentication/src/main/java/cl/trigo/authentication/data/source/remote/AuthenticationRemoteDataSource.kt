package cl.trigo.authentication.data.source.remote

import cl.trigo.core.base.Response
import cl.trigo.authentication.data.model.entry.LoginEntry
import cl.trigo.authentication.data.model.entry.RegisterEntry
import cl.trigo.authentication.data.model.entry.RutEntry
import cl.trigo.authentication.data.net.AuthenticationService
import cl.trigo.authentication.data.model.request.LoginRequest
import cl.trigo.authentication.data.model.request.RegisterRequest
import cl.trigo.authentication.data.model.request.RestorePassParams
import cl.trigo.authentication.data.source.AuthenticationDataSource
import cl.trigo.core.base.ResponseCompletable
import retrofit2.await

open class AuthenticationRemoteDataSource(
    private val authenticationService: AuthenticationService
) : AuthenticationDataSource {
    override suspend fun signUp(
        firstName: String,
        lastName: String,
        rut: String,
        email: String,
        password: String
    ): Response<RegisterEntry> {
        val registerRequest =
            RegisterRequest(
                firstName,
                lastName,
                rut,
                email,
                password
            )
        return authenticationService.signUp(registerRequest).await()
    }

    override suspend fun signIn(email: String, password: String): Response<LoginEntry> {
        val loginRequest =
            LoginRequest(
                email,
                password
            )
        return authenticationService.signIn(loginRequest).await()
    }

    override suspend fun verifyUserByRut(rut: String): Response<RutEntry> {
        return authenticationService.verifyUserByRut(rut).await()
    }

    override suspend fun restorePassword(email: String): ResponseCompletable =
        authenticationService.restorePassword(
            RestorePassParams(email = email)
        ).await()
}
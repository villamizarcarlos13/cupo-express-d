package cl.trigo.cupoexpress.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.observe
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Subscription
import cl.trigo.cupoexpress.ui.fragment.adapter.SubscriptionsAdapter
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickSubscription
import cl.trigo.cupoexpress.ui.viewmodel.SubscriptionsViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_subscriptions.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SubscriptionsFragment : BaseFragment(), OnClickSubscription {

    private val viewModel by viewModel<SubscriptionsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(getSubscriptionsLiveData, ::getSubscriptionsObserver)
            observe(deleteSubscriptionLiveData, ::deleteSubscriptionObserver)
            getSubscriptionsList()
        }
        subscriptions_list.layoutManager = LinearLayoutManager(context)
    }

    override fun onClickItem(subscription: Subscription) {

    }

    override fun onClickDelete(subscription: Subscription) {
        showDeleteDialog(subscription)
    }

    private fun showDeleteDialog(subscription: Subscription) {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.delete_subscription))
                .setMessage(
                    getString(
                        R.string.delete_subscription_message,
                        subscription.fullname
                    )
                )
                .setPositiveButton(getString(R.string.delete)) { _, _ ->
                    viewModel.deleteSubscription(
                        subscription.id.orEmpty()
                    )
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun deleteSubscriptionObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {

            }
            is Completable.OnComplete -> {
                viewModel.getSubscriptionsList()
            }
            is Completable.OnError -> {
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    private fun getSubscriptionsObserver(result: Result<List<Subscription>>?) {
        when (result) {
            is Result.OnLoading -> {

            }
            is Result.OnSuccess -> {
                if (result.value.isEmpty()) {
                    subscriptions_list.isVisible = false
                    tv_no_subscriptions.isVisible = true
                } else {
                    subscriptions_list.isVisible = true
                    tv_no_subscriptions.isVisible = false
                    subscriptions_list.adapter = SubscriptionsAdapter(result.value, this)
                }
            }
            is Result.OnError -> {
                subscriptions_list.isVisible = false
                tv_no_subscriptions.isVisible = true
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_subscriptions

}
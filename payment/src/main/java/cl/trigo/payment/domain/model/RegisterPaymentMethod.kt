package cl.trigo.payment.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class RegisterPaymentMethod(
    val token: String,
    val formAction: String
) : Parcelable
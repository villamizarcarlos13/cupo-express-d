package cl.trigo.core.extension

import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

fun ImageView.load(url: String) {
    Glide.with(this)
        .load(url)
        .centerCrop()
        .into(this)
}

fun ImageView.loadWithPlaceHolder(
    url: String,
    placeHolderId: Int
) {
    Glide.with(this)
        .load(url)
        .error(ContextCompat.getDrawable(context, placeHolderId))
        .centerCrop()
        .into(this)
}

fun ImageView.loadWithRoundTransform(url: String, placeHolderId: Int) {
    Glide.with(this)
        .load(url)
        .error(ContextCompat.getDrawable(context, placeHolderId))
        .transform(CenterCrop(), RoundedCorners(24))
        .into(this)
}

fun ImageView.loadWithCircleCrop(url: String?, placeHolderId: Int) {
    if (isValidUrl(url)) {
        Glide.with(this)
            .load(url)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .error(placeHolderId)
            .circleCrop()
            .into(this)
    }
}

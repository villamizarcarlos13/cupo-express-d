package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.Subscription
import cl.trigo.cupoexpress.domain.usecase.DeleteSubscriptionUseCase
import cl.trigo.cupoexpress.domain.usecase.GetSubscriptionsUseCase

class SubscriptionsViewModel(
    private val deleteSubscriptionUseCase: DeleteSubscriptionUseCase,
    private val getSubscriptionsUseCase: GetSubscriptionsUseCase
) : ViewModel(

) {
    val deleteSubscriptionLiveData = LiveCompletable()
    val getSubscriptionsLiveData = LiveResult<List<Subscription>>()

    fun deleteSubscription(id: String) =
        deleteSubscriptionUseCase.execute(deleteSubscriptionLiveData, id)

    fun getSubscriptionsList() = getSubscriptionsUseCase.execute(getSubscriptionsLiveData)


}
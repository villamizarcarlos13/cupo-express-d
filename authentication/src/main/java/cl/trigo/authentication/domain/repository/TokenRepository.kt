package cl.trigo.authentication.domain.repository

import cl.trigo.authentication.domain.model.User

interface TokenRepository {
    suspend fun saveAuthToken(token: String)
    suspend fun getAuthToken(): String?
    suspend fun closeSession(): Boolean
    suspend fun saveUserInfo(user: User)
}
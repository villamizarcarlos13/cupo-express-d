package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.Benefactor

@Keep
data class BenefactorEntry(
    val id: String?,
    val firstName: String?,
    val lastName: String?,
    val profileUrl: String?,
    val region: String?,
    val country: String?,
    val email: String?,
    val rut: String?,
    val phone: String?
)

fun BenefactorEntry.toBenefactor(): Benefactor {
    return Benefactor(
        id = id.orEmpty(),
        firstName = firstName.orEmpty(),
        lastName = lastName.orEmpty(),
        profileUrl = profileUrl.orEmpty(),
        fullName = "${firstName.orEmpty()} ${lastName.orEmpty()}",
        region = region.orEmpty(),
        country = country.orEmpty(),
        email = email.orEmpty(),
        rut = rut.orEmpty(),
        phone = phone.orEmpty()
    )
}
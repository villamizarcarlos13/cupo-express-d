package cl.trigo.payment.ui.fragment.listener

import cl.trigo.payment.domain.model.Benefit

interface OnClickBenefitListener {
    fun onClickItem(benefit: Benefit)
}
package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.core.extension.getTimeAgo
import cl.trigo.core.extension.loadWithCircleCrop
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Beneficiary
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickBeneficiary
import cl.trigo.cupoexpress.ui.fragment.listener.OnLongClickBeneficiary
import kotlinx.android.synthetic.main.item_benefited.view.*

class BenefitedAdapter(
    private val list: List<Beneficiary>,
    private val onClickBeneficiary: OnClickBeneficiary,
    private val onLongClickBeneficiary: OnLongClickBeneficiary
) : RecyclerView.Adapter<BenefitedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_benefited, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        val timeList = holder.view.context.resources.getStringArray(R.array.time_ago_array)
        val timeAgo = getTimeAgo(item.lastDonationDate, timeList)
        holder.name.text =
            holder.view.context.getString(R.string.user_full_name, item.firstname, item.lastname)
        if (timeAgo.isNotEmpty()){
            holder.lastContribution.text =
                holder.view.context.getString(R.string.contribution_date, timeAgo)
        }else{
            holder.lastContribution.text =
                holder.view.context.getString(R.string.contributions_not_registered)
        }

        holder.isFavorite.isVisible = item.isFavorite
        holder.userImage.loadWithCircleCrop(item.photo, R.drawable.ic_default_photo)

        with(holder.view) {
            tag = item
            setOnClickListener {
                onClickBeneficiary.onClick(list[position])
                return@setOnClickListener
            }
            setOnLongClickListener {
                onLongClickBeneficiary.onLongClick(list[position])
                return@setOnLongClickListener true
            }
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.tv_user_name
        val lastContribution: TextView = view.tv_last_contribution
        val userImage: ImageView = view.iv_user_photo
        val isFavorite: ImageView = view.iv_favorite
        val counter: TextView = view.tv_contribution_counter
    }
}

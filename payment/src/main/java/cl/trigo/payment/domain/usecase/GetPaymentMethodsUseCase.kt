package cl.trigo.payment.domain.usecase

import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.domain.repository.PaymentRepository
import kotlinx.coroutines.flow.Flow

class GetPaymentMethodsUseCase(private val paymentRepository: PaymentRepository) {
    suspend fun getPaymentMethods(forceUpdate: Boolean): Flow<List<PaymentMethod>> =
        paymentRepository.getPaymentMethods(forceUpdate)
}
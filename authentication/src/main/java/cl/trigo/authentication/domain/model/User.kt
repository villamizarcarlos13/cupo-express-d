package cl.trigo.authentication.domain.model

data class User(
    val firstname: String,
    val lastname: String,
    val rut: String,
    val email: String,
    val password: String,
    val role: String,
    val phone: String,
    val photo: String,
    val country: String,
    val city: String,
    val region: String,
    val address: String,
    val active: Boolean
)
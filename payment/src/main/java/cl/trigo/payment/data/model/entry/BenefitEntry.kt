package cl.trigo.payment.data.model.entry

import androidx.annotation.Keep
import cl.trigo.payment.domain.model.Benefit

@Keep
data class BenefitEntry(
    val code: String?,
    val name: String?,
    val description: String?,
    val shortName: String?,
    val price: Int,
    val stock: Int,
    val startavailable: String?,
    val endavailable: String?,
    val images: List<String?>,
    val active: Boolean,
    val id: String?
)

fun BenefitEntry.toBenefit(): Benefit {
    return Benefit(
        code.orEmpty(),
        name.orEmpty(),
        description.orEmpty(),
        shortName.orEmpty(),
        price,
        stock,
        startavailable.orEmpty(),
        endavailable.orEmpty(),
        images,
        active,
        id.orEmpty()
    )
}
package cl.trigo.payment.data.model.entry

import androidx.annotation.Keep

@Keep
data class PaymentMethodEntry(
    val id: String?,
    val creditCardType: String?,
    val lastCardDigits: String?,
    val isLast: Boolean?
)

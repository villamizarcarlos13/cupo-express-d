package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUseCase
import cl.trigo.cupoexpress.domain.model.BeneficiaryDetail
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetBeneficiaryDetailByRutUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUseCase<String, BeneficiaryDetail>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(params: String): BeneficiaryDetail? {
        return beneficiaryRepository.getBeneficiaryDetail(rut = params)
    }
}
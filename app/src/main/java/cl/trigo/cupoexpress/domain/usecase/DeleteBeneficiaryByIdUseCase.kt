package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository

class DeleteBeneficiaryByIdUseCase(private val beneficiaryRepository: BeneficiaryRepository) {
    suspend fun deleteBeneficiary(params: String) {
        beneficiaryRepository.deleteBeneficiaryById(id = params)
    }
}
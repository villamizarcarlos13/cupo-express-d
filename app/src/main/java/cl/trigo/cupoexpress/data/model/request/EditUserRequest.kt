package cl.trigo.cupoexpress.data.model.request

import androidx.annotation.Keep

@Keep
data class EditUserRequest(
    val firstname: String?,
    val lastname: String?,
    val email: String?,
    val phone: String?,
    val photo: String?,
    val phototype: String
)
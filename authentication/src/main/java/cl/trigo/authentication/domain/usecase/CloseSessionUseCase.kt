package cl.trigo.authentication.domain.usecase

import cl.trigo.core.coroutines.CompletableEmptyUseCase
import cl.trigo.authentication.domain.repository.TokenRepository
import kotlinx.coroutines.Dispatchers

class CloseSessionUseCase(
    private val tokenRepository: TokenRepository
) : CompletableEmptyUseCase(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground() {
        tokenRepository.closeSession()
    }
}
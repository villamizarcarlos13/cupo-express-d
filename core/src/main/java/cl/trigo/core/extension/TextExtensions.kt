package cl.trigo.core.extension

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.webkit.URLUtil
import java.text.*
import java.util.*

fun getSpannedText(text: String): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
    } else {
        Html.fromHtml(text)
    }
}

fun validate(rut: Int, dv: Char): Boolean {
    var result = rut
    var m = 0
    var s = 1
    while (result != 0) {
        s = (s + result % 10 * (9 - m++ % 6)) % 11
        result /= 10
    }
    return dv == (if (s != 0) s + 47 else 75).toChar()
}

@SuppressLint("DefaultLocale")
fun validateRut(rut: String): Boolean {
    if (rut.isEmpty()) return false
    val numberString =
        rut.substring(0, rut.length - 1).replace("-", "").replace(".", "")
    val dv = rut.toUpperCase()[rut.length - 1]
    val number: Int
    number = try {
        numberString.toInt()
    } catch (e: Exception) {
        return false
    }
    return validate(number, dv)
}

fun removeSpecialCharactersFromRut(rut: String): String {
    return rut.filter { it.isLetterOrDigit() }
}

fun validatePasswordComplexity(password: String): Boolean {
    val specialCharacters = "-@%\\[\\}+'!/#%|$^?:;,\\(\"\\)~`.*=&\\{>\\]<_"
    val PASSWORD_PATTERN =
        "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$specialCharacters]).{6,})"
    return PASSWORD_PATTERN.toRegex().matches(password)
}

fun rutFormatter(rut: String?): String? {
    if (rut == null) return null
    var original = rut.replace("[^kK0-9]".toRegex(), "")
    if (original.length <= 1) {
        return original
    }
    if (original.length > 9) {
        original = original.substring(0, 9)
    }
    val verifier = original[original.length - 1]
    original = original.substring(0, original.length - 1)
    original = original.replace("[^0-9]".toRegex(), "")
    return if (original.length > 0) {
        val number: String = formatThousands(original)
        "$number-$verifier"
    } else {
        "" + verifier
    }
}

fun formatThousands(input: String): String {
    val number = input.replace(".", "")
    val df = DecimalFormat(
        "#,###",
        DecimalFormatSymbols(Locale("es", "CL"))
    )
    return df.format(number.toLong())
}

fun formatAmount(amount: Int?): String {
    val amountPattern = "###,###.##"
    val sign = "$"

    return try {
        val nf = NumberFormat.getNumberInstance(Locale.getDefault())
        val formatter = nf as DecimalFormat
        formatter.applyPattern(amountPattern)
        sign + formatter.format(amount)
    } catch (e: Exception) {
        ""
    }
}

fun normalizeDate(timestamp: String): String? {
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
    val convertedDate: Date?
    var formattedDate = ""
    try {
        val spanish = Locale("es", "ES")
        convertedDate = sdf.parse(timestamp)
        if (convertedDate != null) {
            formattedDate = SimpleDateFormat("d 'de' MMMM, yyyy", spanish).format(convertedDate)
        }
    } catch (e: ParseException) {
        return formattedDate
    }
    return formattedDate
}

fun isValidUrl(url: String?) : Boolean {
    return URLUtil.isValidUrl(url)
}
package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.core.extension.normalizeDate
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.Contribution
import cl.trigo.cupoexpress.ui.fragment.listener.OnClickContribution
import kotlinx.android.synthetic.main.item_contribution.view.*
import kotlinx.android.synthetic.main.item_subscription.view.tv_beneficiary_name

class ContributionsAdapter(
    private val list: List<Contribution>,
    private val onClickListener: OnClickContribution
) : RecyclerView.Adapter<ContributionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_contribution, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.name.text = item.beneficiaries[0].beneficiary.fullname.orEmpty()
        holder.amount.text = item.totalDonation
        holder.date.text = normalizeDate(item.createdAt).orEmpty()

        with(holder.view) {
            tag = item
            setOnClickListener {
                onClickListener.onClickItem(list[position])
                return@setOnClickListener
            }
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.tv_beneficiary_name
        val date: TextView = view.tv_contribution_date
        val amount: TextView = view.tv_contribution_amount
    }
}
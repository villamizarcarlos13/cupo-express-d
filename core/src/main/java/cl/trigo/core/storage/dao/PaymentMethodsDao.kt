package cl.trigo.core.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cl.trigo.core.storage.entity.PaymentMethodEntity

@Dao
interface PaymentMethodsDao {
    @Query("SELECT * FROM payment_methods WHERE id = :paymentMethodId")
    suspend fun getPaymentMethodById(paymentMethodId: String): PaymentMethodEntity

    @Query("SELECT * FROM payment_methods")
    suspend fun getPaymentMethods(): List<PaymentMethodEntity>

    @Query("DELETE FROM payment_methods")
    suspend fun deleteAllPaymentMethods()

    @Query("DELETE FROM payment_methods WHERE id = :methodId")
    suspend fun deleteMethodById(methodId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePaymentMethod(paymentMethod: PaymentMethodEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePaymentMethods(paymentMethods: List<PaymentMethodEntity>)
}
package cl.trigo.payment.data

import cl.trigo.core.base.ResponseCompletable
import cl.trigo.core.exceptions.DeletePaymentMethodException
import cl.trigo.core.storage.entity.PaymentMethodEntity
import cl.trigo.payment.data.local.PaymentLocal
import cl.trigo.payment.data.mapper.PaymentDataMapper
import cl.trigo.payment.data.model.entry.PaymentMethodEntry
import cl.trigo.payment.data.model.entry.toBenefit
import cl.trigo.payment.data.model.entry.toRegisterPaymentMethod
import cl.trigo.payment.data.remote.PaymentRemote
import cl.trigo.payment.domain.model.Benefit
import cl.trigo.payment.domain.model.PaymentMethod
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import cl.trigo.payment.domain.repository.PaymentRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PaymentDataRepository(
    private val paymentRemoteDataSource: PaymentRemote,
    private val paymentLocalDataSource: PaymentLocal,
    private val mapper: PaymentDataMapper
) : PaymentRepository {

    override suspend fun getPaymentMethods(forceUpdate: Boolean): Flow<List<PaymentMethod>> = flow {
        val localMethods = paymentLocalDataSource.getAllPaymentMethods()
        emit(with(mapper) { localMethods.map { it.toPaymentMethod() }})
        if (forceUpdate) {
            val remoteMethods = paymentRemoteDataSource.getPaymentMethods()
            emit(with(mapper) {remoteMethods.data.map { it.toPaymentMethod() }})
            replaceAllMethodsInDb(remoteMethods.data)
        }
    }

    override suspend fun getBenefits(): List<Benefit> =
        paymentRemoteDataSource.getBenefits().data.map { it.toBenefit() }

    override suspend fun deletePaymentMethod(id: String) {
        runCatching { paymentRemoteDataSource.deletePaymentMethodById(id) }
            .onSuccess { paymentLocalDataSource.deletePaymentMethod(id) }
            .onFailure { throw DeletePaymentMethodException()}
    }

    override suspend fun confirmPayment(
        benefitId: String,
        beneficiaryId: String,
        quantity: String,
        paymentMethodId: String
    ): ResponseCompletable =
        paymentRemoteDataSource.confirmPayment(
            benefitId,
            beneficiaryId,
            quantity.toInt(),
            paymentMethodId
        )

    override suspend fun registerPaymentMethod(): RegisterPaymentMethod =
        paymentRemoteDataSource.registerPaymentMethod().data.toRegisterPaymentMethod()

    private suspend fun replaceAllMethodsInDb(methods: List<PaymentMethodEntry>) {
        paymentLocalDataSource.deleteAllPaymentsMethods()
        insertMethodsToDb(with(mapper) { methods.map { it.toPaymentMethodEntity() } })
    }

    private suspend fun insertMethodsToDb(methods: List<PaymentMethodEntity>) {
        paymentLocalDataSource.insertPaymentMethodsList(methods)
    }
}
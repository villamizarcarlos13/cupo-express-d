package cl.trigo.cupoexpress.data.model.entry

data class BenefitEntry(
    val name: String?,
    val description: String?,
    val price: String?,
    val shortName: String?
)
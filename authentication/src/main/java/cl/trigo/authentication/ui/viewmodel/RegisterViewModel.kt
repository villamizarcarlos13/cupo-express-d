package cl.trigo.authentication.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.authentication.domain.model.Register
import cl.trigo.authentication.domain.model.User
import cl.trigo.authentication.domain.usecase.LoginUseCase
import cl.trigo.authentication.domain.usecase.RegisterUserUseCase
import cl.trigo.core.extension.LiveResult
import cl.trigo.authentication.domain.usecase.RegisterUserUseCase.Companion.EMAIL_PARAM
import cl.trigo.authentication.domain.usecase.RegisterUserUseCase.Companion.FIRST_NAME_PARAM
import cl.trigo.authentication.domain.usecase.RegisterUserUseCase.Companion.LAST_NAME_PARAM
import cl.trigo.authentication.domain.usecase.RegisterUserUseCase.Companion.PASSWORD_PARAM
import cl.trigo.authentication.domain.usecase.RegisterUserUseCase.Companion.RUT_PARAM

class RegisterViewModel(
    private val registerUserUseCase: RegisterUserUseCase,
    private val loginUseCase: LoginUseCase
) : ViewModel() {

    val registerLiveData = LiveResult<Register>()
    val loginLiveData = LiveResult<User>()

    fun login(email: String, password: String) = loginUseCase.execute(
        loginLiveData, mapOf(
            LoginUseCase.EMAIL_PARAM to email,
            LoginUseCase.PASSWORD_PARAM to password
        )
    )

    fun register(
        firstName: String,
        lastName: String,
        rut: String,
        email: String,
        password: String) = registerUserUseCase.execute(
        registerLiveData, mapOf(
            FIRST_NAME_PARAM to firstName,
            LAST_NAME_PARAM to lastName,
            RUT_PARAM to rut,
            EMAIL_PARAM to email,
            PASSWORD_PARAM to password
        )
    )


}
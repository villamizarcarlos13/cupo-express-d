package cl.trigo.cupoexpress.notifications.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu

import androidx.recyclerview.widget.RecyclerView
import cl.trigo.core.extension.load
import cl.trigo.core.extension.millisToDate
import cl.trigo.cupoexpress.notifications.NotificationConstants.HEADER_NEW_NOTIFICATION
import cl.trigo.cupoexpress.notifications.NotificationConstants.HEADER_READ_NOTIFICATION
import cl.trigo.cupoexpress.notifications.NotificationConstants.NEW_NOTIFICATION
import cl.trigo.cupoexpress.notifications.NotificationConstants.READ_NOTIFICATION
import cl.trigo.cupoexpress.notifications.R
import cl.trigo.cupoexpress.notifications.databinding.ItemNotificationHeaderBinding
import cl.trigo.cupoexpress.notifications.databinding.ItemNotificationNewBinding
import cl.trigo.cupoexpress.notifications.databinding.ItemNotificationReadBinding
import cl.trigo.cupoexpress.notifications.ui.fragment.listener.NotificationMenuClickListener
import cl.trigo.cupoexpress.notifications.ui.model.Notification


class NotificationAdapter(
    private val notifications: List<Notification>,
    private val listener: NotificationMenuClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_NEW = NEW_NOTIFICATION
        const val VIEW_TYPE_READ = READ_NOTIFICATION
        const val VIEW_TYPE_HEADER_NEW = HEADER_NEW_NOTIFICATION
        const val VIEW_TYPE_HEADER_READ = HEADER_READ_NOTIFICATION
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_NEW -> {
                NewNotificationViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_notification_new, parent, false)
                )
            }
            VIEW_TYPE_READ -> {
                ReadNotificationViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_notification_read, parent, false)
                )
            }
            VIEW_TYPE_HEADER_NEW -> {
                HeaderNewViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_notification_header, parent, false)
                )
            }
            else -> HeaderReadViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_notification_header, parent, false)
            )
        }
    }

    override fun getItemCount(): Int = notifications.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (notifications[position].viewType) {
            VIEW_TYPE_NEW -> (holder as NewNotificationViewHolder).bind(position)
            VIEW_TYPE_READ -> (holder as ReadNotificationViewHolder).bind(position)
            VIEW_TYPE_HEADER_NEW -> (holder as HeaderNewViewHolder).bind()
            else -> (holder as HeaderReadViewHolder).bind()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return notifications[position].viewType
    }

    private inner class NewNotificationViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val binding = ItemNotificationNewBinding.bind(itemView)
        fun bind(position: Int) {
            val listItem = notifications[position]
            binding.apply {
                tvNotificationBody.text = listItem.body
                ivNotificationIcon.load(listItem.iconUrl)
                tvNotificationTimestamp.text = millisToDate(
                    listItem.dateCreated,
                    tvNotificationTimestamp.context.resources.getString(R.string.yesterday)
                )
                ivNotificationMenu.setOnClickListener {
                    val popupMenu = PopupMenu(ivNotificationMenu.context, ivNotificationMenu)
                    popupMenu.inflate(R.menu.menu_notification_new)
                    popupMenu.setOnMenuItemClickListener { item ->
                        when (item.itemId) {
                            R.id.action_mark_as_read -> listener.onClickMarkAsRead(listItem)
                            R.id.action_delete_new -> listener.onClickDelete(listItem)
                        }
                        true
                    }
                    popupMenu.show()
                }
            }
        }
    }

    private inner class ReadNotificationViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val binding = ItemNotificationReadBinding.bind(itemView)
        fun bind(position: Int) {
            val listItem = notifications[position]
            binding.apply {
                tvNotificationBody.text = listItem.body
                ivNotificationIcon.load(listItem.iconUrl)
                tvNotificationTimestamp.text = millisToDate(
                    listItem.dateCreated,
                    tvNotificationTimestamp.context.resources.getString(R.string.yesterday)
                )
                ivNotificationMenu.setOnClickListener {
                    val popupMenu = PopupMenu(ivNotificationMenu.context, ivNotificationMenu)
                    popupMenu.inflate(R.menu.menu_notification_read)
                    popupMenu.setOnMenuItemClickListener { item ->
                        when (item.itemId) {
                            R.id.action_delete -> listener.onClickDelete(listItem)
                        }
                        true
                    }
                    popupMenu.show()
                }
            }
        }
    }

    private inner class HeaderNewViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val binding = ItemNotificationHeaderBinding.bind(itemView)
        fun bind() {
            binding.tvHeaderTitle.text = itemView.context.resources.getString(R.string.news)
        }
    }

    private inner class HeaderReadViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val binding = ItemNotificationHeaderBinding.bind(itemView)
        fun bind() {
            binding.tvHeaderTitle.text = itemView.context.resources.getString(R.string.previous)
        }
    }
}
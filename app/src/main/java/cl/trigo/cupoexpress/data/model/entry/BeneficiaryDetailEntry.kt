package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.BeneficiaryDetail

@Keep
data class BeneficiaryDetailEntry(
    val beneficiary: BeneficiaryEntry
)

fun BeneficiaryDetailEntry.toBeneficiaryDetail(): BeneficiaryDetail {
    return BeneficiaryDetail(
        beneficiary.toBeneficiary()
    )
}
package cl.trigo.payment.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.payment.domain.model.RegisterPaymentMethod
import cl.trigo.payment.domain.repository.PaymentRepository
import kotlinx.coroutines.Dispatchers

class RegisterPaymentMethodUseCase(
    private val paymentRepository: PaymentRepository
) : ResultUnitUseCase<RegisterPaymentMethod>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): RegisterPaymentMethod? {
        return paymentRepository.registerPaymentMethod()
    }
}
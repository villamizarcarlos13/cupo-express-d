package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.CompletableUseCase
import cl.trigo.cupoexpress.domain.model.SetFavoriteParams
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class SetBeneficiaryFavoriteUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : CompletableUseCase<SetFavoriteParams>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(params: SetFavoriteParams) {
        beneficiaryRepository.setBeneficiaryFavorite(params.id, params.isFavorite)
    }
}

package cl.trigo.authentication.data.repository.local

import cl.trigo.authentication.data.source.TokenDataSource
import cl.trigo.authentication.domain.model.User
import cl.trigo.authentication.domain.repository.TokenRepository

class TokenLocalRepository(
    private val tokenLocalDataSource: TokenDataSource
) : TokenRepository {
    override suspend fun saveAuthToken(token: String) {
        tokenLocalDataSource.saveAuthToken(token)
    }

    override suspend fun getAuthToken(): String? = tokenLocalDataSource.getAuthToken()
    override suspend fun closeSession(): Boolean = tokenLocalDataSource.closeSession()
    override suspend fun saveUserInfo(user: User) = tokenLocalDataSource.saveUserInfo(user)
}
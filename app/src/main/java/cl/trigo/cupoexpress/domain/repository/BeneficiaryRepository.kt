package cl.trigo.cupoexpress.domain.repository

import cl.trigo.core.base.ResponseCompletable
import cl.trigo.cupoexpress.domain.model.*
import kotlinx.coroutines.flow.Flow

interface BeneficiaryRepository {
    suspend fun getBeneficiaries(forceUpdate: Boolean): Flow<List<Beneficiary>>
    suspend fun getUserData(): User
    suspend fun editUserData(
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        photo: String?
    ): UserEditResponse

    suspend fun getCommunes(): Communes
    suspend fun deleteBeneficiaryById(id: String)
    suspend fun checkVersion(): CheckVersion
    suspend fun addBeneficiary(
        id: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        photo: String,
        commune: String

    ): ResponseCompletable

    suspend fun editBeneficiary(
        id: String,
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        commune: String?,
        photo: String?
    ): ResponseCompletable

    suspend fun getBeneficiaryDetail(rut: String): BeneficiaryDetail
    suspend fun getBeneficiaryDonationsById(rut: String): List<Donation>
    suspend fun setBeneficiaryFavorite(id: String, isFavorite: Boolean): ResponseCompletable
    suspend fun getUserInfo(): UserInfo
    suspend fun saveFirebaseMessagingServiceToken(token: String): ResponseCompletable
    suspend fun deleteFirebaseMessagingServiceToken(token: String): ResponseCompletable
    suspend fun updateNotification()
    suspend fun saveUserInfoToPreferences(name: String, userImage: String)
    suspend fun getShowCommunesPrompt(): Boolean
    suspend fun getSubscriptions(): List<Subscription>
    suspend fun getContributions(): List<Contribution>
    suspend fun deleteSubscription(id: String): ResponseCompletable
    suspend fun getPrograms(): List<Program>
    suspend fun getKiosks(): List<Kiosk>
}
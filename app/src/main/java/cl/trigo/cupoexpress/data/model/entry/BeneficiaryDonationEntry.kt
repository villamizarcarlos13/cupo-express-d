package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.core.extension.formatAmount
import cl.trigo.cupoexpress.domain.model.BeneficiaryDonation

@Keep
data class BeneficiaryDonationEntry(
    val type: String?,
    val amount: Int?,
    val beneficiary: BeneficiaryEntry,
    val quantity: Int?
)

fun BeneficiaryDonationEntry.toBeneficiaryDonation(): BeneficiaryDonation {
    return BeneficiaryDonation(
        type = type.orEmpty(),
        amount = formatAmount(amount?.or(0)),
        beneficiary = beneficiary.toBeneficiary(),
        quantity = quantity?.or(0)
    )
}
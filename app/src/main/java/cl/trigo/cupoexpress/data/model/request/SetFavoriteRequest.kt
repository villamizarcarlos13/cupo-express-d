package cl.trigo.cupoexpress.data.model.request

import androidx.annotation.Keep

@Keep
data class SetFavoriteRequest(
    val isFavorite: Boolean
)
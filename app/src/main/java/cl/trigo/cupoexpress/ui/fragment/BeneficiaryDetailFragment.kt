package cl.trigo.cupoexpress.ui.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.exceptions.DeleteBeneficiaryException
import cl.trigo.core.extension.*
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.BeneficiaryDetail
import cl.trigo.cupoexpress.domain.model.Donation
import cl.trigo.cupoexpress.domain.model.LastBenefactor
import cl.trigo.cupoexpress.ui.fragment.adapter.DonationsAdapter
import cl.trigo.cupoexpress.ui.viewmodel.BeneficiaryDetailViewModel
import cl.trigo.payment.ui.activity.DirectContributionActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_beneficiary_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.ArrayList

class BeneficiaryDetailFragment : BaseFragment() {

    private val beneficiaryDetailViewModel by viewModel<BeneficiaryDetailViewModel>()
    private var beneficiaryDetail: BeneficiaryDetail? = null
    private var beneficiaryActivities : ArrayList<String> = arrayListOf()
    private var beneficiaryContributions : ArrayList<Donation> = arrayListOf()
    private val args: BeneficiaryDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(beneficiaryDetailViewModel) {
            observe(getBeneficiaryDetailLiveData, ::getBeneficiariesObserver)
            observe(getBeneficiaryDonationsLiveData, ::getDonationsObserver)
            observe(deleteBeneficiaryLiveData, ::deleteBeneficiaryObserver)
            observe(setFavoriteLiveData, ::setFavoriteBeneficiaryObserver)
        }
        listBeneficiaryDetails.layoutManager = LinearLayoutManager(context)
        setupToolbar()
        setupClickListeners()
    }

    override fun onResume() {
        super.onResume()
        beneficiaryDetailViewModel.getBeneficiaryDetails(args.beneficiaryId)
    }

    private fun setupToolbar() {
        beneficiary_top_bar.setNavigationOnClickListener {
            activity?.setResult(Activity.RESULT_OK)
            activity?.finish()
        }

        beneficiary_top_bar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_delete -> {
                    deleteBeneficiary()
                    true
                }
                R.id.action_edit -> {
                    editBeneficiary()
                    true
                }
                R.id.action_set_favorite -> {
                    if (beneficiaryDetail?.beneficiary?.isFavorite == true) {
                        removeBeneficiaryFromFavorite()
                    } else {
                        addBeneficiaryToFavorite()
                    }
                    true
                }
                else -> false
            }
        }
    }

    private fun editBeneficiary() {
        if (beneficiaryDetail?.beneficiary != null) {
            val direction =
                BeneficiaryDetailFragmentDirections.actionDetailToEdit(beneficiaryDetail!!.beneficiary)
            findNavController(this).navigate(direction)
        }
    }

    private fun deleteBeneficiary() {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.delete_beneficiary_title))
                .setMessage(
                    getString(
                        R.string.delete_beneficiary_message,
                        tv_beneficiary_name.text
                    )
                )
                .setPositiveButton(getString(R.string.delete)) { _, _ ->
                    beneficiaryDetailViewModel.deleteBeneficiaryById(
                        args.beneficiaryId
                    )
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun addBeneficiaryToFavorite() {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(R.string.add_to_favorite_title))
                .setMessage(
                    resources.getString(
                        R.string.add_to_favorite_message,
                        tv_beneficiary_name.text
                    )
                )
                .setPositiveButton(getString(R.string.add)) { _, _ ->
                    beneficiaryDetailViewModel.setFavorite(
                        args.beneficiaryId,
                        true
                    )
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun removeBeneficiaryFromFavorite() {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(R.string.remove_from_favorite_title))
                .setMessage(
                    resources.getString(
                        R.string.remove_from_favorite_message,
                        tv_beneficiary_name.text
                    )
                )
                .setPositiveButton(getString(R.string.accept)) { _, _ ->
                    beneficiaryDetailViewModel.setFavorite(
                        args.beneficiaryId,
                        false
                    )
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun setupClickListeners() {
        btn_contribute.setOnClickListener { contributeToBeneficiary() }
        btn_show_activity.setOnClickListener { showBeneficiaryActivities() }
        btn_show_contributions.setOnClickListener { showBeneficiaryContributions() }
    }

    private fun showBeneficiaryContributions() {
        btn_show_contributions.isActivated = true
        btn_show_activity.isActivated = false
        if (beneficiaryContributions.isEmpty()) setEmptyBeneficiaryContributions() else setBeneficiaryContributions()
    }

    private fun showBeneficiaryActivities() {
        btn_show_contributions.isActivated = false
        btn_show_activity.isActivated = true
        if (beneficiaryActivities.isEmpty()) setEmptyBeneficiaryActivities() else setBeneficiaryActivities()
    }

    private fun setBeneficiaryContributions() {
        tv_no_contributions.isVisible = false
        ll_list.isInvisible = false
    }

    private fun setBeneficiaryActivities() {
        tv_no_contributions.isVisible = false
        ll_list.isInvisible = false
    }

    private fun setEmptyBeneficiaryContributions() {
        tv_no_contributions.isVisible = true
        ll_list.isInvisible = true
        tv_no_contributions.text = resources.getString(R.string.have_no_contributions)
    }

    private fun setEmptyBeneficiaryActivities() {
        tv_no_contributions.isVisible = true
        ll_list.isInvisible = true
        tv_no_contributions.text = resources.getString(R.string.have_no_activities)
    }

    private fun updateToolbarIconFavorite(favorite: Boolean) {
        val drawable: Drawable =
            if (favorite) resources.getDrawable(R.drawable.toolbar_icon_favorite_on, null)
            else resources.getDrawable(R.drawable.toolbar_icon_favorite_off, null)
        val item: MenuItem? = beneficiary_top_bar.menu.findItem(R.id.action_set_favorite)
        item?.icon = drawable
        activity?.invalidateOptionsMenu()
    }

    private fun setBeneficiaryInfo() {
        beneficiaryDetail?.beneficiary?.let {
            updateToolbarIconFavorite(it.isFavorite)
            tv_beneficiary_name.text = it.fullname
            if (it.lastBenefactors.isNotEmpty()) {
                setLastBenefactors(it.lastBenefactors)
            }
            if (it.photo.isNotEmpty() && isValidUrl(it.photo)) {
                iv_beneficiary_image.loadWithRoundTransform(it.photo, R.drawable.ic_avatar_default_xl)
            }
            beneficiaryDetailViewModel.getBeneficiaryDonations(it.rut.orEmpty())
        }
    }

    private fun setLastBenefactors(lastBenefactors: List<LastBenefactor>) {
        when (lastBenefactors.size) {
            1 -> setOneLastBenefactor(lastBenefactors)
            2 -> setTwoLastBenefactors(lastBenefactors)
            else -> setThreeLastBenefactors(lastBenefactors)
        }
    }

    private fun setOneLastBenefactor(lastBenefactors: List<LastBenefactor>) {
        lastBenefactors.let {
            iv_contributor_one.isVisible = true
            val photoUrl = it[0].photo.orEmpty()
            val beneficiaryOne = it[0].fullname.orEmpty()
            if (photoUrl.isNotEmpty()) iv_contributor_one.loadWithCircleCrop(photoUrl, R.drawable.ic_default_photo)
            tv_contributors.text = getSpannedText(resources.getString(R.string.one_contributor,
                beneficiaryOne,
                beneficiaryDetail?.beneficiary?.firstname))
        }
    }

    private fun setTwoLastBenefactors(lastBenefactors: List<LastBenefactor>) {
        lastBenefactors.let {
            iv_contributor_one.isVisible = true
            iv_contributor_two.isVisible = true
            val photoUrlOne = it[0].photo.orEmpty()
            val photoUrlTwo = it[1].photo.orEmpty()
            val beneficiaryOne = it[0].fullname.orEmpty()
            val beneficiaryTwo = it[1].fullname.orEmpty()
            if (photoUrlOne.isNotEmpty()) iv_contributor_one.loadWithCircleCrop(photoUrlOne, R.drawable.ic_default_photo)
            if (photoUrlTwo.isNotEmpty()) iv_contributor_two.loadWithCircleCrop(photoUrlTwo, R.drawable.ic_default_photo)
            tv_contributors.text = getSpannedText(resources.getString(R.string.two_contributors,
                beneficiaryOne,
                beneficiaryTwo,
                beneficiaryDetail?.beneficiary?.firstname))
        }
    }

    private fun setThreeLastBenefactors(lastBenefactors: List<LastBenefactor>) {
        lastBenefactors.let {
            iv_contributor_one.isVisible = true
            iv_contributor_two.isVisible = true
            iv_contributor_three.isVisible = true
            val photoUrlOne = it[0].photo.orEmpty()
            val photoUrlTwo = it[1].photo.orEmpty()
            val photoUrlThree = it[2].photo.orEmpty()
            val beneficiaryOne = it[0].fullname.orEmpty()
            val beneficiaryTwo = it[1].fullname.orEmpty()
            val beneficiaryThree = it[2].fullname.orEmpty()
            if (photoUrlOne.isNotEmpty()) iv_contributor_one.loadWithCircleCrop(photoUrlOne, R.drawable.ic_default_photo)
            if (photoUrlTwo.isNotEmpty()) iv_contributor_two.loadWithCircleCrop(photoUrlTwo, R.drawable.ic_default_photo)
            if (photoUrlThree.isNotEmpty()) iv_contributor_three.loadWithCircleCrop(photoUrlThree, R.drawable.ic_default_photo)
            tv_contributors.text = getSpannedText(resources.getString(R.string.three_contributors,
                beneficiaryOne,
                beneficiaryTwo,
                beneficiaryThree,
                beneficiaryDetail?.beneficiary?.firstname))
        }
    }

    private fun setContributorsList(donations: List<Donation>) {
        val timeAgo =
            getTimeAgo(
                beneficiaryDetail?.beneficiary?.lastDonationDate,
                resources.getStringArray(R.array.time_ago_array)
            )
        tv_last_donation_date.text =
            resources.getString(R.string.last_donation_date, timeAgo)
        beneficiaryContributions.clear()
        beneficiaryContributions.addAll(donations)
        listBeneficiaryDetails.adapter = DonationsAdapter(donations)
        btn_show_contributions.performClick()
    }

    private fun setEmptyContributorsList() {
        tv_last_donation_date.text =
            resources.getString(R.string.contributions_not_registered)
        if (args.userImage.isNotEmpty()) iv_contributor_one.loadWithCircleCrop(args.userImage, R.drawable.ic_avatar_default_xl)
        iv_contributor_one.isVisible = true
        iv_contributor_two.isVisible = false
        iv_contributor_three.isVisible = false
        tv_contributors.text =
            resources.getString(R.string.be_the_first, beneficiaryDetail?.beneficiary?.firstname)
    }

    private fun contributeToBeneficiary() {
        val name = beneficiaryDetail?.beneficiary?.fullname.orEmpty()
        val id = beneficiaryDetail?.beneficiary?.id.orEmpty()
        val image = beneficiaryDetail?.beneficiary?.photo.orEmpty()
        if (name.isNotEmpty() && id.isNotEmpty()) {
            startActivityForResult(
                DirectContributionActivity.getLaunchIntent(
                    activity as Context, name, image, id
                ), REQUEST_DIRECT_CONTRIBUTION
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_DIRECT_CONTRIBUTION -> beneficiaryDetailViewModel.getBeneficiaryDetails(args.beneficiaryId)
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        beneficiary_detail_loading.isVisible = isLoading
    }

    private fun deleteBeneficiaryObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
            }
            is Completable.OnComplete -> {
                activity?.setResult(Activity.RESULT_OK)
                activity?.finish()
            }
            is Completable.OnError -> {
                var errorMessage = result.throwable.message.orEmpty()
                when(result.throwable) {
                    is DeleteBeneficiaryException -> { errorMessage = resources.getString(R.string.delete_beneficiary_error) }
                }
                errorMessage.run { showErrorDialog(message = this) }
            }
            else -> {
            }
        }
    }

    private fun setFavoriteBeneficiaryObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
            }
            is Completable.OnComplete -> beneficiaryDetailViewModel.getBeneficiaryDetails(args.beneficiaryId)
            is Completable.OnError -> {
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
            else -> {
            }
        }
    }


    private fun getBeneficiariesObserver(result: Result<BeneficiaryDetail>?) {
        when (result) {
            is Result.OnLoading -> {
                beneficiary_detail_loading.isVisible = true
            }
            is Result.OnSuccess -> {
                beneficiary_detail_loading.isVisible = false
                beneficiaryDetail = result.value
                setBeneficiaryInfo()
            }
            is Result.OnError -> {
                beneficiary_detail_loading.isVisible = false
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
            else -> {
            }
        }
    }

    private fun getDonationsObserver(result: Result<List<Donation>>?) {
        when (result) {
            is Result.OnLoading -> {
            }
            is Result.OnSuccess -> {
                if (result.value.isNotEmpty()) {
                    setContributorsList(result.value)
                } else {
                    setEmptyContributorsList()
                }
            }
            is Result.OnError -> {
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
            else -> {
            }
        }
    }

    override fun getLayoutId() = R.layout.fragment_beneficiary_detail

    companion object {
        private const val REQUEST_DIRECT_CONTRIBUTION = 71
    }

}

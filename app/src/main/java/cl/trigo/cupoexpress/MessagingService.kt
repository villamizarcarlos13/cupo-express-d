package cl.trigo.cupoexpress

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.AppConstants.NOTIFICATION_DATA
import cl.trigo.cupoexpress.AppConstants.NOTIFICATION_PUSH
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

/**
 *  Receiving notification with "notification" payload from CupoExpress API, this type of notifications
 *  may be received only when app is in foreground. So for the moment is impossible to pass pending
 *  intent with action and data to app when it's in background.
 *  - [onMessageReceived] receives new notification when app is in foreground
 *
 *  - [onNewToken] receives new token from FCM services and saving it to Shared Preferences
 *
 *  - [handleMessage] passing notification via [LocalBroadcastManager] to:
 *   1) [cl.trigo.cupoexpress.ui.activity.MainActivity] - where updating new notifications counter
 *   2) [cl.trigo.cupoexpress.ui.fragment.PeopleListFragment] - updating notification icon in toolbar
 *   3) [cl.trigo.cupoexpress.ui.fragment.ProgramListFragment] - updating notification icon in toolbar
 *   4) [cl.trigo.cupoexpress.ui.fragment.ProfileFragment] - updating notification icon in toolbar
 *
 *  - [cl.trigo.cupoexpress.ui.activity.MainActivity] has responsibility to fetch FCM token from Google service
 *  and then send it to CupoExpress API. Also in [cl.trigo.cupoexpress.ui.activity.MainActivity.onActivityResult]
 *  it's listening for successful login and manage token if user logged in from one of fragments.
 *
 *  - [cl.trigo.cupoexpress.ui.fragment.ProfileFragment] has logic to initiate close session process, and delete FCM token
 *  from app Shared Preferences and CupoExpress API
 *  Feature Module "Notifications" has responsibility to update new notifications counter when getting, deleting or updating notifications
 */
class MessagingService : FirebaseMessagingService() {

    private var broadcaster: LocalBroadcastManager? = null

    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.notification != null) {
            handleMessage(remoteMessage)
        }
    }

    private fun handleMessage(remoteMessage: RemoteMessage) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            remoteMessage.notification?.let {
                val intent = Intent(NOTIFICATION_DATA)
                intent.putExtra(NOTIFICATION_PUSH, it.body)
                broadcaster?.sendBroadcast(intent)
            }
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        try {
            AppPreferences.firebaseToken = token
        } catch (exception: NumberFormatException) {
            Log.e(TAG,"Error saving new FCM token to Shared Preferences")
        }
    }

    companion object {
        private val TAG = FirebaseMessagingService::class.qualifiedName
    }

}
package cl.trigo.cupoexpress.notifications.data.model.entry


data class NotificationEntry(
    val id: String?,
    val title: String?,
    val body: String?,
    val iconUrl: String?,
    val dateCreated: Long?,
    val isRead: Boolean?,
)


package cl.trigo.cupoexpress.data.model.request

import androidx.annotation.Keep

@Keep
data class BeneficiaryRequest(
    val rut: String,
    val firstname: String,
    val lastname: String,
    val email: String,
    val phone: String,
    val photo: String?,
    val phototype: String,
    val comuna: String
)
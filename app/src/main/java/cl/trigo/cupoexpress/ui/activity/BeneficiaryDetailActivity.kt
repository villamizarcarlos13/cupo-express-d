package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation.findNavController
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.fragment.BeneficiaryDetailFragmentArgs

class BeneficiaryDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beneficiary_detail)
        makeStatusBarTransparent()
        val id: String = intent.getStringExtra(PARAM_ID).orEmpty()
        val userImage: String = intent.getStringExtra(PARAM_USER_IMAGE).orEmpty()

        findNavController(this, R.id.nav_host_beneficiary)
            .setGraph(
                R.navigation.nav_graph_beneficiary,
                BeneficiaryDetailFragmentArgs(id, userImage).toBundle()
            )
    }

    companion object {
        const val PARAM_ID = "PARAM_ID"
        const val PARAM_USER_IMAGE = "PARAM_USER_IMAGE"

        fun getLaunchIntent(context: Context, id: String, userImage: String): Intent {
            return Intent(context, BeneficiaryDetailActivity::class.java).apply {
                putExtra(PARAM_ID, id)
                putExtra(PARAM_USER_IMAGE, userImage)
            }
        }
    }
}

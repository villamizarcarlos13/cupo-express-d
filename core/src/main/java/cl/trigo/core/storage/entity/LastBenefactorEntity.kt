package cl.trigo.core.storage.entity


data class LastBenefactorEntity (
    val fullname: String,
    val createdAt: Int,
    val rut: String,
    val id: String,
    val photo: String
)

package cl.trigo.authentication.di

import cl.trigo.authentication.data.net.AuthenticationService
import cl.trigo.authentication.data.repository.local.TokenLocalRepository
import cl.trigo.authentication.data.repository.remote.LoginRemoteRepository
import cl.trigo.authentication.data.source.AuthenticationDataSource
import cl.trigo.authentication.data.source.TokenDataSource
import cl.trigo.authentication.data.source.local.TokenLocalDataSource
import cl.trigo.authentication.data.source.remote.AuthenticationRemoteDataSource
import cl.trigo.authentication.domain.repository.LoginRepository
import cl.trigo.authentication.domain.repository.TokenRepository
import cl.trigo.authentication.domain.usecase.*
import cl.trigo.authentication.ui.viewmodel.LoginRutViewModel
import cl.trigo.authentication.ui.viewmodel.LoginViewModel
import cl.trigo.authentication.ui.viewmodel.RegisterViewModel
import cl.trigo.core.util.AppPreferences
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val authenticationModule = module {

    /* API REST Remote */
    single { get<Retrofit>().create(AuthenticationService::class.java) as AuthenticationService }

    /* DataSources*/
    factory<AuthenticationDataSource> {
        AuthenticationRemoteDataSource(
            get()
        )
    }
    factory<TokenDataSource> { TokenLocalDataSource(AppPreferences) }

    /* Repositories */
    factory<TokenRepository> { TokenLocalRepository(get()) }
    factory<LoginRepository> { LoginRemoteRepository(get()) }

    /* UseCases */
    factory { GetUserByRutUseCase(get()) }
    factory {
        LoginUseCase(
            get(),
            get()
        )
    }
    factory { CloseSessionUseCase(get()) }
    factory { RegisterUserUseCase(get()) }
    factory { RestorePasswordUseCase(get()) }

    /* View Models */
    viewModel { LoginRutViewModel(get()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { RegisterViewModel(get(), get()) }
}
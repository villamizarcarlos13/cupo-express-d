package cl.trigo.cupoexpress.data.model.entry

import cl.trigo.core.extension.formatAmount
import cl.trigo.cupoexpress.domain.model.Benefactor
import cl.trigo.cupoexpress.domain.model.Benefit
import cl.trigo.cupoexpress.domain.model.Donation

data class DonationEntry(
    val benefactor: BenefactorEntry?,
    val createAt: String?,
    val benefit: BenefitEntry,
    val amount: Int?,
    val likesQuantity: Int?
)

fun DonationEntry.toDonation() = Donation(
    benefactor = Benefactor(
        benefactor?.id.orEmpty(),
        benefactor?.firstName.orEmpty(),
        benefactor?.lastName.orEmpty(),
        benefactor?.profileUrl.orEmpty(),
        "${benefactor?.firstName.orEmpty()} ${benefactor?.lastName.orEmpty()}",
        benefactor?.region.orEmpty(),
        benefactor?.country.orEmpty(),
        benefactor?.email.orEmpty(),
        benefactor?.rut.orEmpty(),
        benefactor?.phone.orEmpty()
    ),
    createAt = createAt.orEmpty(),
    benefit = Benefit(
        benefit.name.orEmpty(),
        benefit.description.orEmpty(),
        benefit.price.orEmpty(),
        benefit.shortName.orEmpty()
    ),
    amount = formatAmount(amount?.or(0)),
    likesQuantity = likesQuantity ?: 0
)
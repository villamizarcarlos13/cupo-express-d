package cl.trigo.payment.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.getSpannedText
import cl.trigo.core.extension.loadWithCircleCrop
import cl.trigo.core.extension.observe
import cl.trigo.payment.R
import cl.trigo.payment.domain.model.Benefit
import cl.trigo.payment.ui.fragment.adapter.BenefitsAdapter
import cl.trigo.payment.ui.fragment.listener.OnClickBenefitListener
import cl.trigo.payment.ui.viewmodel.GetBenefitsViewModel
import kotlinx.android.synthetic.main.fragment_select_benefit.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SelectBenefitFragment : BaseFragment(), OnClickBenefitListener {

    private val viewModel by viewModel<GetBenefitsViewModel>()
    private val args: SelectBenefitFragmentArgs by navArgs()
    private var selectedBenefit: Benefit? = null
    var amount: Int = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(getBenefitsLiveData, ::getBenefitsObserver)
            getBenefits()
        }
        list_benefits.setHasFixedSize(false)
        list_benefits.layoutManager = GridLayoutManager(context, 2)
        initView()
    }

    private fun initView() {
        select_benefit_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        tv_beneficiary_name.text =
            getSpannedText(getString(R.string.direct_contribution_title, args.beneficiaryName))
        iv_beneficiary_image?.loadWithCircleCrop(args.beneficiaryImage, R.drawable.ic_default_photo)
        btn_continue.setOnClickListener {
            if (selectedBenefit != null) {
                val direction =
                    SelectBenefitFragmentDirections.actionAmountToPaymentMethod(
                        args.beneficiaryName,
                        args.beneficiaryImage,
                        args.beneficiaryId,
                        selectedBenefit!!
                    )
                NavHostFragment.findNavController(this).navigate(direction)
            }
        }
    }

    override fun onClickItem(benefit: Benefit) {
        selectedBenefit = benefit
    }

    private fun showLoading(isLoading: Boolean) {
        select_benefit_loading.isVisible = isLoading
    }

    private fun getBenefitsObserver(result: Result<List<Benefit>>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
            }
            is Result.OnSuccess -> {
                showLoading(false)
                list_benefits.adapter = BenefitsAdapter(result.value, this)
            }
            is Result.OnError -> {
                showLoading(false)
                val errorMessage = result.throwable.message!!
                errorMessage.run { showErrorDialog(message = this) }
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_select_benefit
}
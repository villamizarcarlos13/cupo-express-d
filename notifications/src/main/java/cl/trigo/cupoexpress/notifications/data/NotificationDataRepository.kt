package cl.trigo.cupoexpress.notifications.data

import cl.trigo.cupoexpress.notifications.data.local.NotificationLocal
import cl.trigo.cupoexpress.notifications.data.mapper.NotificationDataMapper
import cl.trigo.core.storage.entity.NotificationEntity
import cl.trigo.cupoexpress.notifications.data.model.entry.NotificationEntry
import cl.trigo.cupoexpress.notifications.data.remote.NotificationRemote
import cl.trigo.cupoexpress.notifications.domain.model.NotificationModel
import cl.trigo.cupoexpress.notifications.domain.repository.NotificationRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class NotificationDataRepository(
    private val remoteDataSource: NotificationRemote,
    private val localDataSource: NotificationLocal,
    private val dataMapper: NotificationDataMapper
) : NotificationRepository {
    override suspend fun markAsReadNotification(id: String) {
        runCatching { remoteDataSource.markAsReadNotification(id) }
            .onSuccess { localDataSource.markAsReadNotification(id) }
    }
    override suspend fun deleteNotification(id: String) {
        runCatching { remoteDataSource.deleteNotification(id) }
            .onSuccess { localDataSource.deleteNotification(id) }

    }
    override suspend fun getAllNotifications(forceUpdate: Boolean): Flow<List<NotificationModel>> = flow {
        val localNotifications = localDataSource.getAllNotifications()
        emit(with(dataMapper) { localNotifications.map { it.toNotificationModel() } })
        updateNewNotificationsCount(localNotifications.filter { !(it.isRead) }.size)
        if (forceUpdate) {
            val remoteNotifications = remoteDataSource.getAllNotifications()
            emit(with(dataMapper) { remoteNotifications.data.map { it.toNotificationModel() } })
            replaceAllNotificationsInDb(remoteNotifications.data)
        }
    }

    private suspend fun replaceAllNotificationsInDb(notifications: List<NotificationEntry>) {
        localDataSource.deleteAllNotifications()
        insertNotificationsToDb(with(dataMapper) { notifications.map { it.toNotificationEntity() } })
        updateNewNotificationsCount(notifications.filter { !(it.isRead?:false) }.size)
    }

    private suspend fun insertNotificationsToDb(notifications: List<NotificationEntity>) {
        localDataSource.insertNotificationsList(notifications)
    }

    private suspend fun updateNewNotificationsCount(count : Int) {
        localDataSource.updateNotificationCount(count)
    }
}
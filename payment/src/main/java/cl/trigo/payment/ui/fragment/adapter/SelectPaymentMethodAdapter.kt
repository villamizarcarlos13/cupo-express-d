package cl.trigo.payment.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.payment.R
import cl.trigo.payment.ui.fragment.listener.OnClickPaymentMethod
import cl.trigo.payment.domain.model.PaymentMethod
import kotlinx.android.synthetic.main.item_select_payment_method.view.*
import java.util.*

class SelectPaymentMethodAdapter(
    private val list: List<PaymentMethod>,
    private val onClickListener: OnClickPaymentMethod
) : RecyclerView.Adapter<SelectPaymentMethodAdapter.ViewHolder>() {

    private var selectedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_select_payment_method, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.cardNumber.text = item.lastCardDigits
        if (item.creditCardType.toLowerCase(Locale.ROOT).contains("visa")) {
            holder.iconCard.setImageDrawable(
                holder.iconCard.context.resources.getDrawable(
                    R.drawable.ic_visa,
                    null
                )
            )
        } else {
            holder.iconCard.setImageDrawable(
                holder.iconCard.context.resources.getDrawable(
                    R.drawable.ic_mastercard,
                    null
                )
            )
        }

        holder.view.isSelected = position == selectedPosition
        holder.radioButton.isChecked = position == selectedPosition

        with(holder.view) {
            tag = item
            setOnClickListener {
                onClickListener.onClickItem(list[position])
                selectedPosition = position
                notifyDataSetChanged()
                return@setOnClickListener
            }
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val cardNumber: TextView = view.tv_card_number
        val iconCard: ImageView = view.iv_card
        val radioButton: RadioButton = view.rb_selected
    }
}
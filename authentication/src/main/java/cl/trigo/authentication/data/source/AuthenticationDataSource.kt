package cl.trigo.authentication.data.source

import cl.trigo.core.base.Response
import cl.trigo.authentication.data.model.entry.LoginEntry
import cl.trigo.authentication.data.model.entry.RegisterEntry
import cl.trigo.authentication.data.model.entry.RutEntry
import cl.trigo.core.base.ResponseCompletable

interface AuthenticationDataSource {
    suspend fun signUp(
        firstName: String,
        lastName: String,
        rut: String,
        email: String,
        password: String
    ): Response<RegisterEntry>

    suspend fun signIn(email: String, password: String): Response<LoginEntry>

    suspend fun verifyUserByRut(rut: String): Response<RutEntry>

    suspend fun restorePassword(email: String): ResponseCompletable
}
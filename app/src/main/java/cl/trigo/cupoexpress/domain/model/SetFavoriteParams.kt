package cl.trigo.cupoexpress.domain.model

data class SetFavoriteParams(
    val id: String,
    val isFavorite: Boolean
)

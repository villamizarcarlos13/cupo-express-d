package cl.trigo.payment.ui.fragment

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.common.RegisterBottomSheetFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.extension.*
import cl.trigo.payment.R
import cl.trigo.payment.ui.viewmodel.ConfirmPaymentViewModel
import kotlinx.android.synthetic.main.fragment_confirm_payment.*
import kotlinx.android.synthetic.main.fragment_confirm_payment.iv_beneficiary_image
import kotlinx.android.synthetic.main.fragment_confirm_payment.tv_beneficiary_name
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class ConfirmPaymentFragment : BaseFragment() {

    private val args: ConfirmPaymentFragmentArgs by navArgs()
    private val viewModel by viewModel<ConfirmPaymentViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            observe(confirmPaymentLiveData, ::confirmPaymentObserver)
        }
        initView()
    }

    private fun initView() {
        confirm_payment_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        tv_beneficiary_name.text =
            getSpannedText(getString(R.string.direct_contribution_title, args.beneficiaryName))
        iv_beneficiary_image?.loadWithCircleCrop(args.beneficiaryImage, R.drawable.ic_default_photo)
        tv_contribution_amount.text = formatAmount(args.benefit.price)
        tv_frequency_title.text = resources.getString(R.string.frequency_none_title)
        tv_frequency_message.text = resources.getString(R.string.frequency_none)

        if (args.paymentMethod.creditCardType.toLowerCase(Locale.ROOT).contains("visa")) {
            iv_card_icon.setImageDrawable(resources.getDrawable(R.drawable.ic_visa, null))
        } else {
            iv_card_icon.setImageDrawable(resources.getDrawable(R.drawable.ic_mastercard, null))
        }
        tv_card_digits.text = args.paymentMethod.lastCardDigits
        btn_confirm_payment.setOnClickListener {
            viewModel.confirmPayment(
                args.benefit.id,
                args.beneficiaryId,
                "1",
                args.paymentMethod.id
            )
        }
    }

    private fun setupViewOnSuccess() {
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.thanks_for_contribution),
                resources.getString(
                    R.string.thanks_for_contribution_message,
                    args.beneficiaryName
                ),
                resources.getString(R.string.proceed)
            )
        bottomSheet.liveData().observe(viewLifecycleOwner, Observer {
            handleBottomSheetResult(it)
        })
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun setupViewOnError() {
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.error),
                resources.getString(R.string.contribution_error),
                resources.getString(R.string.proceed)
            )
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun handleBottomSheetResult(isFinished: Boolean) {
        if (isFinished) {
            activity.let {
                it?.setResult(Activity.RESULT_OK)
                it?.finish()
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        confirm_payment_loading.isVisible = isLoading
    }

    private fun confirmPaymentObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
                showLoading(true)
                btn_confirm_payment.isEnabled = false
            }
            is Completable.OnComplete -> {
                showLoading(false)
                btn_confirm_payment.isEnabled = true
                setupViewOnSuccess()
            }
            is Completable.OnError -> {
                showLoading(false)
                btn_confirm_payment.isEnabled = true
                setupViewOnError()
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_confirm_payment
}
package cl.trigo.payment.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.payment.R
import cl.trigo.payment.ui.fragment.SelectBenefitFragmentArgs

class DirectContributionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_direct_contribution)
        makeStatusBarTransparent()
        val beneficiaryName: String? = intent.getStringExtra(PARAM_BENEFICIARY_NAME)
        val beneficiaryImage: String? = intent.getStringExtra(PARAM_BENEFICIARY_IMAGE)
        val beneficiaryId: String? = intent.getStringExtra(PARAM_BENEFICIARY_ID)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_beneficiary_contribution) as NavHostFragment
        navHostFragment.navController.setGraph(
            R.navigation.nav_graph_direct_contribution,
            SelectBenefitFragmentArgs(
                beneficiaryName.orEmpty(),
                beneficiaryImage.orEmpty(),
                beneficiaryId.orEmpty()
            ).toBundle()
        )
    }

    companion object {

        const val PARAM_BENEFICIARY_NAME = "PARAM_BENEFICIARY_NAME"
        const val PARAM_BENEFICIARY_IMAGE = "PARAM_BENEFICIARY_IMAGE"
        const val PARAM_BENEFICIARY_ID = "PARAM_BENEFICIARY_ID"

        fun getLaunchIntent(context: Context, name: String, image: String, id: String): Intent {
            return Intent(context, DirectContributionActivity::class.java).apply {
                putExtra(PARAM_BENEFICIARY_NAME, name)
                putExtra(PARAM_BENEFICIARY_IMAGE, image)
                putExtra(PARAM_BENEFICIARY_ID, id)
            }
        }
    }
}
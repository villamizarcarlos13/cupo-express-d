package cl.trigo.cupoexpress.data.remote

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.AppConstants.PLATFORM_NAME
import cl.trigo.cupoexpress.data.model.entry.*
import cl.trigo.cupoexpress.data.model.request.*
import cl.trigo.cupoexpress.data.net.BeneficiaryService
import cl.trigo.cupoexpress.domain.model.Kiosk
import retrofit2.await

open class BeneficiaryRemoteDataSource(
    private val appPreferences: AppPreferences,
    private val beneficiaryService: BeneficiaryService
) : BeneficiaryRemote {
    override suspend fun getBeneficiaries(): Response<List<BeneficiaryEntry>> =
        beneficiaryService.getBeneficiaries(appPreferences.authToken!!).await()

    override suspend fun getCommunes(): Response<List<CommuneEntry>> =
        beneficiaryService.getCommunesList().await()


    override suspend fun deleteBeneficiaryById(id: String): ResponseCompletable =
        beneficiaryService.deleteBeneficiaryById(appPreferences.authToken!!, id).await()

    override suspend fun editBeneficiary(
        id: String,
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        commune: String?,
        photo: String?
    ): ResponseCompletable {
        return beneficiaryService.editBeneficiary(
            appPreferences.authToken!!,
            id,
            EditBeneficiaryRequest(firstName, lastName, email, phone, photo,"png", commune)
        ).await()
    }

    override suspend fun editUser(
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        photo: String?
    ): Response<UserEditResponseEntry> {
        return beneficiaryService.editUser(
            appPreferences.authToken!!,
            EditUserRequest(firstName, lastName, email, phone, photo, "png")
        ).await()
    }

    override suspend fun getUserData(): Response<UserDataResponseEntry> =
        beneficiaryService.getUserData(appPreferences.authToken!!).await()

    override suspend fun checkVersion(): Response<CheckVersionEntry> =
        beneficiaryService.checkVersion(CheckVersionRequest(PLATFORM_NAME)).await()


    override suspend fun addBeneficiary(
        rut: String,
        firstName: String,
        lastName: String,
        email: String,
        phone: String,
        photo: String,
        commune: String
    ): ResponseCompletable {
        val beneficiaryRequest = BeneficiaryRequest(
            rut = rut,
            firstname = firstName,
            lastname = lastName,
            email = email,
            phone = phone,
            photo = photo,
            phototype = "png",
            comuna = commune
        )
        return beneficiaryService.addBeneficiary(appPreferences.authToken!!, beneficiaryRequest)
            .await()
    }

    override suspend fun getBeneficiaryDetail(rut: String): Response<BeneficiaryDetailEntry> =
        beneficiaryService.getBeneficiaryDetail(appPreferences.authToken!!, rut).await()

    override suspend fun getBeneficiaryDonationsById(rut: String): Response<List<DonationEntry>> =
        beneficiaryService.getBeneficiaryDonationsById(appPreferences.authToken!!, rut).await()

    override suspend fun setBeneficiaryFavorite(
        id: String,
        isFavorite: Boolean
    ): ResponseCompletable = beneficiaryService.setBeneficiaryFavorite(
        appPreferences.authToken!!,
        id,
        SetFavoriteRequest(isFavorite)
    ).await()

    override suspend fun saveFirebaseMessagingServiceToken(token: String): ResponseCompletable {
            appPreferences.firebaseToken = token
            return beneficiaryService.saveFirebaseMessagingServiceToken(appPreferences.authToken!!, token)
                .await()
    }

    override suspend fun deleteFirebaseMessagingServiceToken(token: String): ResponseCompletable =
        beneficiaryService.deleteFirebaseMessagingServiceToken(appPreferences.authToken!!)
            .await()

    override suspend fun getContributions(): Response<List<ContributionEntry>> =
        beneficiaryService.getContributions(appPreferences.authToken!!).await()

    override suspend fun getPrograms(): Response<List<ProgramEntry>> =
        beneficiaryService.getPrograms(appPreferences.authToken!!).await()

    override suspend fun getKiosks(): Response<List<Kiosk>> {
        TODO("Not yet implemented")
    }

}
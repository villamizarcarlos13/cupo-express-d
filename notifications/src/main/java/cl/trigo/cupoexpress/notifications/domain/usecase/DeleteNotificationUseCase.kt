package cl.trigo.cupoexpress.notifications.domain.usecase

import cl.trigo.cupoexpress.notifications.domain.repository.NotificationRepository

class DeleteNotificationUseCase (private val repository: NotificationRepository) {
    suspend fun deleteNotification(id: String) = repository.deleteNotification(id)
}
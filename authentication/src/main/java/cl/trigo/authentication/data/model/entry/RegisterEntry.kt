package cl.trigo.authentication.data.model.entry

import androidx.annotation.Keep

@Keep
data class RegisterEntry(val uid: String?)
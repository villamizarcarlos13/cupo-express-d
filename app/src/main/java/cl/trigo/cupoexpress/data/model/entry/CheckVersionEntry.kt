package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep
import cl.trigo.cupoexpress.domain.model.CheckVersion

@Keep
data class CheckVersionEntry(
    val version: String?,
    val minimal: String?,
    val mustUpdate: Boolean?
)

fun CheckVersionEntry.toCheckVersion(): CheckVersion {
    return CheckVersion(
        version = version.orEmpty(),
        minimal = minimal.orEmpty(),
        mustUpdate = mustUpdate
    )
}

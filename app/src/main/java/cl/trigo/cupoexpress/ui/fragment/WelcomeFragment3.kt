package cl.trigo.cupoexpress.ui.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.R
import kotlinx.android.synthetic.main.fragment_welcome_step3.*

class WelcomeFragment3 : Fragment() {

    private val SHOW_WELCOME = 0
    private val HIDE_WELCOME = 1


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome_step3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chbox_not_show_again.setOnClickListener{
            val checked: Boolean = (it as CheckBox).isChecked
            val sharedPref = activity?.
            getSharedPreferences(AppPreferences.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
            with(sharedPref?.edit()){
                if (checked){
                    this?.putInt(getString(R.string.welcome_checkbox), HIDE_WELCOME)
                }else{
                    this?.putInt(getString(R.string.welcome_checkbox), SHOW_WELCOME)
                }
                this?.apply()
            }

        }
    }




}
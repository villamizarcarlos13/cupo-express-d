package cl.trigo.core.storage.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import cl.trigo.core.storage.db.DbConstants.TABLE_NOTIFICATIONS

@Entity(tableName = TABLE_NOTIFICATIONS)
data class NotificationEntity(
    @PrimaryKey val id: String,
    val title: String,
    val body: String,
    val iconUrl: String,
    val dateCreated: Long,
    val isRead: Boolean
)
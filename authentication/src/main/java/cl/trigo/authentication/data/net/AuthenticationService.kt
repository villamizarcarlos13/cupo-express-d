package cl.trigo.authentication.data.net

import cl.trigo.core.base.Response
import cl.trigo.authentication.data.model.entry.LoginEntry
import cl.trigo.authentication.data.model.entry.RegisterEntry
import cl.trigo.authentication.data.model.entry.RutEntry
import cl.trigo.authentication.data.model.request.LoginRequest
import cl.trigo.authentication.data.model.request.RegisterRequest
import cl.trigo.authentication.data.model.request.RestorePassParams
import cl.trigo.core.base.ResponseCompletable
import retrofit2.Call
import retrofit2.http.*

interface AuthenticationService {

    @Headers("Accept: application/json")
    @POST("users")
    fun signUp(@Body registerRequest: RegisterRequest): Call<Response<RegisterEntry>>

    @Headers("Accept: application/json")
    @POST("users/login")
    fun signIn(@Body loginRequest: LoginRequest): Call<Response<LoginEntry>>

    @Headers("Accept: application/json")
    @POST("users/password")
    fun restorePassword(@Body param: RestorePassParams): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @GET("users/{rut}")
    fun verifyUserByRut(@Path("rut") rut: String): Call<Response<RutEntry>>
}
package cl.trigo.cupoexpress.notifications.data.mapper

import cl.trigo.core.storage.entity.NotificationEntity
import cl.trigo.cupoexpress.notifications.data.model.entry.NotificationEntry
import cl.trigo.cupoexpress.notifications.domain.model.NotificationModel

class NotificationDataMapper {

    fun NotificationEntry.toNotificationModel() = NotificationModel (
        id = id.orEmpty(),
        title = title.orEmpty(),
        body = body.orEmpty(),
        iconUrl = iconUrl.orEmpty(),
        dateCreated = dateCreated?: 0L,
        isRead = isRead?: false
    )

    fun NotificationEntity.toNotificationModel() = NotificationModel (
        id = id.orEmpty(),
        title = title.orEmpty(),
        body = body.orEmpty(),
        iconUrl = iconUrl.orEmpty(),
        dateCreated = dateCreated?: 0L,
        isRead = isRead?: false
    )

    fun NotificationEntry.toNotificationEntity() = NotificationEntity (
        id = id.orEmpty(),
        title = title.orEmpty(),
        body = body.orEmpty(),
        iconUrl = iconUrl.orEmpty(),
        dateCreated = dateCreated?: 0L,
        isRead = isRead?: false
    )
}

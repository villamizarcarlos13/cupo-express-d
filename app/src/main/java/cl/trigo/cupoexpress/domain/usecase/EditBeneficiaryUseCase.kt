package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.CompletableUseCase
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class EditBeneficiaryUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : CompletableUseCase<Map<String, String?>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    companion object {
        const val ID_PARAM = "ID_PARAM"
        const val COMMUNE_PARAM = "COMMUNE_PARAM"
        const val FIRST_NAME_PARAM = "FIRST_NAME_PARAM"
        const val LAST_NAME_PARAM = "LAST_NAME_PARAM"
        const val EMAIL_PARAM = "EMAIL_PARAM"
        const val PHONE_PARAM = "PHONE_PARAM"
        const val PHOTO_PARAM = "PHOTO_PARAM"
    }

    override suspend fun executeOnBackground(params: Map<String, String?>) {
        val id = params.getValue(ID_PARAM)
        val commune = params.getValue(COMMUNE_PARAM)
        val firstName = params.getValue(FIRST_NAME_PARAM)
        val lastName = params.getValue(LAST_NAME_PARAM)
        val email = params.getValue(EMAIL_PARAM)
        val phone = params.getValue(PHONE_PARAM)
        val photo = params.getValue(PHOTO_PARAM)

        if (id != null) {
            beneficiaryRepository.editBeneficiary(id, firstName, lastName, email, phone, photo, commune)
        }
    }
}
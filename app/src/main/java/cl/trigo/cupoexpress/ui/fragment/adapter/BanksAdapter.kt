package cl.trigo.cupoexpress.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import cl.trigo.cupoexpress.R
import kotlinx.android.synthetic.main.item_brand.view.*

class BanksAdapter : RecyclerView.Adapter<BanksAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_brand, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.image.setImageDrawable(holder.image.context?.let {
            ContextCompat.getDrawable(it, banksIds[position])
        })
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.iv_brand
    }

    override fun getItemCount(): Int = banksIds.size

    private var banksIds = arrayOf(
        R.drawable.logo_santander
    )
}
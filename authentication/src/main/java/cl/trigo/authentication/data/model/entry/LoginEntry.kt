package cl.trigo.authentication.data.model.entry

import androidx.annotation.Keep
import cl.trigo.authentication.domain.model.Login
import cl.trigo.authentication.domain.model.Status
@Keep
data class LoginEntry(val idToken: String, val user: UserEntry)

fun LoginEntry.toLogin(status: Status): Login {
    return Login(idToken = idToken, user = user.toUser(), status = status)
}
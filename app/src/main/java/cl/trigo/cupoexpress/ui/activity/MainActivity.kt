package cl.trigo.cupoexpress.ui.activity

import android.app.Activity
import android.content.*
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.core.extension.observe
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.AppConstants
import cl.trigo.cupoexpress.AppConstants.GOOGLE_PLAY_URL
import cl.trigo.cupoexpress.AppConstants.MARKET_URL
import cl.trigo.cupoexpress.AppConstants.REQUEST_LOGIN_FROM_PEOPLE_LIST
import cl.trigo.cupoexpress.AppConstants.REQUEST_LOGIN_FROM_PROFILE
import cl.trigo.cupoexpress.BuildConfig
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.domain.model.CheckVersion
import cl.trigo.cupoexpress.ui.viewmodel.HomeViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.messaging.FirebaseMessaging
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val SHOW_WELCOME = 0
    private lateinit var bottomNavigationView: BottomNavigationView
    private val viewModel by viewModel<HomeViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        makeStatusBarTransparent()
        checkServices()
        setUpNavigation()
        if (!BuildConfig.DEBUG) {
            with(viewModel) {
                observe(checkVersionLiveData, ::checkVersionObserver)
                checkVersion()
            }
        }
        viewModel.saveToken.observe(this, { saveFcmTokenObserver(it) })
        showIndications()


    }

    override fun onStart() {
        super.onStart()
        messageReceiver?.let { LocalBroadcastManager.getInstance(this)
            .registerReceiver(it, IntentFilter(AppConstants.NOTIFICATION_DATA)) }
    }

    override fun onStop() {
        super.onStop()
        messageReceiver?.let { LocalBroadcastManager.getInstance(this).unregisterReceiver(it) }
    }

    private fun checkServices() {
        if (checkGooglePlayServices()) {
            retrieveFirebaseMessagingServiceToken()
        }
    }

    private fun setUpNavigation() {
        bottomNavigationView = findViewById(R.id.bottom_navigation)
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

        NavigationUI.setupWithNavController(
            bottomNavigationView,
            navHostFragment!!.navController
        )
    }

    private fun showIndications(){
        val sharedPref = getSharedPreferences(AppPreferences.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE) ?: return
        val show_hide_flag = sharedPref.getInt(getString(R.string.welcome_checkbox), SHOW_WELCOME)
        if (show_hide_flag == SHOW_WELCOME){
            startActivity(Intent(this, WelcomeActivity::class.java))
        }
    }

    private fun retrieveFirebaseMessagingServiceToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) { return@OnCompleteListener }
            val token = task.result
            viewModel.saveFcmToken(token)
        })
    }

    private val messageReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            viewModel.updateNotification()
        }
    }

    private fun checkGooglePlayServices(): Boolean {
        val status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        return status == ConnectionResult.SUCCESS
    }

    private fun openGooglePlay() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL + packageName)))
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_URL + packageName)))
        }
    }

    private fun showCheckVersionError() {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.error))
            .setMessage(getString(R.string.check_version_error_message))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.try_again)) { _, _ ->
                viewModel.checkVersion()
            }
            .show()
    }

    private fun showMustUpdate(versionName: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.show_update_title))
            .setMessage(getString(R.string.show_update_message, versionName))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.update)) { _, _ ->
                openGooglePlay()
            }
            .show()
    }

    private fun checkVersionObserver(result: Result<CheckVersion>?) {
        when (result) {
            is Result.OnLoading -> {
            }
            is Result.OnSuccess -> {
                val versionName: String = BuildConfig.VERSION_NAME
                result.value.apply {
                    if (mustUpdate == true || version != versionName) {
                        showMustUpdate(version)
                    }
                }

            }
            is Result.OnError -> {
                showCheckVersionError()
            }
            else -> {
            }
        }
    }

    private fun saveFcmTokenObserver(result: Completable) {
        when (result) {
            is Completable.OnComplete -> { }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val unmaskedRequestCode = requestCode and 0x0000ffff
            if (unmaskedRequestCode == REQUEST_LOGIN_FROM_PROFILE || unmaskedRequestCode == REQUEST_LOGIN_FROM_PEOPLE_LIST) {
                checkServices()
            }
        }
    }
}
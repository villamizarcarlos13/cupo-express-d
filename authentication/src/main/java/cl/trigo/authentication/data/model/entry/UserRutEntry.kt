package cl.trigo.authentication.data.model.entry

import androidx.annotation.Keep
import cl.trigo.authentication.domain.model.UserRut
@Keep
data class UserRutEntry(
    val firstname: String?,
    val lastname: String?,
    val email: String?,
    val active: Boolean
)

fun UserRutEntry.toUserRut() = UserRut(
    firstname = firstname,
    lastname = lastname,
    email = email,
    active = active
)
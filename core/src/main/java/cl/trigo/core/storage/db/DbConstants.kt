package cl.trigo.core.storage.db

object DbConstants {
    const val TABLE_NOTIFICATIONS = "notifications"
    const val DATABASE_NAME = "cupo-express.db"
    const val DATABASE_VERSION = 1
    const val TABLE_BENEFICIARIES = "beneficiaries"
    const val TABLE_PAYMENT_METHODS = "payment_methods"
}
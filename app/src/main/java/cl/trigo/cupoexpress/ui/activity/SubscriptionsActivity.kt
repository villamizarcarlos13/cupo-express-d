package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.fragment.SubscriptionsFragment
import kotlinx.android.synthetic.main.activity_subscriptions.*

class SubscriptionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscriptions)
        makeStatusBarTransparent()
        subscriptions_top_bar.setNavigationOnClickListener { this.onBackPressed() }
        supportFragmentManager.beginTransaction()
            .add(R.id.subscriptions_fragment_container, SubscriptionsFragment())
            .commit()
    }

    companion object {
        fun getLaunchIntent(context: Context): Intent =
            Intent(context, SubscriptionsActivity::class.java)
    }

}
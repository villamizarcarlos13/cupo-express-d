package cl.trigo.authentication.data.model.entry

import androidx.annotation.Keep
import cl.trigo.authentication.domain.model.User
@Keep
data class UserEntry(
    val firstname: String?,
    val lastname: String?,
    val rut: String?,
    val email: String?,
    val password: String?,
    val role: String?,
    val phone: String?,
    val photo: String?,
    val country: String?,
    val city: String?,
    val region: String?,
    val address: String?,
    val active: Boolean
)

fun UserEntry.toUser() = User(
    firstname = firstname.orEmpty(),
    lastname = lastname.orEmpty(),
    rut = rut.orEmpty(),
    email = email.orEmpty(),
    password = password.orEmpty(),
    role = role.orEmpty(),
    phone = phone.orEmpty(),
    photo = photo.orEmpty(),
    country = country.orEmpty(),
    city = city.orEmpty(),
    region = region.orEmpty(),
    address = address.orEmpty(),
    active = active
)
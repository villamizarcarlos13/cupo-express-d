package cl.trigo.payment.data.remote

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.core.util.AppPreferences
import cl.trigo.payment.data.model.entry.BenefitEntry
import cl.trigo.payment.data.model.entry.PaymentMethodEntry
import cl.trigo.payment.data.model.entry.RegisterPaymentMethodEntry
import cl.trigo.payment.data.model.request.PaymentItemRequest
import cl.trigo.payment.data.model.request.PaymentRequest
import cl.trigo.payment.data.net.PaymentService
import retrofit2.await

class PaymentRemoteDataSource(
    private val appPreferences: AppPreferences,
    private val paymentService: PaymentService
) : PaymentRemote {

    override suspend fun getPaymentMethods(): Response<List<PaymentMethodEntry>> =
        paymentService.getPaymentMethods(appPreferences.authToken!!).await()

    override suspend fun registerPaymentMethod(): Response<RegisterPaymentMethodEntry> =
        paymentService.registerPaymentMethod(appPreferences.authToken!!).await()

    override suspend fun deletePaymentMethodById(id: String): ResponseCompletable =
        paymentService.deletePaymentMethodById(appPreferences.authToken!!, id).await()

    override suspend fun confirmPayment(
        benefitId: String,
        beneficiaryId: String,
        quantity: Int,
        paymentMethodId: String
    ): ResponseCompletable {
        val paymentItem = PaymentItemRequest(beneficiaryId, quantity)
        val paymentRequest = PaymentRequest(benefitId, listOf(paymentItem), paymentMethodId)
        return paymentService.confirmPayment(appPreferences.authToken!!, paymentRequest)
            .await()
    }

    override suspend fun getBenefits(): Response<List<BenefitEntry>> =
        paymentService.getBenefits().await()


}
package cl.trigo.cupoexpress.ui.fragment.listener

import cl.trigo.cupoexpress.domain.model.Commune

interface OnClickCommune {
    fun onClickItem(commune: Commune, isSelected: Boolean): Unit
}
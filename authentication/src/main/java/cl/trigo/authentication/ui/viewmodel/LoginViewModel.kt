package cl.trigo.authentication.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.authentication.domain.usecase.LoginUseCase
import cl.trigo.authentication.domain.usecase.LoginUseCase.Companion.EMAIL_PARAM
import cl.trigo.authentication.domain.usecase.LoginUseCase.Companion.PASSWORD_PARAM
import cl.trigo.authentication.domain.usecase.RestorePasswordUseCase
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.core.extension.LiveResult

class LoginViewModel(
    private val loginUseCase: LoginUseCase,
    private val restorePasswordUseCase: RestorePasswordUseCase
) : ViewModel() {

    val loginLiveData = LiveResult<cl.trigo.authentication.domain.model.User>()
    val restorePasswordLiveData = LiveCompletable()

    fun login(email: String, password: String) = loginUseCase.execute(
        loginLiveData, mapOf(
            EMAIL_PARAM to email,
            PASSWORD_PARAM to password
        )
    )

    fun restorePassword(email: String) = restorePasswordUseCase.execute(restorePasswordLiveData, email)
}
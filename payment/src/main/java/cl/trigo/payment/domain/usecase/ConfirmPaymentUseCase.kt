package cl.trigo.payment.domain.usecase

import cl.trigo.core.coroutines.CompletableUseCase
import cl.trigo.payment.domain.repository.PaymentRepository
import kotlinx.coroutines.Dispatchers

class ConfirmPaymentUseCase(
    private val paymentRepository: PaymentRepository
) : CompletableUseCase<Map<String, String?>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    companion object {
        const val BENEFIT_ID_PARAM = "BENEFIT_ID_PARAM"
        const val BENEFICIARY_ID_PARAM = "BENEFICIARY_ID_PARAM"
        const val QUANTITY_PARAM = "QUANTITY_PARAM"
        const val PAYMENT_METHOD_PARAM = "PAYMENT_METHOD_PARAM"
    }

    override suspend fun executeOnBackground(params: Map<String, String?>) {
        val benefitId = params.getValue(BENEFIT_ID_PARAM).orEmpty()
        val beneficiaryId = params.getValue(BENEFICIARY_ID_PARAM).orEmpty()
        val quantity = params.getValue(QUANTITY_PARAM).orEmpty()
        val paymentMethodId = params.getValue(PAYMENT_METHOD_PARAM).orEmpty()
        paymentRepository.confirmPayment(benefitId, beneficiaryId, quantity, paymentMethodId)
    }
}
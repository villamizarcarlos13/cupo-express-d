package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.CheckVersion
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class CheckVersionUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUnitUseCase<CheckVersion>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): CheckVersion? = beneficiaryRepository.checkVersion()
}

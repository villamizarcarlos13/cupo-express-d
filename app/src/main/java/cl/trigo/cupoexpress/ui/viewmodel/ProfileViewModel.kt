package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.authentication.domain.usecase.CloseSessionUseCase
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.UserInfo
import cl.trigo.cupoexpress.domain.usecase.DeleteFcmTokenUseCase
import cl.trigo.cupoexpress.domain.usecase.GetUserInfoUseCase

class ProfileViewModel(
    private val closeSessionUseCase: CloseSessionUseCase,
    private val getUserInfoUseCase: GetUserInfoUseCase,
    private val deleteFcmTokenUseCase: DeleteFcmTokenUseCase
) : ViewModel() {
    val closeSessionLiveCompletable = LiveCompletable()
    val userInfoLiveData = LiveResult<UserInfo>()
    val deleteFcmTokenLiveData = LiveCompletable()

    fun closeSession() = closeSessionUseCase.execute(closeSessionLiveCompletable)

    fun getUserInfo() = getUserInfoUseCase.execute(userInfoLiveData)

    fun deleteFcmToken(token: String) = deleteFcmTokenUseCase.execute(deleteFcmTokenLiveData, token)

}
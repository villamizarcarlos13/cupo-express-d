package cl.trigo.core.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cl.trigo.core.storage.entity.BeneficiaryEntity

@Dao
interface BeneficiaryDao {
    @Query("SELECT * FROM beneficiaries WHERE id = :beneficiaryId")
    suspend fun getBeneficiaryById(beneficiaryId: String): BeneficiaryEntity

    @Query("SELECT * FROM beneficiaries")
    suspend fun getBeneficiariesList(): List<BeneficiaryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveBeneficiary(beneficiary: BeneficiaryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveBeneficiariesList(beneficiaries: List<BeneficiaryEntity>?)

    @Query("DELETE FROM beneficiaries")
    suspend fun deleteAllBeneficiaries()

    @Query("DELETE FROM beneficiaries WHERE id = :beneficiaryId")
    suspend fun deleteBeneficiaryById(beneficiaryId: String)

}
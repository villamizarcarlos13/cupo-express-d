package cl.trigo.cupoexpress.domain.model

data class Foundation(
    val id: String,
    val logo: String,
    val description: String,
    val region: String,
    val city: String,
    val rut: String
)
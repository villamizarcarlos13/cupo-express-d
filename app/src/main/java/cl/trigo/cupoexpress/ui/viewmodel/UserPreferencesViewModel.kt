package cl.trigo.cupoexpress.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveResult
import cl.trigo.cupoexpress.domain.model.User
import cl.trigo.cupoexpress.domain.model.UserEditResponse
import cl.trigo.cupoexpress.domain.usecase.EditUserDataUseCase
import cl.trigo.cupoexpress.domain.usecase.GetUserDataUseCase

class UserPreferencesViewModel(
    private val editUserDataUseCase: EditUserDataUseCase,
    private val getUserDataUseCase: GetUserDataUseCase
) : ViewModel(

) {
    val editUserDataLiveData = LiveResult<UserEditResponse>()
    val getUserDataLiveData = LiveResult<User>()

    fun editUser(
        firstName: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        photo: String?
    ) = editUserDataUseCase.execute(
        editUserDataLiveData, mapOf(
            EditUserDataUseCase.FIRST_NAME_PARAM to firstName,
            EditUserDataUseCase.LAST_NAME_PARAM to lastName,
            EditUserDataUseCase.EMAIL_PARAM to email,
            EditUserDataUseCase.PHONE_PARAM to phone,
            EditUserDataUseCase.PHOTO_PARAM to photo
        )
    )

    fun getUserData() = getUserDataUseCase.execute(getUserDataLiveData)

}
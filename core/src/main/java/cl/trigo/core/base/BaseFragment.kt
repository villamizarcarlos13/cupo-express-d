package cl.trigo.core.base

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = inflater.inflate(getLayoutId(), container, false)

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    fun showErrorDialog(message: String) {
        AlertDialog.Builder(activity)
            .setTitle("Error")
            .setMessage(message)
            .setPositiveButton("Aceptar", null)
            .show()
    }

    fun showAlertDialog(title: String, message: String, positiveListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(activity)
            .setTitle("Importante!")
            .setMessage(message)
            .setPositiveButton("Aceptar", positiveListener)
            .setNegativeButton("Cancelar", null)
            .show()
    }

    fun hideDialog() {}

    interface OnChangeFragmentInteractionListener {
        fun changeFragmentTo(fragment: Fragment, addBackStack: Boolean = false)
    }
}
package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.findNavController
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R

class ProgramDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program_detail)
        setupNavigation()
        makeStatusBarTransparent()
    }

    private fun setupNavigation() {
        val navController: NavController = findNavController(R.id.nav_host_program)
        val navGraph: NavGraph = navController.navInflater.inflate(R.navigation.nav_graph_program)
        navController.graph = navGraph
    }

    companion object {
        private const val SHOULD_SHOW_MAP = "SHOULD_SHOW_MAP"

        fun getLaunchIntent(context: Context, shouldShowMap: Boolean): Intent {
            return Intent(context, ProgramDetailActivity::class.java).apply {
                putExtra(SHOULD_SHOW_MAP, shouldShowMap)
            }
        }
    }
}
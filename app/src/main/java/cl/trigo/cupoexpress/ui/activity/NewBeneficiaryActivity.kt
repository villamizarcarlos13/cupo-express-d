package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.fragment.AddBeneficiaryInformationFragment

class NewBeneficiaryActivity : AppCompatActivity(),
    BaseFragment.OnChangeFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_beneficiary)
        makeStatusBarTransparent()
        val addBeneficiaryFragment = AddBeneficiaryInformationFragment(this)
        changeFragmentTo(addBeneficiaryFragment)
    }

    override fun changeFragmentTo(fragment: Fragment, addBackStack: Boolean) {
        if (addBackStack) {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragment_container, fragment).addToBackStack("tag")

            }.commit()
        } else {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragment_container, fragment)
            }.commit()
        }
    }

    companion object {
        fun getLaunchIntent(context: Context): Intent =
            Intent(context, NewBeneficiaryActivity::class.java)
    }
}

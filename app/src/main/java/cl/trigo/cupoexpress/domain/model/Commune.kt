package cl.trigo.cupoexpress.domain.model

data class Commune (
    val name: String,
    val country:String,
    val region: String,
    val province: String,
    val isActive: Boolean,
    val id:String
)
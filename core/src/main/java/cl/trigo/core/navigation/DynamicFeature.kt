package cl.trigo.core.navigation

interface DynamicFeature<T> {
    val dynamicStart: T?
}
package cl.trigo.authentication.domain.repository

import cl.trigo.authentication.domain.model.Login
import cl.trigo.authentication.domain.model.LoginRut
import cl.trigo.authentication.domain.model.Register
import cl.trigo.core.base.ResponseCompletable

interface LoginRepository {
    suspend fun signUp(
        firstName: String,
        lastName: String,
        rut: String,
        email: String,
        password: String
    ): Register

    suspend fun signIn(email: String, password: String): Login

    suspend fun verifyUserByRut(rut: String): LoginRut

    suspend fun restorePassword(email: String): ResponseCompletable
}
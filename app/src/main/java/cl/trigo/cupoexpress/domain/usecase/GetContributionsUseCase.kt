package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.Contribution
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetContributionsUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUnitUseCase<List<Contribution>>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): List<Contribution>? {
        return beneficiaryRepository.getContributions()
    }
}
package cl.trigo.payment.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Benefit(
    val code: String,
    val name: String,
    val description: String,
    val shortName: String,
    val price: Int,
    val stock: Int,
    val startavailable: String,
    val endavailable: String,
    val images: List<String?>,
    val active: Boolean,
    val id: String
) : Parcelable
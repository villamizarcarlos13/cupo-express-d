package cl.trigo.payment.data.model.entry

import androidx.annotation.Keep
import cl.trigo.payment.domain.model.RegisterPaymentMethod

@Keep
data class RegisterPaymentMethodEntry(
    val token: String?,
    val formAction: String?
)

fun RegisterPaymentMethodEntry.toRegisterPaymentMethod(): RegisterPaymentMethod {
    return RegisterPaymentMethod(
        token = token.orEmpty(),
        formAction = formAction.orEmpty()
    )
}
package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep

@Keep
data class UserEntry(
    val id: String?,
    val firstname: String?,
    val lastname: String?,
    val photo: String?,
    val rut: String?,
    val email: String?,
    val role: String?,
    val phone: String?,
    val country: String?,
    val city: String?,
    val region: String?,
    val address: String?,
    val active: Boolean,
    val termsAcceptance: Boolean
)


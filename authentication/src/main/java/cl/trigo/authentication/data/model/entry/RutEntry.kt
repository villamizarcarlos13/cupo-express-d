package cl.trigo.authentication.data.model.entry

import androidx.annotation.Keep
import cl.trigo.authentication.domain.model.LoginRut
import cl.trigo.authentication.domain.model.Status
@Keep
data class RutEntry(val user: UserRutEntry)

fun RutEntry.toLogin(status: Status): LoginRut {
    return LoginRut(user = user.toUserRut(), status = status)
}
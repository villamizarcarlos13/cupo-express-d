package cl.trigo.authentication.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveResult

class LoginRutViewModel(
    private val getUserByRutUseCase: cl.trigo.authentication.domain.usecase.GetUserByRutUseCase
) : ViewModel() {
    val userByRutLiveData = LiveResult<cl.trigo.authentication.domain.model.LoginRut>()
    fun validateRut(rut: String) = getUserByRutUseCase.execute(userByRutLiveData, rut)
}
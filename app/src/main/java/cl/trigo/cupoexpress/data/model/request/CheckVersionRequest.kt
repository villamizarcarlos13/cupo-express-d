package cl.trigo.cupoexpress.data.model.request

data class CheckVersionRequest (
    val system: String = "ANDROID"
)

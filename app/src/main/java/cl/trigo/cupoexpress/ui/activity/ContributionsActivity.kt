package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.fragment.ContributionsFragment
import kotlinx.android.synthetic.main.activity_contributions.*

class ContributionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contributions)
        makeStatusBarTransparent()
        contributions_top_bar.setNavigationOnClickListener { this.onBackPressed() }
        supportFragmentManager.beginTransaction()
            .add(R.id.contributions_fragment_container, ContributionsFragment())
            .commit()
    }

    companion object {
        fun getLaunchIntent(context: Context): Intent =
            Intent(context, ContributionsActivity::class.java)
    }
}
package cl.trigo.cupoexpress.domain.usecase

import cl.trigo.core.coroutines.ResultUnitUseCase
import cl.trigo.cupoexpress.domain.model.User
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import kotlinx.coroutines.Dispatchers

class GetUserDataUseCase(
    private val beneficiaryRepository: BeneficiaryRepository
) : ResultUnitUseCase<User>(
    backgroundContext = Dispatchers.IO,
    foregroundContext = Dispatchers.Main
) {
    override suspend fun executeOnBackground(): User? =
        beneficiaryRepository.getUserData()

}
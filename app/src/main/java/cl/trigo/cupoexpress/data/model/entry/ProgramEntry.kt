package cl.trigo.cupoexpress.data.model.entry

import androidx.annotation.Keep

@Keep
data class ProgramEntry(
    val id: String,
    val name: String,
    val description: String,
    val region: String,
    val city: String,
    val country: String,
    val contact: String,
    val email: String,
    val phone: String,
    val logo: String,
    val header: String,
    val active: Boolean,
    val foundation: FoundationEntry
)

fun ProgramEntry.toProgram(): cl.trigo.cupoexpress.domain.model.Program {
    return cl.trigo.cupoexpress.domain.model.Program(
        id,
        name,
        description,
        region,
        city,
        country,
        contact,
        email,
        phone,
        logo,
        header,
        active,
        foundation.toFoundation()
    )
}
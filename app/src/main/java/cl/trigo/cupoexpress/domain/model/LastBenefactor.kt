package cl.trigo.cupoexpress.domain.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class LastBenefactor(
    val fullname: String?,
    val createdAt: Int?,
    val rut: String?,
    val id: String?,
    val photo: String?
) : Parcelable
package cl.trigo.core.common

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cl.trigo.core.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_bottom_sheet.*

class RegisterBottomSheetFragment : BottomSheetDialogFragment() {

    private var title: String = ""
    private var message: String = ""
    private var button: String = ""

    private val liveData: MutableLiveData<Boolean> = MutableLiveData()

    fun liveData(): LiveData<Boolean> = liveData

    override fun getTheme(): Int = R.style.RoundedBottomSheetDialogTheme

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_bottom_sheet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            title = it.getString(KEY_TITLE).toString()
            message = it.getString(KEY_MESSAGE).toString()
            button = it.getString(KEY_BUTTON_TEXT).toString()
        }
    }

    private fun initView() {
        tv_title.text = title
        tv_message.text = message
        btn_finish.text = button
        btn_finish.setOnClickListener {
            liveData.postValue(true)
            this@RegisterBottomSheetFragment.dismiss()
        }
    }

    companion object {

        fun newInstance(title: String, message: String, button: String ): RegisterBottomSheetFragment =
            RegisterBottomSheetFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_TITLE, title)
                    putString(KEY_MESSAGE, message)
                    putString(KEY_BUTTON_TEXT, button)
                }
            }

        private const val KEY_TITLE = "title"
        private const val KEY_MESSAGE = "message"
        private const val KEY_BUTTON_TEXT = "button_text"
    }

}
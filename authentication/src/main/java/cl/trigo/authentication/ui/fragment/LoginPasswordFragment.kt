package cl.trigo.authentication.ui.fragment

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import cl.trigo.authentication.R
import cl.trigo.authentication.domain.model.User
import cl.trigo.authentication.ui.viewmodel.LoginViewModel
import cl.trigo.core.base.BaseFragment
import cl.trigo.core.common.RegisterBottomSheetFragment
import cl.trigo.core.coroutines.Completable
import cl.trigo.core.coroutines.Result
import cl.trigo.core.extension.observe
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_login_password.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.HttpException

class LoginPasswordFragment : BaseFragment() {

    private val loginViewModel by viewModel<LoginViewModel>()
    private var passwordError: Boolean = false
    private val args: LoginPasswordFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(loginViewModel) {
            observe(loginLiveData, ::validatePasswordObserver)
            observe(restorePasswordLiveData, ::restorePasswordObserver)
        }
        setupClickListeners()
        setupView()
    }

    private fun setupClickListeners() {
        login_pass_top_bar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        btn_login.setOnClickListener {
            val password = input_password.editText!!.text.toString()
            if (!validatePassword(password)) return@setOnClickListener
            loginViewModel.login(args.user.email.orEmpty(), password)
        }
        btn_restore_password.setOnClickListener { showRestorePasswordDialog() }
    }

    private fun setupView() {
        tv_user_name.text = resources.getString(R.string.greatings, args.user.firstname)
        input_password.editText?.addTextChangedListener(textWatcher)
    }

    private fun showRestorePasswordDialog() {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.restore_password))
                .setMessage(getString(R.string.restore_password_message))
                .setPositiveButton("Restore") { _, _ ->
                    loginViewModel.restorePassword(args.user.email.orEmpty())
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .show()
        }
    }

    private fun validatePassword(password: String): Boolean {
        password.let {
            if (it.length < 3) {
                showPasswordError(resources.getString(R.string.wrong_pass_format_login))
                return false
            }
        }
        return true
    }

    private fun showPasswordError(error: String) {
        passwordError = true
        tv_error.text = error
        tv_error.isVisible = true
        input_password.requestFocus()
        input_password.boxStrokeColor = ResourcesCompat.getColor(resources, R.color.red, null)
    }

    private fun removePasswordError() {
        tv_error.isVisible = false
        input_password.boxStrokeColor = ResourcesCompat.getColor(resources, R.color.white, null)
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (passwordError) {
                passwordError = false
                removePasswordError()
            }
        }
    }

    private fun showRestorePasswordSuccessful() {
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.done),
                resources.getString(R.string.email_sent),
                resources.getString(R.string.proceed)
            )
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun showRestorePasswordError() {
        val bottomSheet =
            RegisterBottomSheetFragment.newInstance(
                resources.getString(R.string.error),
                resources.getString(R.string.error_restore_paasword),
                resources.getString(R.string.proceed)
            )
        bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
    }

    private fun showLoading(isLoading: Boolean) {
        login_pass_loading.isVisible = isLoading
    }

    private fun restorePasswordObserver(result: Completable) {
        when (result) {
            is Completable.OnLoading -> {
                showLoading(true)
            }
            is Completable.OnComplete -> {
                showLoading(false)
                showRestorePasswordSuccessful()
            }
            is Completable.OnError -> {
                showLoading(false)
                showRestorePasswordError()
            }
        }
    }

    private fun validatePasswordObserver(result: Result<User>?) {
        when (result) {
            is Result.OnLoading -> {
                showLoading(true)
                btn_login.isEnabled = false
            }
            is Result.OnSuccess -> {
                showLoading(false)
                btn_login.isEnabled = true
                activity?.setResult(Activity.RESULT_OK)
                activity?.finish()
            }
            is Result.OnError -> {
                showLoading(false)
                btn_login.isEnabled = true
                if (result.throwable is HttpException) {
                    val exception: HttpException = result.throwable as HttpException
                    when (exception.code()) {
                        400 -> {
                            showPasswordError(resources.getString(R.string.wrong_pass))
                        }
                        else -> {
                            val errorMessage = result.throwable.message!!
                            errorMessage.run {
                                showErrorDialog(message = this)
                            }
                        }
                    }
                } else {
                    val errorMessage = result.throwable.message!!
                    errorMessage.run {
                        showErrorDialog(message = this)
                    }
                }
            }
        }
    }

    override fun getLayoutId() = R.layout.fragment_login_password
}

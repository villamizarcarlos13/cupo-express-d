package cl.trigo.cupoexpress.di

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import androidx.room.Room
import cl.trigo.core.util.AppPreferences
import cl.trigo.cupoexpress.BuildConfig
import cl.trigo.cupoexpress.data.net.BeneficiaryService
import cl.trigo.cupoexpress.data.BeneficiaryDataRepository
import cl.trigo.cupoexpress.data.local.BeneficiaryLocal
import cl.trigo.cupoexpress.data.local.BeneficiaryLocalDataSource
import cl.trigo.core.storage.db.CupoExpressDatabase
import cl.trigo.core.storage.db.DbConstants.DATABASE_NAME
import cl.trigo.cupoexpress.data.mapper.BeneficiaryDataMapper
import cl.trigo.cupoexpress.data.remote.BeneficiaryRemote
import cl.trigo.cupoexpress.data.remote.BeneficiaryRemoteDataSource
import cl.trigo.cupoexpress.domain.repository.BeneficiaryRepository
import cl.trigo.cupoexpress.domain.usecase.*
import cl.trigo.cupoexpress.ui.viewmodel.*
import cl.trigo.payment.domain.usecase.DeletePaymentMethodUseCase
import com.google.firebase.auth.FirebaseAuth
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {

    /* Android Services */
    single {
        androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    /* Shared Preferences */
    single<SharedPreferences> {
        androidContext().getSharedPreferences(
            AppPreferences.SHARED_PREFERENCES_NAME,
            Context.MODE_PRIVATE
        )
    }

    /* OkHttp */
    single {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder()
            .callTimeout(1, TimeUnit.MINUTES)
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .addInterceptor(logging)
            .build()
    }

    /* Retrofit */
    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    /* Database */
    single {
        Room.databaseBuilder(
            androidApplication(),
            CupoExpressDatabase::class.java,
            DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    /* Dao Interfaces */
    factory { get<CupoExpressDatabase>().beneficiaryDao()}

    /* Mappers */
    factory { BeneficiaryDataMapper() }

    /* Firebase */
    single {
        FirebaseAuth.getInstance()
    }

    /* API REST Remote */
    single { get<Retrofit>().create(BeneficiaryService::class.java) as BeneficiaryService }

    /* DataSources */
    factory<BeneficiaryRemote> { BeneficiaryRemoteDataSource(AppPreferences, get()) }
    factory<BeneficiaryLocal> { BeneficiaryLocalDataSource(AppPreferences, get()) }

    /* Repositories */
    factory<BeneficiaryRepository> { BeneficiaryDataRepository(get(), get(), get()) }

    /* UseCases */
    factory { CheckVersionUseCase(get()) }
    factory { GetBeneficiariesUseCase(get()) }
    factory { DeleteBeneficiaryByIdUseCase(get()) }
    factory { AddBeneficiaryUseCase(get()) }
    factory { EditBeneficiaryUseCase(get()) }
    factory { GetBeneficiaryDetailByRutUseCase(get()) }
    factory { GetUserInfoUseCase(get()) }
    factory { GetCommunesUseCase(get()) }
    factory { SetBeneficiaryFavoriteUseCase(get()) }
    factory { GetUserDataUseCase(get()) }
    factory { EditUserDataUseCase(get()) }
    factory { GetSubscriptionsUseCase(get()) }
    factory { GetContributionsUseCase(get()) }
    factory { DeleteSubscriptionUseCase(get()) }
    factory { DeletePaymentMethodUseCase(get()) }
    factory { GetBeneficiaryDonationsUseCase(get()) }
    factory { GetProgramsUseCase(get()) }
    factory { SaveFcmTokenUseCase(get()) }
    factory { DeleteFcmTokenUseCase(get()) }
    factory { UpdateNotificationUseCase(get()) }
    factory { GetKiosksUseCase(get()) }


    /* View Models */
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { ProfileViewModel(get(), get(), get()) }
    viewModel { BeneficiaryViewModel(get(), get(), get(), get()) }
    viewModel { BeneficiaryDetailViewModel(get(), get(), get(), get()) }
    viewModel { AddBeneficiaryViewModel(get(), get()) }
    viewModel { EditBeneficiaryViewModel(get(), get()) }
    viewModel { UserPreferencesViewModel(get(), get()) }
    viewModel { SubscriptionsViewModel(get(), get()) }
    viewModel { ContributionsViewModel(get()) }
    viewModel { ProgramViewModel(get(), get(), get()) }
    viewModel { KiosksViewModel(get(), get()) }
}
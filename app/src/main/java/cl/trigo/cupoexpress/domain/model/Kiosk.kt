package cl.trigo.cupoexpress.domain.model

data class Kiosk (
    val id: String,
    val name: String,
    val country: String,
    val region: String,
    val commune: String,
    val province: String,
    val address: String,
    val image: String,
    val description: String,
    val isActive: Boolean,
    val lat: Double,
    val lng: Double
)
package cl.trigo.cupoexpress

import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import cl.trigo.authentication.di.authenticationModule
import cl.trigo.cupoexpress.di.appModule
import cl.trigo.payment.di.paymentModule
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

open class CupoExpress : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
        initRemoteConfigs()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@CupoExpress)
            modules(listOf(
                authenticationModule,
                paymentModule,
                appModule))
        }
    }

    private fun initRemoteConfigs() {
        val remoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val updated = it.result
                    Log.d(TAG, "Config params updated: $updated")
                    Log.d(TAG,"Fetch and activate succeeded")
                } else {
                    Log.d(TAG,"Fetch failed")
                }
            }
    }
}
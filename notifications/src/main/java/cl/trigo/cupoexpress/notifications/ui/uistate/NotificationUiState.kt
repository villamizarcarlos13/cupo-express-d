package cl.trigo.cupoexpress.notifications.ui.uistate

import cl.trigo.cupoexpress.notifications.ui.model.Notification


sealed class NotificationUiState (val isLoading: Boolean = false, val notificationsList: List<Notification>? = null) {
    object Loading : NotificationUiState(isLoading = true)
    data class Error(val error: Throwable) : NotificationUiState( isLoading = false)
    data class SuccessLoadNotifications(val notifications: List<Notification>) : NotificationUiState( isLoading = false, notificationsList = notifications)
    object SuccessMarkAsRead : NotificationUiState( isLoading = false)
    object SuccessDelete : NotificationUiState( isLoading = false)
}
package cl.trigo.payment.data.net

import cl.trigo.core.base.Response
import cl.trigo.core.base.ResponseCompletable
import cl.trigo.payment.data.model.entry.BenefitEntry
import cl.trigo.payment.data.model.entry.PaymentMethodEntry
import cl.trigo.payment.data.model.entry.RegisterPaymentMethodEntry
import cl.trigo.payment.data.model.request.PaymentRequest
import retrofit2.Call
import retrofit2.http.*

interface PaymentService {

    @Headers("Accept: application/json")
    @GET("payments/methods")
    fun getPaymentMethods(
        @Header("Authorization") authorization: String
    ): Call<Response<List<PaymentMethodEntry>>>

    @Headers("Accept: application/json")
    @POST("payments/register")
    fun registerPaymentMethod(
        @Header("Authorization") authorization: String
    ): Call<Response<RegisterPaymentMethodEntry>>

    @Headers("Accept: application/json")
    @DELETE("payments/methods/{id}")
    fun deletePaymentMethodById(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @POST("donations")
    fun confirmPayment(
        @Header("Authorization") authorization: String,
        @Body paymentRequest: PaymentRequest
    ): Call<ResponseCompletable>

    @Headers("Accept: application/json")
    @GET("benefits")
    fun getBenefits(
    ): Call<Response<List<BenefitEntry>>>
}
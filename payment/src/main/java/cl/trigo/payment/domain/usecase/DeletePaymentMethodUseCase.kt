package cl.trigo.payment.domain.usecase

import cl.trigo.payment.domain.repository.PaymentRepository

class DeletePaymentMethodUseCase(private val paymentRepository: PaymentRepository) {
    suspend fun deletePaymentMethod(params: String) {
        paymentRepository.deletePaymentMethod(id = params)
    }
}
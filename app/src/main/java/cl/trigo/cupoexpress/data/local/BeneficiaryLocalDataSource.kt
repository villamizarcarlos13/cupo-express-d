package cl.trigo.cupoexpress.data.local

import cl.trigo.core.util.AppPreferences
import cl.trigo.core.storage.dao.BeneficiaryDao
import cl.trigo.core.storage.entity.BeneficiaryEntity
import cl.trigo.cupoexpress.domain.model.UserInfo

class BeneficiaryLocalDataSource(
    private val appPreferences: AppPreferences,
    private val beneficiaryDao: BeneficiaryDao
) : BeneficiaryLocal {
    override suspend fun getUserInfo(): UserInfo = UserInfo(
        appPreferences.userFirstName,
        appPreferences.userImage,
        appPreferences.firebaseToken.orEmpty(),
        appPreferences.newNotificationCount.or(0),
        AppPreferences.authToken != AppPreferences.AUTH_TOKEN_DEFAULT
    )

    override suspend fun saveUserInfoToPreferences(name: String, userImage: String) {
        appPreferences.userFirstName = name
        appPreferences.userImage = userImage
    }

    override suspend fun updateNotification() {
        val counter = appPreferences.newNotificationCount + 1
        appPreferences.newNotificationCount = counter
    }

    override suspend fun getShowCommunesPrompt(): Boolean =
        appPreferences.showCommunesPrompt

    override suspend fun getBeneficiaries(): List<BeneficiaryEntity> =
       beneficiaryDao.getBeneficiariesList()


    override suspend fun insertBeneficiariesList(beneficiaries: List<BeneficiaryEntity>) =
        beneficiaryDao.saveBeneficiariesList(beneficiaries)


    override suspend fun getBeneficiaryById(id: String): BeneficiaryEntity =
        beneficiaryDao.getBeneficiaryById(id)


    override suspend fun insertBeneficiary(beneficiary: BeneficiaryEntity) {
        beneficiaryDao.saveBeneficiary(beneficiary)
    }

    override suspend fun deleteAllBeneficiary() {
        beneficiaryDao.deleteAllBeneficiaries()
    }

    override suspend fun deleteBeneficiaryById(id: String) {
        beneficiaryDao.deleteBeneficiaryById(id)
    }
}
package cl.trigo.payment.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveResult
import cl.trigo.payment.domain.model.Benefit
import cl.trigo.payment.domain.usecase.GetBenefitsUseCase

class GetBenefitsViewModel(
    private val getBenefitsUseCase: GetBenefitsUseCase
) : ViewModel(

) {

    val getBenefitsLiveData = LiveResult<List<Benefit>>()

    fun getBenefits() = getBenefitsUseCase.execute(getBenefitsLiveData)

}
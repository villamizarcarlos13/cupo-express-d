package cl.trigo.cupoexpress.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cl.trigo.core.extension.makeStatusBarTransparent
import cl.trigo.cupoexpress.R
import cl.trigo.cupoexpress.ui.fragment.UserPreferencesFragment
import kotlinx.android.synthetic.main.activity_user_preferences.*

class UserPreferencesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_preferences)
        makeStatusBarTransparent()
        user_preference_top_bar.setNavigationOnClickListener { this.onBackPressed() }
        supportFragmentManager.beginTransaction()
            .add(R.id.user_preference_fragment_container, UserPreferencesFragment())
            .commit()
    }

    companion object {
        fun getLaunchIntent(context: Context): Intent =
            Intent(context, UserPreferencesActivity::class.java)
    }

}
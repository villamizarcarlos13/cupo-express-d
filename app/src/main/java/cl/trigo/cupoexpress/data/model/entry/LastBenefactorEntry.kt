package cl.trigo.cupoexpress.data.model.entry

import cl.trigo.cupoexpress.domain.model.LastBenefactor

data class LastBenefactorEntry (
    val fullname: String?,
    val createdAt: Int?,
    val rut: String?,
    val id: String?,
    val photo: String?
)

fun LastBenefactorEntry.toLastBenefactor() = LastBenefactor (
    fullname = fullname.orEmpty(),
    createdAt = createdAt?.or(0),
    rut = rut,
    id = id,
    photo = photo
)
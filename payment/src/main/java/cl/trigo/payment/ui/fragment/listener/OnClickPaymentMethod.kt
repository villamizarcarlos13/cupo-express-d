package cl.trigo.payment.ui.fragment.listener

import cl.trigo.payment.domain.model.PaymentMethod

interface OnClickPaymentMethod {
    fun onClickItem(paymentMethod: PaymentMethod): Unit
    fun onClickDelete(paymentMethod: PaymentMethod): Unit
}
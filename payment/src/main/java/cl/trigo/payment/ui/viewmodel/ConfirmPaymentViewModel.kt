package cl.trigo.payment.ui.viewmodel

import androidx.lifecycle.ViewModel
import cl.trigo.core.extension.LiveCompletable
import cl.trigo.payment.domain.usecase.ConfirmPaymentUseCase
import cl.trigo.payment.domain.usecase.ConfirmPaymentUseCase.Companion.BENEFICIARY_ID_PARAM
import cl.trigo.payment.domain.usecase.ConfirmPaymentUseCase.Companion.BENEFIT_ID_PARAM
import cl.trigo.payment.domain.usecase.ConfirmPaymentUseCase.Companion.PAYMENT_METHOD_PARAM
import cl.trigo.payment.domain.usecase.ConfirmPaymentUseCase.Companion.QUANTITY_PARAM

class ConfirmPaymentViewModel(
    private val confirmPaymentUseCase: ConfirmPaymentUseCase
) : ViewModel(

) {
    val confirmPaymentLiveData = LiveCompletable()


    fun confirmPayment(
        benefitId: String,
        beneficiaryId: String,
        quantity: String,
        paymentMethodId: String
    ) =
        confirmPaymentUseCase.execute(
            confirmPaymentLiveData, mapOf(
                BENEFIT_ID_PARAM to benefitId,
                BENEFICIARY_ID_PARAM to beneficiaryId,
                QUANTITY_PARAM to quantity,
                PAYMENT_METHOD_PARAM to paymentMethodId
            )
        )
}
